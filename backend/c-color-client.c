/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "gcolorsel2.h"
#include "gcolorsel2-private.h"

#include "GColorsel2.h"

#include "c-palette.h"
#include "c-color.h"

#include <liboaf/liboaf.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

typedef struct {
  ColorClient client;

  unsigned long cnum;
  
  CPalette *palette;

  GList *insert_list; /* GList<ColorList *> */
  ColorList *insert_list_last;
  
} CColorClientPrivate;

#define C_CLIENT_PRIVATE(client) ((CColorClientPrivate *)(C_COLOR_CLIENT(client)->private))

/* CORBA */

typedef struct {
  POA_ColorClient servant;
  
  CColorClient *color_client;
  
  CPalette *palette;
} impl_POA_ColorClient;

#define C_SERVANT_PRIVATE(servant) (C_CLIENT_PRIVATE (servant->color_client))

/* Error related */

static CErrorSeverity
corba_error_severity_to_c_error_severity (ColorErrorSeverity severity)
{
  switch (severity) {
    case ErrorUser:
      return C_ERROR_USER; break;
    default:
  }
  
  return C_ERROR_BUG;
}      

static CErrorType
corba_error_type_to_c_error_type (ColorErrorType error)
{
  switch (error) {
    case ErrorBadConnectNum:
      return C_ERROR_BAD_CONNECT_NUM; break;    
    case ErrorBadPosition:
      return C_ERROR_BAD_POSITION; break;    
    case ErrorBadRGB:
      return C_ERROR_BAD_RGB; break;    
    case ErrorCannotOpenFile:
      return C_ERROR_CANNOT_OPEN_FILE; break;
    case ErrorBadFile:
      return C_ERROR_BAD_FILE; break;
    case ErrorCannotSaveFile:
      return C_ERROR_CANNOT_SAVE_FILE; break;
    case ErrorReadOnly:
      return C_ERROR_READ_ONLY; break;      
    default:
  }
  
  return C_ERROR_UNKNOWN;
}

static gboolean
c_color_client_set_error (CORBA_Environment *ev, CError **error)
{
  ColorException *except;
  
  switch (ev->_major) {
    case CORBA_NO_EXCEPTION:
      CORBA_exception_free (ev);
      return FALSE;
      
    case CORBA_SYSTEM_EXCEPTION:
      *error = c_error_new (C_ERROR_UNKNOWN, C_ERROR_SERVER, _("CORBA Error : '%s'"), 
                            CORBA_exception_id (ev));
      CORBA_exception_free (ev);
      return TRUE;
      
    case CORBA_USER_EXCEPTION:
      except = CORBA_exception_value (ev);                          
      
      *error = c_error_new_simple (corba_error_type_to_c_error_type (except->type),
                                   corba_error_severity_to_c_error_severity (except->severity),
                                   except->message);
      CORBA_exception_free (ev);
      return TRUE;
      
    default:
      g_assert_not_reached ();
  }
  
  return TRUE;
}

/* CORBA request */

static void 
client_ping (PortableServer_Servant _servant,
             CORBA_Environment *ev);                  
             
static void
client_remove (PortableServer_Servant _servant,
	       const ColorPosList *pos_list,
	       CORBA_Environment *ev);
	       
static void
client_insert (PortableServer_Servant _servant,
               CORBA_unsigned_long pos, const ColorList *color_list,	       
               CORBA_Environment *ev);
               
static void
client_change (PortableServer_Servant _servant,
               CORBA_unsigned_long pos, const Color *color,
               CORBA_Environment *ev);               
               
static void
client_move (PortableServer_Servant _servant,
             CORBA_unsigned_long to, const ColorPosList *pos_list,
             CORBA_Environment *ev);               
         
static PortableServer_ServantBase__epv base_epv = {
  NULL,
  NULL,
  NULL
};
  
static POA_ColorClient__epv client_epv = {
  NULL,
  client_ping,
  client_remove,
  client_move,
  client_insert,
  client_change,
};

static POA_ColorClient__vepv poa_client_vepv = { &base_epv, &client_epv };

static void 
client_ping (PortableServer_Servant _servant,
             CORBA_Environment *ev)
{
}           

static void
client_remove (PortableServer_Servant _servant,
	       const ColorPosList *pos_list,
	       CORBA_Environment *ev)
{
  impl_POA_ColorClient *servant = (impl_POA_ColorClient *)_servant;
  CError *error = NULL;
  CColorBase *base = C_COLOR_BASE (servant->color_client);  
  
  c_color_base_emit_freeze (C_COLOR_BASE (servant->color_client));
  
  c_palette_remove (C_SERVANT_PRIVATE (servant)->palette, pos_list, base, &error);
  /* FIXME : error ? */
  
  c_color_base_emit_thaw (C_COLOR_BASE (servant->color_client));
}

static void
client_move (PortableServer_Servant _servant,
             CORBA_unsigned_long to, const ColorPosList *pos_list,
             CORBA_Environment *ev)
{
  impl_POA_ColorClient *servant = (impl_POA_ColorClient *)_servant;
  CPalette *palette = C_SERVANT_PRIVATE (servant)->palette;
  CError *error = NULL;
  CColorBase *base = C_COLOR_BASE (servant->color_client);  
  
  c_color_base_emit_freeze (C_COLOR_BASE (servant->color_client));  
  c_palette_move (palette, to, pos_list, base, &error);
  /* FIXME : error ? */
  c_color_base_emit_thaw (C_COLOR_BASE (servant->color_client));  
}             

static void
client_insert (PortableServer_Servant _servant,
               CORBA_unsigned_long pos, const ColorList *color_list,	       
               CORBA_Environment *ev)
{
  impl_POA_ColorClient *servant = (impl_POA_ColorClient *)_servant;
  CPalette *palette = C_SERVANT_PRIVATE (servant)->palette;
  CError *error = NULL;
  CColorBase *base = C_COLOR_BASE (servant->color_client);
  
  base->data_received = TRUE;
  
  c_color_base_emit_freeze (C_COLOR_BASE (servant->color_client));
  
  c_palette_insert (palette, pos, color_list, base, &error);
  /* FIXME : error ? */
  
  c_color_base_emit_thaw (C_COLOR_BASE (servant->color_client));  
}                             

static void
client_change (PortableServer_Servant _servant,
               CORBA_unsigned_long pos, const Color *color,
               CORBA_Environment *ev)
{
  impl_POA_ColorClient *servant = (impl_POA_ColorClient *)_servant;
  CPalette *palette = C_SERVANT_PRIVATE (servant)->palette;  
  CError *error = NULL;
  CColorBase *base = C_COLOR_BASE (servant->color_client);  
  
  c_palette_change (palette, pos, color, base, &error);
  /* FIXME : error != NULL */
  
  c_color_base_emit_freeze (C_COLOR_BASE (servant->color_client));
  c_color_base_emit_thaw (C_COLOR_BASE (servant->color_client));  
}               

/* Object */

#define PARENT_TYPE C_COLOR_BASE_TYPE
static CColorBaseClass *parent_class = NULL;

static ColorServer c_color_client_get_server (CError **error);
static ColorClient c_color_client_get_client (CColorClient *color_client, CError **error);

static void
c_color_client_connect (CColorClient *color_client, const char *path, 
			gboolean try_create, gboolean truncate, 
			CError **error);

static void 
c_color_client_disconnect (CColorClient *color_client, CError **error);

void do_remove (CColorBase *color_base, GList *pos, CError **error);
void do_insert (CColorBase *color_base, 
		gushort red, gushort green, gushort blue,
		const char *name, const char *comment);

void do_commit_insert (CColorBase *color_base, guint pos, CError **error);

void do_change (CColorBase *color_base, guint pos,
                gushort red, gushort green, gushort blue,
                const char *name, const char *comment, CError **error);
                
void do_move (CColorBase *color_base, guint to,
              GList *pos, CError **error);                

void do_get (CColorBase *color_base, guint pos,
	     gushort *red, gushort *green, gushort *blue,
	     char **name, char **comment, CError **error);

guint do_get_length (CColorBase *color_base);

/* CColorClient*/

static void
destroy (GtkObject *object)
{
  CError *error = NULL;
  
  if (C_CLIENT_PRIVATE (object)->cnum) {
    c_color_client_disconnect (C_COLOR_CLIENT (object), &error);
    
    if (error) {
      g_warning ("Disconnect error : %s", error->message);
      c_error_free (error); 
    }
  }  
  
  g_free (C_COLOR_CLIENT (object)->private);
  
  if (C_COLOR_CLIENT (object)->path)
    g_free (C_COLOR_CLIENT (object)->path);
    
  c_palette_free (C_CLIENT_PRIVATE (object)->palette);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void 
class_init (CColorClientClass *class)
{
  GtkObjectClass *object_class;
  CColorBaseClass *base_class;
  
  object_class = (GtkObjectClass *) class;
  base_class = (CColorBaseClass *) class;
  
  parent_class = gtk_type_class (C_COLOR_BASE_TYPE);
  
  object_class->destroy = destroy;  

  base_class->do_remove = do_remove;
  base_class->do_insert = do_insert;
  base_class->do_move = do_move;
  base_class->do_commit_insert = do_commit_insert;
  base_class->do_change = do_change;
  base_class->do_get = do_get;
  base_class->do_get_length = do_get_length;
}

static void 
init (CColorClient *color_client)
{
  CColorClientPrivate  *private;  
  
  color_client->path = NULL;
  
  private = g_new0 (CColorClientPrivate, 1);
  color_client->private = private;
  
  private->cnum   = 0;
  private->client = CORBA_OBJECT_NIL;

  private->insert_list = NULL;
  private->insert_list_last = NULL;

  private->palette = c_palette_new ();
}

void
c_color_client_construct (CColorClient *color_client, const char *path, 
			  gboolean try_create, gboolean truncate, 
   			  CError **error)
{
  g_assert (color_client != NULL);
  g_assert (path != NULL);
  
  c_color_client_connect (color_client, path, try_create, truncate, error);
}   			  

CColorClient *
c_color_client_new (const char *path,
                    gboolean try_create, gboolean truncate,
                    CError **error)
{
  CColorClient *color_client;
  
  color_client = C_COLOR_CLIENT (gtk_type_new (c_color_client_get_type ()));
  
  c_color_client_construct (color_client, path, try_create, truncate, error);
  
  return color_client;
}

static void
c_color_client_connect (CColorClient *color_client, const char *path, 
			gboolean try_create, gboolean truncate, 
			CError **error)
{  
  ColorServer color_server;
  ColorClient client;
  CORBA_Environment ev;
  CORBA_boolean corba_read_only;

  CORBA_exception_init(&ev);

  g_assert (color_client != NULL);
  g_assert (path != NULL);
  
  /* FIXME : si on est deja connecte ? */
  
  client = c_color_client_get_client (color_client, error);
  if (*error) return;
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  C_CLIENT_PRIVATE (color_client)->cnum
    = ColorServer_connect (color_server, client,
                           path, try_create, truncate, &corba_read_only, &ev);                                                        
                           
  if (c_color_client_set_error (&ev, error)) 
    C_CLIENT_PRIVATE (color_client)->cnum = 0;
  else {
    color_client->path = g_strdup (path);

    C_COLOR_BASE (color_client)->read_only = corba_read_only;
  }
}

static void 
c_color_client_disconnect (CColorClient *color_client, CError **error)
{
  ColorServer color_server;
  CORBA_Environment ev;

  CORBA_exception_init(&ev);

  g_assert (color_client != NULL);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  ColorServer_disconnect (color_server, C_CLIENT_PRIVATE (color_client)->cnum, &ev);
                           
  if (! c_color_client_set_error (&ev, error)) 
    C_CLIENT_PRIVATE (color_client)->cnum = 0;  
}

static ColorServer static_color_server = CORBA_OBJECT_NIL;

static ColorServer 
c_color_client_get_server (CError **error)
{
  CORBA_Environment ev;

  CORBA_exception_init(&ev);

  if (static_color_server == CORBA_OBJECT_NIL) {

    static_color_server = oaf_activate_from_id ("OAFAID:["IID"]", 0, NULL, &ev);
  
    if (CORBA_Object_is_nil (static_color_server, &ev)) 
    
      *error = c_error_new (C_ERROR_NO_SERVER, C_ERROR_SERVER,
                            _("Failed to get object reference for ColorServer"));
    
  }

  return static_color_server;
}

static ColorClient
c_color_client_get_client (CColorClient *color_client, CError **error)
{
  CORBA_Environment ev;
  impl_POA_ColorClient *servant;
  
  CORBA_exception_init (&ev);
  
  g_return_val_if_fail (color_client != NULL, CORBA_OBJECT_NIL);
  
  if (CORBA_Object_is_nil (C_CLIENT_PRIVATE (color_client)->client, &ev)) {
  
    servant = g_new0 (impl_POA_ColorClient, 1);
    servant->servant._private = NULL;
    servant->servant.vepv = &poa_client_vepv;
    servant->color_client = color_client;
  
    POA_ColorClient__init (servant, &ev);
    
    CORBA_free (PortableServer_POA_activate_object (gcolorsel_poa, servant, &ev));
    g_assert (ev._major == CORBA_NO_EXCEPTION);
  
    C_CLIENT_PRIVATE (color_client)->client = PortableServer_POA_servant_to_reference (gcolorsel_poa, servant, &ev);
    g_assert (ev._major == CORBA_NO_EXCEPTION);    
  
    if (CORBA_Object_is_nil (C_CLIENT_PRIVATE (color_client)->client, &ev))
  
      *error = c_error_new (C_ERROR_UNKNOWN, C_ERROR_SERVER,
                            _("Failed to get object reference for ColorClient"));          
  }
  
  return C_CLIENT_PRIVATE (color_client)->client;                            
}

static ColorPosList *
g_list_to_pos_list (GList *list)
{
  ColorPosList *pos_list;
  guint length;  

  length = g_list_length (list);
  
  pos_list = ColorPosList__alloc ();
  pos_list->_buffer  = CORBA_sequence_CORBA_unsigned_long_allocbuf (length);
  pos_list->_length  = 0;
  pos_list->_maximum = length; 
  
  CORBA_sequence_set_release (pos_list, TRUE);
      
  while (list) {
  
    pos_list->_buffer[pos_list->_length++] = GPOINTER_TO_INT (list->data);  
  
    list = list->next;
  }

  return pos_list;
}

void 
do_remove (CColorBase *color_base, GList *pos, CError **error)
{
  CORBA_Environment ev;
  ColorPosList *pos_list;
  ColorServer color_server;

  pos_list = g_list_to_pos_list (pos);  
  
  CORBA_exception_init(&ev);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  ColorServer_remove (color_server, C_CLIENT_PRIVATE (color_base)->cnum, 
	              pos_list, &ev);    
	              
  CORBA_free (pos_list);	              
	              
  c_color_client_set_error (&ev, error);
}

void do_move (CColorBase *color_base, guint to,
              GList *pos, CError **error)
{
  CORBA_Environment ev;
  ColorPosList *pos_list;
  ColorServer color_server;

  pos_list = g_list_to_pos_list (pos);  
  
  CORBA_exception_init(&ev);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  ColorServer_move (color_server, C_CLIENT_PRIVATE (color_base)->cnum, 
	            to, pos_list, &ev);    
	              
  CORBA_free (pos_list);
	              
  c_color_client_set_error (&ev, error);
}              

void 
do_insert (CColorBase *color_base, 
	   gushort red, gushort green, gushort blue,
	   const char *name, const char *comment)
{
  CColorClientPrivate *private;
  ColorList *insert_list;
  Color *color;
  
  private = C_CLIENT_PRIVATE (color_base);

  insert_list = private->insert_list_last;

  if (! insert_list) {
  
    insert_list = ColorList__alloc ();
    insert_list->_buffer  = CORBA_sequence_Color_allocbuf (50);
    insert_list->_length  = 0;
    insert_list->_maximum = 50; 

    private->insert_list_last = insert_list;

    private->insert_list = g_list_append (private->insert_list, insert_list);

    CORBA_sequence_set_release (insert_list, TRUE);
  }

  if (insert_list->_length == insert_list->_maximum) {
  
    int max;
    
    max = insert_list->_maximum * 2;

    insert_list = ColorList__alloc ();
    insert_list->_buffer  = CORBA_sequence_Color_allocbuf (max);
    insert_list->_length  = 0;
    insert_list->_maximum = max * 2; 

    private->insert_list_last = insert_list;

    private->insert_list = g_list_append (private->insert_list, insert_list);

    CORBA_sequence_set_release (insert_list, TRUE);
  }
  
  color = &(insert_list->_buffer[insert_list->_length++]);
  
  color->red     = red;
  color->blue    = blue;
  color->green   = green;
  color->name    = CORBA_string_dup (name);
  color->comment = CORBA_string_dup (comment);  
}

void 
do_commit_insert (CColorBase *color_base, guint pos, CError **error)
{
  ColorServer color_server;
  CORBA_Environment ev;
  CColorClientPrivate *private;
  GList *list;
  ColorList *insert_list;
  
  private = C_CLIENT_PRIVATE (color_base);
  
  CORBA_exception_init(&ev);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;

  list = private->insert_list;

  while (list) {

    insert_list = list->data;
    
    ColorServer_insert (color_server, private->cnum, 
			pos, insert_list, &ev);  
			
    if (c_color_client_set_error (&ev, error)) {
      CORBA_free (insert_list);
      return;
    }

    pos = pos + insert_list->_length;
    
    CORBA_free (insert_list);

    list = list->next;

  }

  g_list_free (private->insert_list);

  private->insert_list = NULL;
  private->insert_list_last = NULL;

}

void do_change (CColorBase *color_base, guint pos,
                gushort red, gushort green, gushort blue,
                const char *name, const char *comment, CError **error)
{
  ColorServer color_server;
  CORBA_Environment ev;
  CColorClientPrivate *private;
  Color *color;
  
  private = C_CLIENT_PRIVATE (color_base);
  
  CORBA_exception_init(&ev);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  color = Color__alloc ();
  
  color->red = red;
  color->green = green;
  color->blue = blue;
  color->name = CORBA_string_dup (name);
  color->comment = CORBA_string_dup (comment);
  
  ColorServer_change (color_server, private->cnum, 
	              pos, color, &ev);  
	              
  CORBA_free (color);	               
	              
  c_color_client_set_error (&ev, error);
}
                
void 
do_get (CColorBase *color_base, guint pos,
	gushort *red, gushort *green, gushort *blue,
	char **name, char **comment, CError **error)
{
  CColor *color;

  color = c_palette_get (C_CLIENT_PRIVATE (color_base)->palette, pos, error);
  if (*error) return;
  
  if (red)     *red     = color->red;
  if (blue)    *blue    = color->blue;
  if (green)   *green   = color->green;
  if (name)    *name    = color->name;
  if (comment) *comment = color->comment;
}

guint 
do_get_length (CColorBase *color_base)
{
  return C_CLIENT_PRIVATE (color_base)->palette->nb_elem;
}

void
c_color_client_get_server_info (char **version, GList **info, CError **error)
{
  CORBA_Environment ev;
  ColorServer color_server;
  CORBA_char *c_version;
  ColorInfo *c_info;
  CColorInfo *tmp;
  int i;
  
  CORBA_exception_init (&ev);
  
  color_server = c_color_client_get_server (error);
  if (*error) return;
  
  ColorServer_info (color_server, &c_version, &c_info, &ev);
  
  if (c_color_client_set_error (&ev, error)) {
    return; 
  }
  
  if (version) {
    *version = g_strdup (c_version);
  }
    
  if (info) {
  
    *info = NULL;

    for (i=0; i<c_info->_length; i++) {
    
      tmp = g_new0 (CColorInfo, 1);
      tmp->name = g_strdup (c_info->_buffer[i].name);
      tmp->nb_colors = c_info->_buffer[i].nb_colors;
      tmp->nb_connects = c_info->_buffer[i].nb_connects;
      tmp->read_only = c_info->_buffer[i].read_only;
    
      *info = g_list_prepend (*info, tmp);
    
    }    
  }  
  
  CORBA_free (c_version);
  CORBA_free (c_info);
}

void
c_color_client_info_free (GList *info)
{
  CColorInfo *i;
  GList *tmp = info;
  
  while (tmp) {

    i = tmp->data;
    g_assert (i != NULL);
    g_assert (i->name != NULL);
    
    g_free (i->name);
    g_free (i);
    
    tmp = tmp->next;
  }
  
  g_list_free (info);
}

C_MAKE_TYPE (c_color_client, "CColorClient", CColorClient, class_init, init, PARENT_TYPE)
