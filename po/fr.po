# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Free Software Foundation, Inc.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: gcolorsel2 VERSION\n"
"POT-Creation-Date: 2000-10-14 20:52+0200\n"
"PO-Revision-Date: 2000-07-06 18:02+0200\n"
"Last-Translator: Eric Brayeur <eb@ibelgique.com>\n"
"Language-Team: French\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"

#: backend/c-color-client.c:108
#, c-format
msgid "CORBA Error : '%s'"
msgstr "Erreur CORBA : '%s'"

#: backend/c-color-client.c:449 backend/gcolorseld.c:410
msgid "Failed to get object reference for ColorServer"
msgstr "Impossible d'obtenir une r�f�rence pour l'objet ColorServer"

#: backend/c-color-client.c:484
msgid "Failed to get object reference for ColorClient"
msgstr "Impossible d'obtenir une r�f�rence pour l'objet ColorClient"

#: backend/c-connect.c:211
#, c-format
msgid "Connection number '%d' doesn't exist"
msgstr "La connection avec le num�ro '%d' n'existe pas"

#: backend/c-error.c:29
msgid "Unknown error"
msgstr "Erreur inconnue"

#: backend/c-error.c:30
msgid "Color server couldn't be contacted"
msgstr "Impossible de contacter le serveur de couleur"

#: backend/c-error.c:31
msgid "Bad connection number"
msgstr "Mauvais num�ro de connection"

#: backend/c-error.c:32
msgid "Bad color position"
msgstr "Position de couleur incorrecte"

#: backend/c-error.c:33
msgid "Bad color RGB"
msgstr "Composante RGB incorrecte"

#: backend/c-error.c:34
msgid "Cannot open file"
msgstr "Impossible d'ouvrir le fichier"

#: backend/c-error.c:35
msgid "Cannot save file"
msgstr "Impossible de sauver le fichier"

#: backend/c-error.c:36
msgid "Bad file"
msgstr "Mauvais fichier"

#: backend/c-error.c:37
msgid "Read only"
msgstr "Lecture seule"

#: backend/c-palette.c:121
#, c-format
msgid "Bad position in GET : %d"
msgstr "Mauvaise position dans GET : %d"

#: backend/c-palette.c:135
#, c-format
msgid "Bad positions in MOVE : from = %d to = %d"
msgstr "Mauvaise position dans MOVE : de = %d jusque = %d"

#: backend/c-palette.c:141
#, c-format
msgid "Bad offset in MOVE : from = %d to = %d offset = %d"
msgstr "Mauvais deplacement dans MOVE : de = %d jusque = %d deplacement = %d"

#: backend/c-palette.c:180
#, c-format
msgid "Bad position in INSERT : pos = %d"
msgstr "Mauvaise position dans INSERT : position = %d"

#: backend/c-palette.c:226
msgid "Bad position(s) in REMOVE : must be sorted, and not equal"
msgstr ""
"Mauvaise(s) position(s) dans REMOVE : les positions doivent �tre tri�es et "
"diff�rentes"

#: backend/c-palette.c:241
#, c-format
msgid "Bad position(s) in REMOVE : %d"
msgstr "Mauvaise(s) position(s) dans REMOVE : %d"

#: backend/c-palette.c:286
#, c-format
msgid "Bad position in CHANGE : %d"
msgstr "Mauvaise position dans CHANGE : %d"

#: backend/c-palette.c:340
#, c-format
msgid "File '%s' is not a valid palette file or is corrupted"
msgstr "Le fichier '%s' n'est pas un fichier palette ou il est corrompu"

#: backend/c-palette.c:384 backend/c-palette.c:405
#, c-format
msgid "Error when writting '%s'"
msgstr "Erreur lors de l'�criture de '%s'"

#: backend/c-palette-list.c:137
#, c-format
msgid "Can't open '%s' because TRUNCATE is set but not TRY_CREATE !"
msgstr ""
"Impossible d'ouvrir '%s', TRUNCATE est positionn� mais pas TRY_CREATE !"

#: backend/c-palette-list.c:165
#, c-format
msgid "File '%s' not found and cannot create it !"
msgstr "Fichier '%s' introuvable et impossible de le cr�er !"

#: backend/c-palette-list.c:171
#, c-format
msgid "File '%s' not found !"
msgstr "Fichier '%s' introuvable !"

#: backend/c-palette-list.c:188
#, c-format
msgid "Cannot open file '%s' for read only : permission denied !"
msgstr ""
"Impossible ouvrir le fichier '%s' en lecture seule : permission non-accord�e "
"!"

#: backend/c-palette-list.c:194
#, c-format
msgid "Cannot open file '%s' for read/write : permission denied !"
msgstr ""
"Impossible d'ouvrir le fichier '%s' en lecture/�criture : permission "
"non-accord�e !"

#: backend/c-palette-list.c:201
#, c-format
msgid "Cannot open file '%s' !"
msgstr "Impossible d'ouvrir le fichier '%s' !"

#: backend/c-palette-list.c:229
#, c-format
msgid "# Created with GColorsel %s\n"
msgstr "# Cr�� par GColorsel %s\n"

#: backend/gcolorsel.c:376
msgid "Attempt to init GColorsel a second time"
msgstr "GColorsel a d�j� �t� initialis�"

#: backend/gcolorsel.c:388
msgid "Cannot initialize OAF"
msgstr "Impossible d'initialiser OAF"

#: backend/gcolorsel.c:394
msgid "Cannot resolve initial reference to RootPOA"
msgstr ""

#: backend/gcolorsel.c:401
msgid "Cannot get the POA manager"
msgstr ""

#: backend/gcolorsel.c:409
msgid "Failed to init GColorsel"
msgstr "Impossible d'initialiser GColorsel"

#: backend/gcolorsel.c:426
msgid "Tried to activate GColorsel before initializing"
msgstr "Tentative d'activer GColorsel avant de l'initialiser"

#: backend/gcolorsel.c:433
msgid "Failed to activate the bonobo POA manager"
msgstr ""

#: backend/gcolorseld.c:186
#, c-format
msgid "You cannot change '%s' : read only !"
msgstr "Vous ne pouvez pas modifier '%s' : lecture seule !"

#: backend/gcolorseld.c:395
msgid "Failed to init Object Activation Framework"
msgstr "Impossible d'initialiser OAF (Object Activation Framework)"

#: backend/gcolorseld.c:417
msgid ""
"OAF doesn't know about our IID; indicates broken installation; can't "
"register; exiting\n"
msgstr ""
"OAF ne connait pas notre IID; Indique une mauvaise installation; impossible "
"de s'enregistrer; arr�t\n"

#: backend/gcolorseld.c:420
msgid "Another server (gcolorsel2d) already registered with OAF; exiting\n"
msgstr "Un autre serveur (gcolorsel2d) est d�j� enregistr� par OAF; arr�t\n"

#: backend/gcolorseld.c:423
msgid "Unknown error registering server (gcolorsel2d) with OAF; exiting\n"
msgstr ""
"Erreur inconnue lors de l'enregistrement du serveur (gcolorsel2d) par OAF; "
"arr�t\n"

#: gui/e-clipped-label.c:106
msgid "..."
msgstr "..."

#: gui/c-color-edit.c:567
msgid "Edit"
msgstr "Edition"

#. Labels
#: gui/c-color-edit.c:602
msgid "Red :"
msgstr "Rouge :"

#: gui/c-color-edit.c:603
msgid "Green :"
msgstr "Vert :"

#: gui/c-color-edit.c:604
msgid "Blue :"
msgstr "Bleu"

#: gui/c-color-edit.c:605
msgid "Name :"
msgstr "Nom :"

#: gui/c-color-edit.c:606
msgid "Comment :"
msgstr "Note :"

#: gui/c-color-edit.c:665
msgid "Grab"
msgstr "Saisir"

#: gui/c-color-list.c:47
msgid "Color"
msgstr "Couleur"

#: gui/c-color-list.c:48
msgid "Value"
msgstr "Valeur"

#: gui/c-color-list.c:49
msgid "Name"
msgstr "Nom"

#: gui/c-color-list.c:418
msgid "Dragged"
msgstr "D�pos�e"

#: gui/c-shell.c:296
msgid "Cannot open colors system file for reading !"
msgstr "Impossible de lire le fichier des couleurs syst�mes !"

#: gui/c-shell.c:460
#, c-format
msgid ""
"Error : %s\n"
"\n"
"%s"
msgstr ""
"Erreur : %s\n"
"\n"
"%s"

#: gui/c-shell.c:467
#, c-format
msgid ""
"Oups ! I have got an error message :\n"
"\n"
"%s\n"
"\n"
"Type : %s\n"
"\n"
"This genereally means that the client or the server is buggy !"
msgstr ""
"Oh ! Je viens de recevoir une erreur :\n"
"\n"
"%s\n"
"\n"
"Type : %s\n"
"\n"
"Une telle erreur provient g�n�rallement d'un bug dans le client ou dans le "
"serveur !"

#: gui/c-shell.c:474
#, c-format
msgid ""
"Error : %s\n"
"\n"
"%s\n"
"\n"
"Please, check that the server is correctly installed ..."
msgstr ""
"Erreur : %s\n"
"\n"
"%s\n"
"\n"
"Veuillez v�rifier que le serveur est correctement install� ..."

#: gui/c-shell.c:513
#, c-format
msgid "Cannot change '%s' : read only !"
msgstr "Impossible de modifier '%s' : lecture seule !"

#: gui/c-views.c:154
msgid "There are no palette to show here"
msgstr "Il n'y a pas de palette � montrer i�i"

#: gui/c-views.c:158
msgid "Go in the file menu to create or open one !"
msgstr "Creez/Ouvrez en une � partir du menu fichier !"

#: gui/c-views.c:686
msgid "New color"
msgstr "Nouvelle couleur"

#: gui/gcolorsel2-gui.c:51
msgid "Attempt to init GColorselGui a second time"
msgstr "GColorselGui a d�j� �t� initialis�"

#: gui/gcolorsel2-gui.c:64
msgid "Failed to init GColorselGui"
msgstr "Impossible d'initialiser GColorselGui"
