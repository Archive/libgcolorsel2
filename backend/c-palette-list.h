/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_PALETTE_LIST_H
#define C_PALETTE_LIST_H

typedef struct _CPaletteListData CPaletteListData;

#include "c-palette.h"
#include "c-connect.h"
#include "gcolorsel2.h"

#include <stdio.h>

struct _CPaletteListData {
  CPalette palette;
  
  char *path;  
  FILE *fp;
  gboolean read_only;
  
  GList *connect;
  
  guint timeout;
  
  gboolean modified;
};                                                         

extern GList *palette_list;

CPaletteListData *c_palette_list_connect     (const char *path, 
                                              CConnect *connect, 
                                              gboolean try_create, gboolean truncate,
                                              CError **error);
void              c_palette_list_disconnect  (CConnect *connect);

CPalette         *c_palette_data_get_palette (CPaletteListData *palette_data);

void              c_palette_data_set_modified (CPaletteListData *palette_data);

#endif