/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "gcolorsel2-gui.h"

#include <gcolorsel2.h>

#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

static gboolean have_initted = FALSE;

void
gcolorsel_gui_preinit (gpointer app, gpointer mod_info)
{
}

void
gcolorsel_gui_postinit (gpointer app, gpointer mod_info)
{
  have_initted = TRUE;
}

const char gcolorsel_gui_version[] = VERSION;

gboolean 
gcolorsel_gui_init (int argc, char **argv)
{
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  
  if (have_initted) {
    g_warning (_("Attempt to init GColorselGui a second time"));
    return FALSE;
  }
  
  gcolorsel_gui_preinit (NULL, NULL);
  
  if (! gcolorsel_is_initialized ())
    if (! gcolorsel_init (argc, argv))
      return FALSE;
    
  gcolorsel_gui_postinit (NULL, NULL);
  
  if (! have_initted) {
    g_warning (_("Failed to init GColorselGui"));
    return FALSE;
  }    
    
  return TRUE;    
}

gboolean 
gcolorsel_gui_is_initialized (void)
{
  return have_initted;
}

