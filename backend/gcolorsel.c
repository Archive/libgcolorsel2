/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "gcolorsel2.h"
#include "gcolorsel2-private.h"

#include <glib.h>
#include <gtk/gtkobject.h>
#include <gtk/gtksignal.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>
#include <liboaf/liboaf.h>

#define PARENT_TYPE GTK_TYPE_OBJECT
static GtkObjectClass *parent_class = NULL;

enum {
  FREEZE,
  THAW,
  REMOVE_COLOR,
  INSERT_COLOR,
  CHANGE_COLOR,
  MOVE_COLOR,
  LAST_SIGNAL
};

static guint c_color_base_signals[LAST_SIGNAL] = { 0 };

static void
view_destroyed (GtkObject *view, gpointer *base)
{
  gtk_object_unref (GTK_OBJECT (base));
}
  
void
c_color_base_connect (GtkObject *view, CColorBase *base,
                      GtkSignalFunc freeze, GtkSignalFunc thaw,
                      GtkSignalFunc insert, GtkSignalFunc remove,
                      GtkSignalFunc move, GtkSignalFunc change)
{
  guint length;
  guint i;
  
  g_assert (view != NULL);
  
  gtk_object_ref (GTK_OBJECT (base));
  
  gtk_signal_connect (GTK_OBJECT (view), "destroy",
                      view_destroyed, base);
  
  /* This is a hack to send data to connected view ... in the case where
     the ColorBase have already received the data. 
     If not, view will received data in a proper way ;-) */
     
  if (base->data_received) {
  
    if (freeze) freeze (view, base);
  
    if (insert) {
      length = c_color_base_get_length (base);
     
      for (i=1; i<=length; i++)
	insert (view, i, base);
    }

    if (thaw) thaw (view, base);
    
  } 
    
  if (freeze)
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "freeze",
                                           freeze, GTK_OBJECT (view)); 
                                           
  if (thaw)                                
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "thaw",
                                           thaw, GTK_OBJECT (view)); 

  if (insert)
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "insert_color",
                                           insert, GTK_OBJECT (view));

  if (remove)
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "remove_color",
                                           remove, GTK_OBJECT (view));       
                                           
  if (move)                                
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "move_color",
	                                   move, GTK_OBJECT (view));       

  if (change)
    gtk_signal_connect_object_while_alive (GTK_OBJECT (base), "change_color",
                                           change, GTK_OBJECT (view));   
}

void
c_color_base_emit_freeze (CColorBase *color_base)
{
  if (! color_base->freeze_count++) 
  
    gtk_signal_emit (GTK_OBJECT (color_base), 
                     c_color_base_signals[FREEZE]);
}

void
c_color_base_emit_thaw (CColorBase *color_base)
{
  g_assert (color_base->freeze_count > 0);
  
  if (! --color_base->freeze_count) 
  
      gtk_signal_emit (GTK_OBJECT (color_base), 
                     c_color_base_signals[THAW]);
}

void 
c_color_base_emit_insert (CColorBase *color_base, guint to)
{
  gtk_signal_emit (GTK_OBJECT (color_base), 
		   c_color_base_signals[INSERT_COLOR], to);
}

void
c_color_base_emit_move (CColorBase *color_base, 
			guint from, guint to, int offset)
{
  guint i;
  
  if (from <= to) {
  
    if (offset < 0) {
  
      for (i = from; i <= to; i++)  
  
        gtk_signal_emit (GTK_OBJECT (color_base), 
                         c_color_base_signals[MOVE_COLOR],
                         i, offset); 
                         
    } else
    
      for (i = to; i >= from; i--)
      
        gtk_signal_emit (GTK_OBJECT (color_base),
                         c_color_base_signals[MOVE_COLOR],
                         i, offset);                           
  }
}

void 
c_color_base_emit_remove (CColorBase *color_base, guint pos)
{
  gtk_signal_emit (GTK_OBJECT (color_base),
		   c_color_base_signals[REMOVE_COLOR], pos);
}

void
c_color_base_emit_change (CColorBase *color_base, guint pos)
{
  gtk_signal_emit (GTK_OBJECT (color_base),
                   c_color_base_signals[CHANGE_COLOR], pos);
}                   

typedef void (*GtkSignal_NONE__UINT_INT) (GtkObject *object,
                                          guint arg1,
                                          gint arg2,
                                          gpointer user_data);

static void
c_marshal_NONE__UINT_INT (GtkObject *object,
                          GtkSignalFunc func,
                          gpointer func_data, GtkArg *args)
{
  GtkSignal_NONE__UINT_INT rfunc;
  
  rfunc = (GtkSignal_NONE__UINT_INT) func;
  (*rfunc) (object,
            GTK_VALUE_UINT (args[0]),
            GTK_VALUE_INT  (args[1]),
            func_data);
}                                      

static void 
class_init (CColorBaseClass *class)
{
  GtkObjectClass *object_class;
  
  object_class = (GtkObjectClass *) class;
  
  parent_class = gtk_type_class (GTK_TYPE_OBJECT);
  
  c_color_base_signals[FREEZE] = 
    gtk_signal_new ("freeze",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, freeze),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
                    
  c_color_base_signals[THAW] = 
    gtk_signal_new ("thaw",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, thaw),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);
  
  c_color_base_signals[REMOVE_COLOR] = 
    gtk_signal_new ("remove_color",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, remove_color),
                    gtk_marshal_NONE__UINT,
                    GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
                    
  c_color_base_signals[INSERT_COLOR] = 
    gtk_signal_new ("insert_color",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, insert_color),
                    gtk_marshal_NONE__UINT,
                    GTK_TYPE_NONE, 1, GTK_TYPE_UINT);
                    
  c_color_base_signals[CHANGE_COLOR] = 
    gtk_signal_new ("change_color",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, change_color),
                    gtk_marshal_NONE__UINT,
                    GTK_TYPE_NONE, 1, GTK_TYPE_UINT);                                        
                    
  c_color_base_signals[MOVE_COLOR] = 
    gtk_signal_new ("move_color",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorBaseClass, move_color),
                    c_marshal_NONE__UINT_INT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_UINT, GTK_TYPE_INT);                    

  gtk_object_class_add_signals (object_class, c_color_base_signals, LAST_SIGNAL);                    
}

static void 
init (CColorBase *color_base)
{
  color_base->freeze_count = 0;
  color_base->data_received = FALSE;
  color_base->read_only = FALSE;  
}

CColorBase *
c_color_base_new ()
{
  return C_COLOR_BASE (gtk_type_new (c_color_base_get_type ()));
}

void
c_color_base_insert (CColorBase *color_base,
		     gushort red, gushort green, gushort blue,  
		     const char *name, const char *comment)
{
  /* FIXME : NULL ? */

  C_COLOR_BASE_GET_CLASS (color_base)->do_insert (color_base, 
						  red, green, blue,
						  name, comment);
}

void 
c_color_base_remove (CColorBase *color_base, GList *pos, CError **error)
{
  /* FIXME : NULL ? */

  C_COLOR_BASE_GET_CLASS (color_base)->do_remove (color_base, pos, error);
}

void
c_color_base_move (CColorBase *color_base, guint to,
                   GList *pos, CError **error)
{
  /* FIXME : NULL ? */
  
  C_COLOR_BASE_GET_CLASS (color_base)->do_move (color_base, to, pos, error);
}                   

void
c_color_base_commit_insert (CColorBase *color_base, guint pos, CError **error)
{
  /* FIXME : NULL ? */

  C_COLOR_BASE_GET_CLASS (color_base)->do_commit_insert (color_base, 
							 pos, error);
}

void 
c_color_base_change (CColorBase *color_base, guint pos,
                     gushort red, gushort green, gushort blue,
                     const char *name, const char *comment, CError **error)
{
  C_COLOR_BASE_GET_CLASS (color_base)->do_change (color_base, pos,
                                                  red, green, blue,
                                                  name, comment, error);
}                     

void 
c_color_base_get (CColorBase *color_base, guint pos,
                    gushort *red, gushort *green, gushort *blue,
                    char **name, char **comment, CError **error)
{
  /* FIXME : NULL ? */

  C_COLOR_BASE_GET_CLASS (color_base)->do_get (color_base, pos,
					       red, green, blue, 
					       name, comment, error);
}

guint
c_color_base_get_length (CColorBase *color_base)
{
  /* FIXME : NULL ? */

  return C_COLOR_BASE_GET_CLASS (color_base)->do_get_length (color_base);
}

C_MAKE_TYPE (c_color_base, "CColorBase", CColorBase, class_init, init, PARENT_TYPE)

/* Library */

const char *gcolorsel_system_paths[] = { "/usr/lib/X11/rgb.txt", NULL };

char *
gcolorsel_get_user_file_path (void)
{
  return g_strconcat (g_get_home_dir (), "/.gcolorsel2.pal", NULL); 
}

static gboolean have_initted = FALSE;

void
gcolorsel_preinit (gpointer app, gpointer mod_info)
{
}

void
gcolorsel_postinit (gpointer app, gpointer mod_info)
{
  have_initted = TRUE;
}                                                                      

const char gcolorsel_version[] = VERSION;

CORBA_ORB                 gcolorsel_orb = CORBA_OBJECT_NIL;
PortableServer_POA        gcolorsel_poa;
PortableServer_POAManager gcolorsel_poa_manager = NULL;

gboolean
gcolorsel_init (int argc, char **argv)
{
  CORBA_Environment ev;
  
  CORBA_exception_init (&ev);
  
  if (have_initted) {
    g_warning (_("Attempt to init GColorsel a second time"));
    return FALSE;
  }
  
  gcolorsel_preinit (NULL, NULL);
  
  if (! oaf_is_initialized ()) 
    gcolorsel_orb = oaf_init (argc, argv);
  else
    gcolorsel_orb = oaf_orb_get ();
    
  if (gcolorsel_orb == CORBA_OBJECT_NIL) {
    g_warning (_("Cannot initialize OAF"));
    return FALSE;
  }
  
  gcolorsel_poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references (gcolorsel_orb, "RootPOA", &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    g_warning (_("Cannot resolve initial reference to RootPOA"));
    CORBA_exception_free (&ev);
    return FALSE;
  }  
  
  gcolorsel_poa_manager = PortableServer_POA__get_the_POAManager (gcolorsel_poa, &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    g_warning (_("Cannot get the POA manager"));
    CORBA_exception_free (&ev);
    return FALSE;
  }
  
  gcolorsel_postinit (NULL, NULL);
  
  if (! have_initted) {
    g_warning (_("Failed to init GColorsel"));
    return FALSE;
  }

  return TRUE;  
}

gboolean
gcolorsel_activate (void)
{
  CORBA_Environment ev;
  
  CORBA_exception_init (&ev);
  
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  
  if (! gcolorsel_poa_manager) {
    g_warning (_("Tried to activate GColorsel before initializing"));
    CORBA_exception_free (&ev);
    return FALSE;
  }
  
  PortableServer_POAManager_activate (gcolorsel_poa_manager, &ev);
  if (ev._major != CORBA_NO_EXCEPTION) {
    g_warning (_("Failed to activate the bonobo POA manager"));
    CORBA_exception_free (&ev);
    return FALSE;
  }  
  
  CORBA_exception_free (&ev);
  
  return TRUE;
}

gboolean
gcolorsel_is_initialized (void)
{
  return have_initted;
}

