/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-palette-list.h"

#include "c-palette.h"
#include "c-connect.h"
#include "gcolorsel2.h"

#include <errno.h>
#include <stdio.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

#define TIMEOUT 5000

GList *palette_list = NULL;

static CPaletteListData *
c_palette_list_get (const char *path)
{
  CPaletteListData *palette_data;
  GList    *tmp;
  
  g_assert (path != NULL);
  
  tmp = palette_list;
  
  while (tmp) {

    palette_data = tmp->data;
  
    if (! strcmp (path, palette_data->path)) return palette_data;
  
    tmp = tmp->next;
  }
   
  return NULL;
}

static gint
c_palette_timeout (gpointer data)
{
  CPaletteListData *palette_data;
  CError *error = NULL;
    
  palette_data = data;
  
  if (palette_data->modified) { 
    g_assert (palette_data->read_only == FALSE);
    
    if (ftruncate (fileno (palette_data->fp), 0)) {
      g_warning ("Save error : can't truncate '%s' !\n", palette_data->path);
      return FALSE;
    }
    
    rewind (palette_data->fp);    
    
    c_palette_save (palette_data->fp, (CPalette *)palette_data, palette_data->path, &error);
      
    if (error) {
      g_warning ("Save error : %s", error->message);      
      return FALSE;
    }
    
    fflush (palette_data->fp);
    
    palette_data->modified = FALSE;
  }
  
  if (! palette_data->connect) {
    palette_list = g_list_remove (palette_list, palette_data);    
    
    fclose (palette_data->fp);
    g_free (palette_data->path);       
    c_palette_free ((CPalette *)palette_data);

  } else 
  
    palette_data->timeout = 0;
  
  return FALSE;
}

static void
c_palette_list_install_timeout (CPaletteListData *palette_data)
{ 
  if (palette_data->timeout)
  
    g_source_remove (palette_data->timeout);
    
  palette_data->timeout = g_timeout_add_full (0, TIMEOUT, c_palette_timeout, 
                                              palette_data, NULL);
}

void
c_palette_data_set_modified (CPaletteListData *palette_data)
{
  g_assert (palette_data != NULL);
  g_assert (palette_data->read_only == FALSE);
  
  palette_data->modified = TRUE;
  
  c_palette_list_install_timeout (palette_data);
}

static FILE *
c_palette_list_open (const char *path, gboolean try_create, gboolean truncate,
                     gboolean *read_only, gboolean *created, CError **error)
{
  FILE *fp;
  
  *created = FALSE;  
  *read_only = FALSE;
  
  if (truncate) {
  
    if (! try_create) {
      *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_BUG,
                            _("Can't open '%s' because TRUNCATE is set but not TRY_CREATE !"), path);
      return NULL;
    }
    
    fp = fopen (path, "w+");
    if (fp) {
      *created = TRUE;
      return fp;
    }
    
  } else {
   
    fp = fopen (path, "r+");
    if (fp) return fp;
    
  }
  
  if (errno == ENOENT) { /* File not found */
    
    if (try_create) {
    
      fp = fopen (path, "w+");
      if (fp) {
        *created = TRUE;
        return fp;
      }
      
      *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                            _("File '%s' not found and cannot create it !"), path);     

      return NULL;
    }

    *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                          _("File '%s' not found !"), path);     
                          
    return NULL;                          
    
  } else {
    
    if (errno == EACCES) { /* Permission denied */
    
      if (! try_create) {
   
        fp = fopen (path, "r");
        if (fp) {
          *read_only = TRUE;
          return fp;
        }             
        
        *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                            _("Cannot open file '%s' for read only : permission denied !"), path); 

        return NULL;      
      }

      *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                            _("Cannot open file '%s' for read/write : permission denied !"), path); 

      return NULL;          
    }
  }
      
  *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                        _("Cannot open file '%s' !"), path);
      
  return NULL;
}
 
CPaletteListData *
c_palette_list_connect (const char *path, CConnect *connect, 
                        gboolean try_create, gboolean truncate, CError **error)
{
  CPaletteListData *palette_data;
  CPalette *palette;
  FILE *fp;
  gboolean read_only;
  gboolean created;
  
  g_assert (path != NULL);
  g_assert (connect != NULL);
    
  palette_data = c_palette_list_get (path);
  
  if (! palette_data) {
  
    fp = c_palette_list_open (path, try_create, truncate, &read_only, &created, error);     
    if (*error) return NULL;
    
    if (created) {
    
      palette = c_palette_new ();
      palette->header = g_strdup_printf (_("# Created with GColorsel %s\n"), VERSION);

    } else {
  
      palette = c_palette_load (fp, path, error);    
      if (*error) return NULL;
      
    }
    
    palette_data = g_realloc (palette, sizeof (CPaletteListData));
    palette_data->path      = g_strdup (path);
    palette_data->fp        = fp;
    palette_data->read_only = read_only;
    palette_data->connect   = NULL;
    palette_data->timeout   = 0;
    palette_data->modified  = FALSE;
      
    palette_list = g_list_prepend (palette_list, palette_data);
    
    if (created) 
      c_palette_data_set_modified (palette_data);
  }
  
  connect->palette = palette_data;
  
  palette_data->connect = g_list_prepend (palette_data->connect, connect);

  return palette_data;  
}

void
c_palette_list_disconnect (CConnect *connect)
{
  CPaletteListData *palette_data;
  
  g_assert (connect != NULL);
  g_assert (connect->palette != NULL);
  
  palette_data = connect->palette;
  
  palette_data->connect = g_list_remove (palette_data->connect, connect);
  
  if (! palette_data->connect) {
  
    printf ("Disconnect : '%s'\n", palette_data->path);
  
    c_palette_list_install_timeout (palette_data);
  }

  connect->palette = NULL;
}

CPalette *
c_palette_data_get_palette (CPaletteListData *palette_data)
{
  g_assert (palette_data != NULL);
  
  return &(palette_data->palette);
}

