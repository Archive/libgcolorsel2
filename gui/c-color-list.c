/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-color-list.h"

#include "c-shell.h"
#include "c-utils.h"

#include <gcolorsel2.h>

#include <stdio.h>
#include <gnome.h>

#define DEFAULT_COLOR_WIDTH  48
#define DEFAULT_COLOR_HEIGHT 15

enum {
  COLUMN_COLOR = 0,
  COLUMN_VALUE,
  COLUMN_NAME,
  COLUMN_LAST
};

static gint c_color_list_compare_color (GtkCList *clist, 
                                        gconstpointer ptr1, gconstpointer ptr2);
static gint c_color_list_compare_value (GtkCList *clist,
                                        gconstpointer ptr1, gconstpointer ptr2);

static CListColumn col_name[] = {
  { N_("Color"), c_color_list_compare_color,  DEFAULT_COLOR_WIDTH, FALSE },
  { N_("Value"), c_color_list_compare_value,  75,                  TRUE  },
  { N_("Name"),  NULL,                        60,                  TRUE  },
  { NULL,        NULL,                        0,                   FALSE }
};

#define TARGET_COLOR       69
#define TARGET_COLOR_LIST  96

static GtkTargetEntry targets_table[] = {
  { "application/x-color",      0, TARGET_COLOR },
  { "application/x-color-list", 0, TARGET_COLOR_LIST}
};

enum {
  DO_ERROR,
  DO_SELECT_CHANGE,
  LAST_SIGNAL
};
  
#define PARENT_TYPE C_LIST_TYPE
static CListClass *parent_class = NULL;
static guint c_color_list_signals[LAST_SIGNAL] = { 0 };

static void     c_color_list_realize                (GtkWidget *widget);
static void     c_color_list_append_color_to_redraw (CColorList *color_list, guint pos);
static gint     c_color_base_idle                   (gpointer data);
static void     c_color_list_select_row             (GtkCList *clist, gint row, gint col, GdkEvent *event);
static void     c_color_list_unselect_row           (GtkCList *clist, gint row, gint col, GdkEvent *event);

static void     c_color_list_get_drag_widget        (CList *clist, GtkWidget **widget);
static void     c_color_list_data_get               (CList *clist, GtkSelectionData *data, int info);
static gpointer c_color_list_get_data_type          (CList *clist);
static void     c_color_list_do_insert              (CList *clist, GtkSelectionData *data, guint pos);
static void     c_color_list_do_move                (CList *clist, GtkSelectionData *data, guint to);

static gint     
insert_compare_func (gconstpointer ptr1, gconstpointer ptr2)
{
  if (GPOINTER_TO_INT (ptr1) > GPOINTER_TO_INT (ptr2)) return 1;
  if (GPOINTER_TO_INT (ptr1) < GPOINTER_TO_INT (ptr2)) return -1;  
  
  return 0;
}

static void
c_color_list_log_error (CColorList *color_list, CError *error)
{
  gtk_signal_emit (GTK_OBJECT (color_list), c_color_list_signals[DO_ERROR], error);
}

static void
base_freeze (CColorList *color_list, CColorBase *color_base)
{
  gtk_clist_freeze (GTK_CLIST (color_list));
  
  if (color_list->add_one_freeze_next) {
  
    if (! --color_list->add_one_freeze_next) {
      gtk_clist_freeze (GTK_CLIST (color_list));
    }
    
  }
}

static void
base_thaw (CColorList *color_list, CColorBase *color_base)
{
  GtkCList *list;
  
  list = GTK_CLIST (color_list);
  
  if (list->row_list) 
    gtk_clist_sort (list);    
  
  gtk_clist_thaw (list);
  
  if (color_list->add_one_thaw_next) {
  
    if (! --color_list->add_one_thaw_next) {
      gtk_clist_thaw (list); 
    }
  }
}

static void
base_remove_color (CColorList *color_list, guint pos, CColorBase *color_base)
{
  gint row;
  GtkCList *clist;
  
  clist = GTK_CLIST (color_list);
  
  if ((clist->row_list) && (clist->row_list->next)) {
  
    row = gtk_clist_find_row_from_data (clist, GINT_TO_POINTER (pos));

    gtk_clist_remove (clist, row);    

  } else gtk_clist_clear (clist);
}

static void
base_move_color (CColorList *color_list, guint pos, int offset, CColorBase *color_base)
{
  gint row;
  
  row = gtk_clist_find_row_from_data (GTK_CLIST (color_list), GINT_TO_POINTER (pos));
  
  gtk_clist_set_row_data (GTK_CLIST (color_list), row, GINT_TO_POINTER (pos + offset));
  
  c_color_list_append_color_to_redraw (color_list, pos + offset);
}

static void
base_insert_color (CColorList *color_list, guint pos, CColorBase *color_base)
{
  char *text [COLUMN_LAST];
  gint row;  
  gushort red, green, blue;
  char *name;
  char *comment;
  CError *error = NULL;

  c_color_base_get (color_base, pos, &red, &green, &blue, 
		    &name, &comment, &error);
		    
  if (error) { /* This should not append ! */    
    c_color_list_log_error (color_list, error);
    return;
  }		    
  
  text[COLUMN_COLOR] = NULL;
  text[COLUMN_NAME]  = name;
  text[COLUMN_VALUE] = g_strdup_printf ("%d %d %d", red, green, blue);
  
  row = gtk_clist_prepend (GTK_CLIST (color_list), text);
  
  g_free (text[COLUMN_VALUE]);
  
  gtk_clist_set_row_data (GTK_CLIST (color_list), row, GINT_TO_POINTER (pos));

  c_color_list_append_color_to_redraw (color_list, pos);
}

static void
base_change_color (CColorList *list, guint pos, CColorBase *color_base)
{
  gint row;  
  gushort red, green, blue;
  char *name;
  char *comment;
  char *value;
  CError *error = NULL;

  c_color_base_get (color_base, pos, &red, &green, &blue, 
		    &name, &comment, &error);
		    
  if (error) { /* This should not append ! */
    c_color_list_log_error (list, error);
    return;
  }		    
  
  row = gtk_clist_find_row_from_data (GTK_CLIST (list), GINT_TO_POINTER (pos));
  
  value = g_strdup_printf ("%d %d %d", red, green, blue);
  
  gtk_clist_set_text (GTK_CLIST (list), row, COLUMN_NAME, name);
  gtk_clist_set_text (GTK_CLIST (list), row, COLUMN_VALUE, value);
  
  g_free (value);
  
  c_color_list_append_color_to_redraw (list, pos);
}

/* GtkObject methods */

static void
destroy (GtkObject *object)
{
  CColorList *color_list;
  
  color_list = C_COLOR_LIST (object);
  
  gdk_font_unref (color_list->font);
  
  if (color_list->idle)
    gtk_idle_remove (color_list->idle);
    
  if (color_list->idle_select)
    gtk_idle_remove (color_list->idle_select);    
    
  if (color_list->colors_to_redraw)
    g_list_free (color_list->colors_to_redraw);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
class_init (CColorListClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkCListClass  *clist_class;
  CListClass     *list_class;
  
  object_class = (GtkObjectClass *) class;
  widget_class = (GtkWidgetClass *) class;
  clist_class  = (GtkCListClass *) class;
  list_class   = (CListClass *) class;
  
  parent_class = gtk_type_class (C_LIST_TYPE);

  c_color_list_signals[DO_ERROR] = 
    gtk_signal_new ("do_error",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorListClass, do_error),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);                    
                    
  c_color_list_signals[DO_SELECT_CHANGE] = 
    gtk_signal_new ("do_select_change",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorListClass, do_error),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);                                        
                                        
  gtk_object_class_add_signals (object_class, c_color_list_signals, LAST_SIGNAL);                    

  object_class->destroy = destroy;
  
  widget_class->realize = c_color_list_realize;      
  
  clist_class->select_row = c_color_list_select_row;
  clist_class->unselect_row = c_color_list_unselect_row;    
  
  list_class->get_drag_widget = c_color_list_get_drag_widget;
  list_class->do_insert = c_color_list_do_insert;
  list_class->do_move = c_color_list_do_move;
  list_class->data_get = c_color_list_data_get;
  list_class->get_data_type = c_color_list_get_data_type;
}

static void
init (CColorList *color_list)
{
  static gboolean one = FALSE;
  int i = 0;
  
  color_list->color_base = NULL;
  
  color_list->add_one_freeze_next = 0;
  color_list->add_one_thaw_next = 0;
  
//  gtk_widget_add_events (GTK_WIDGET (color_list), GDK_POINTER_MOTION_MASK);
  
  color_list->font = gdk_font_load ("-bitstream-courier-bold-r-normal-*-13-*-*-*-*-*-*-*");

  color_list->width  = DEFAULT_COLOR_WIDTH;
  color_list->height = DEFAULT_COLOR_HEIGHT;  
  
  color_list->colors_to_redraw = NULL;
  color_list->idle = 0;
  
  color_list->idle_select = 0;
  
  if (! one) {
  
    while (col_name[i].name) {

      col_name[i].name = _(col_name[i].name);    
    
      i++;
    }
  
    one = TRUE;
  }
}

static void     
c_color_list_get_drag_widget (CList *clist, GtkWidget **widget)
{
  CError *error = NULL;
  gushort red, green, blue;
  guint col = 0;
  guint tmp;
  GList *list;
  int nb = 0; 
  char *str = NULL;
  
  list = GTK_CLIST (clist)->selection;
  if (! list) return;
  
  while (list) {

    tmp = GPOINTER_TO_INT (gtk_clist_get_row_data (GTK_CLIST (clist), GPOINTER_TO_INT (list->data)));
    if (col)
      col = MIN (col, tmp);
    else
      col = tmp;
  
    nb++;
    list = list->next;
  }
  
  c_color_base_get (C_COLOR_LIST (clist)->color_base, col,
                    &red, &green, &blue, NULL, NULL, &error);
                    
  if (error) {
    c_color_list_log_error (C_COLOR_LIST (clist), error);
  }                    

  if (nb > 1) {
    str = g_strdup_printf ("%d", nb);  
  }
  
  *widget = c_utils_widget_rgb (red, green, blue, str);
  
  if (str) g_free (str);
}

static void     
c_color_list_data_get (CList *clist, GtkSelectionData *data, int info)
{
  gushort red, green, blue;
  CError *error = NULL;
  guint16 *vals, *tmp_vals;
  GList *list, *tmp_list;
  gint length;
  
  tmp_list = list = c_color_list_get_selected (C_COLOR_LIST (clist), &length);
  
  if (info == TARGET_COLOR) length = 1;
  
  tmp_vals = vals = g_new (guint16, 8 * length);
  
  while (list) {
  
    c_color_base_get (C_COLOR_LIST (clist)->color_base, GPOINTER_TO_INT (list->data),
                      &red, &green, &blue, NULL, NULL, &error);
                    
    if (error) {
      c_color_list_log_error (C_COLOR_LIST (clist), error);
      red = green = blue = 0;      
    }                                        
  
    *vals = (red   / 255.0) * 0xffff; vals++;
    *vals = (green / 255.0) * 0xffff; vals++; 
    *vals = (blue  / 255.0) * 0xffff; vals++; 
    *vals = 0xffff; vals++;
    
    if (info == TARGET_COLOR) break;
    
    list = list->next; 
  }

  gtk_selection_data_set (data,
                          info,
                          16, (guchar *) tmp_vals, 8 * length);

  g_list_free (tmp_list);                          
  g_free (tmp_vals);                          
}

static gpointer
c_color_list_get_data_type (CList *clist)
{
  GtkCList *list;
  
  list = GTK_CLIST (clist);
  if (list->selection && list->selection->next) {
  
    return gtk_target_list_new (&(targets_table[1]), 1);
  
  } 
   
  return gtk_target_list_new (&(targets_table[0]), 1);
}

static void     
c_color_list_do_insert (CList *clist, GtkSelectionData *data, guint pos)
{
  guint col;
  guint16 *vals;  
  CError *error = NULL;
  gushort red, green, blue;
  int nb = 0;
  
  col = GPOINTER_TO_INT (gtk_clist_get_row_data (GTK_CLIST (clist), pos - 1));  
  if (! col) {
    col = c_color_base_get_length (C_COLOR_LIST (clist)->color_base) + 1;
  }
  
  vals = (guint16 *)data->data;
  
  while (nb < data->length / 8) {

    red   = (*vals * 255.0) / 0xffff; vals++;
    green = (*vals * 255.0) / 0xffff; vals++;
    blue  = (*vals * 255.0) / 0xffff; vals++;
    vals++;
  
    c_color_base_insert (C_COLOR_LIST (clist)->color_base,
                         red, green, blue, _("Dragged"), "");
             
    nb++;                         
  }                         
                       
  c_color_base_commit_insert (C_COLOR_LIST (clist)->color_base, col, &error);                       
                       
  if (error) {
    c_color_list_log_error (C_COLOR_LIST (clist), error);
  }                                                               
}

static void     
c_color_list_do_move (CList *clist, GtkSelectionData *data, guint to)
{
  CError *error = NULL;
  CColorBase *color_base;
  GList *list;
  
  color_base = C_COLOR_LIST (clist)->color_base;
  
  list = c_color_list_get_selected (C_COLOR_LIST (clist), NULL);

  if (list) {
  
    c_color_base_move (color_base, to, list, &error);
    if (error) {
      c_color_list_log_error (C_COLOR_LIST (clist), error);
    }
    g_list_free (list);
  
  }
}

static GtkSelectionMode
c_select_to_gtk_select (CSelectType select_type)
{
  switch (select_type) {
   
    case C_SELECT_SINGLE:
      return GTK_SELECTION_SINGLE;
    
    case C_SELECT_MULTIPLE:
      return GTK_SELECTION_EXTENDED;
      
  }
  
  g_assert_not_reached ();

  return GTK_SELECTION_MULTIPLE;
}

void
c_color_list_construct (CColorList *color_list, CColorBase *color_base,
                        CSelectType select_type)
{
  g_return_if_fail (color_list != NULL);
  g_return_if_fail (color_base != NULL);

  c_list_construct (C_LIST (color_list), col_name, COLUMN_COLOR,  
                    targets_table, sizeof (targets_table) / sizeof (GtkTargetEntry));
  
  color_list->color_base = color_base;  

  gtk_clist_set_row_height (GTK_CLIST (color_list), color_list->height);  
  gtk_clist_set_selection_mode (GTK_CLIST (color_list), 
                                c_select_to_gtk_select (select_type));
                                
  c_color_base_connect (GTK_OBJECT (color_list), color_base, 
                        base_freeze, base_thaw,
                        base_insert_color, base_remove_color,
                        base_move_color, base_change_color);
}

CColorList *
c_color_list_new (CColorBase *color_base, CSelectType select_type)
{
  CColorList *list;
  
  g_return_val_if_fail (color_base != NULL, NULL);
    
  list = C_COLOR_LIST (gtk_type_new (c_color_list_get_type ()));
  
  c_color_list_construct (list, color_base, select_type);
  
  return list;
}

static void
c_color_list_realize (GtkWidget *widget)
{
  if (C_COLOR_LIST (widget)->colors_to_redraw)
  
      C_COLOR_LIST (widget)->idle = gtk_idle_add_priority (GTK_PRIORITY_LOW, c_color_base_idle, widget);  
      
  if (GTK_WIDGET_CLASS (parent_class)->realize)
    GTK_WIDGET_CLASS (parent_class)->realize (widget);      
}

static gint
c_color_list_idle_select (gpointer data)
{
  CColorList *color_list;

  color_list = C_COLOR_LIST (data);
  
  color_list->idle_select = 0;

  gtk_signal_emit (GTK_OBJECT (color_list), c_color_list_signals[DO_SELECT_CHANGE]);  
  
  return FALSE;
}

static void 
c_color_list_select_row (GtkCList *clist, gint row, gint col, GdkEvent *event)
{
  CColorList *color_list;
  
  GTK_CLIST_CLASS (parent_class)->select_row (clist, row, col, event);  
  
  color_list = C_COLOR_LIST (clist);
  
  if (! color_list->idle_select) {
    color_list->idle_select = gtk_idle_add_priority (GTK_PRIORITY_LOW, c_color_list_idle_select, color_list);
  }
}

static void 
c_color_list_unselect_row (GtkCList *clist, gint row, gint col, GdkEvent *event)
{
  CColorList *color_list;
  
  GTK_CLIST_CLASS (parent_class)->unselect_row (clist, row, col, event);  
  
  color_list = C_COLOR_LIST (clist);
  
  if (! color_list->idle_select) {
    color_list->idle_select = gtk_idle_add_priority (GTK_PRIORITY_LOW, c_color_list_idle_select, color_list);
  }
}

static gint
c_color_list_compare_color (GtkCList *clist, 
                            gconstpointer ptr1, gconstpointer ptr2)
{
  GtkCListRow *row1, *row2;
  guint pos1, pos2;
  
  row1 = (GtkCListRow *) ptr1;
  row2 = (GtkCListRow *) ptr2;
  
  g_assert (row1 != NULL);
  g_assert (row2 != NULL);
  
  pos1 = GPOINTER_TO_INT (row1->data);
  pos2 = GPOINTER_TO_INT (row2->data);
  
  if (pos1 < pos2) return -1;
  if (pos1 > pos2) return 1;
  
  return 0;
}
                                        
static gint 
c_color_list_compare_value (GtkCList *clist,
                            gconstpointer ptr1, gconstpointer ptr2)
{
  GtkCListRow *row1, *row2;
  guint pos1, pos2;
  gushort red, green, blue;
  guint rgb1, rgb2;
  CError *error = NULL;
  
  row1 = (GtkCListRow *) ptr1;
  row2 = (GtkCListRow *) ptr2;
  
  pos1 = GPOINTER_TO_INT (row1->data);
  pos2 = GPOINTER_TO_INT (row2->data);
  
  c_color_base_get (C_COLOR_LIST (clist)->color_base, pos1, &red, &green, &blue, NULL, NULL, &error);
  rgb1 = red * green * blue;
  
  if (error) { /* This should not append ! */
    c_color_list_log_error (C_COLOR_LIST (clist), error);
    return 0;
  }		    
    
  c_color_base_get (C_COLOR_LIST (clist)->color_base, pos2, &red, &green, &blue, NULL, NULL, &error);
  rgb2 = red * green * blue;  
  
  if (error) { /* This should not append ! */
    c_color_list_log_error (C_COLOR_LIST (clist), error);
    return 0;
  }		    

  if (rgb1 < rgb2) return -1;
  if (rgb1 > rgb2) return 1;
  
  return 0;
}                            

static GdkPixmap *
c_color_list_render_pixmap (CColorList *color_list, guint pos,
                            gushort red, gushort green, gushort blue,                            
                            int width, int height, GdkFont *font)
{
  GdkColor color;
  GdkGC *gc;
  GtkStyle *style;
  GdkPixmap *pixmap;
  char buf[20];
  int h, w;
  
  color.red   = red   * 255;
  color.green = green * 255;
  color.blue  = blue  * 255;
  
  gdk_color_alloc (gtk_widget_get_colormap (GTK_WIDGET (color_list)), &color);

  pixmap = gdk_pixmap_new (GTK_WIDGET (color_list)->window, width, height, -1);      
  gc = gdk_gc_new (GTK_WIDGET (color_list)->window);
  
  style = gtk_widget_get_style (GTK_WIDGET (color_list));
  
  gdk_gc_set_foreground (gc, &style->black);
  gdk_draw_rectangle (pixmap, gc, FALSE,        
                      0, 0, width - 1, height - 1); 
                      
  gdk_gc_set_foreground (gc, &color);  
  gdk_draw_rectangle (pixmap, gc, TRUE, 1, 1, width - 2, height - 2);
  
  if (font) {
  
    if (red * green * blue > (255 * 255 * 255) / 2)
      gdk_gc_set_foreground (gc, &style->black);  
    else
      gdk_gc_set_foreground (gc, &style->white);
    
    sprintf (buf, "%d", pos);
    
    h = gdk_string_height (font, buf);
    w = gdk_string_width  (font, buf);
    
    gdk_draw_string (pixmap, font, gc, 
                     (width - w) / 2, (height - h) / 2 + h, buf);
  }
  
  gdk_gc_unref (gc);
  
  return pixmap;
}

static gint
c_color_base_idle (gpointer data)
{
  CColorList *color_list;
  GdkPixmap *pixmap;
  gushort red, green, blue;
  guint pos;
  int row;
  CError *error = NULL;
  
  /* FIXME : peut etre plusieurs couleurs en 1 fois ? */
  
  color_list = C_COLOR_LIST (data);
  
  g_assert (color_list->colors_to_redraw != NULL);
  
  data = color_list->colors_to_redraw->data;
  pos = GPOINTER_TO_INT (data);
  color_list->colors_to_redraw = g_list_remove (color_list->colors_to_redraw, data);

  row = gtk_clist_find_row_from_data (GTK_CLIST (color_list), data);

  if (row != -1) {
   
    c_color_base_get (color_list->color_base, pos, &red, &green, &blue, NULL, NULL, &error);

    if (error) { /* This should not append ! */
      c_color_list_log_error (color_list, error);
      return TRUE;
    }		    
  
    pixmap = c_color_list_render_pixmap (color_list, pos, red, green, blue,
                                         color_list->width, color_list->height, 
                                         color_list->font);
                                   
    gtk_clist_set_pixmap (GTK_CLIST (color_list), row, COLUMN_COLOR, pixmap, NULL);
    gdk_pixmap_unref (pixmap);                                         
    
  }   
                                           
  if (! color_list->colors_to_redraw) {
    gtk_idle_remove (color_list->idle);
    color_list->idle = 0;
  }          
  
  return TRUE;
}

static void
c_color_list_append_color_to_redraw (CColorList *color_list, guint pos)
{
  g_assert (color_list != NULL);
  
  if ((! color_list->idle) && (GTK_WIDGET_REALIZED (color_list)))
    color_list->idle = gtk_idle_add_priority (GTK_PRIORITY_LOW, c_color_base_idle, color_list);
    
  color_list->colors_to_redraw = g_list_prepend (color_list->colors_to_redraw, 
                                                GINT_TO_POINTER (pos));
}

void
c_color_list_clear_selection (CColorList *color_list, gboolean block_signal)
{
  gtk_clist_unselect_all (GTK_CLIST (color_list));
  
  if (block_signal) {
    gtk_idle_remove (color_list->idle_select);
    color_list->idle_select = 0;
  }
}

gboolean
c_color_list_have_selected (CColorList *color_list)
{
  return GTK_CLIST (color_list)->selection != NULL;
}

GList *
c_color_list_get_selected (CColorList *color_list, int *length)
{
  GList *list = NULL;
  GList *selection;
  int row;
  
  g_assert (color_list != NULL);
  
  selection = GTK_CLIST (color_list)->selection_end;
  
  if (length) {
    *length = 0;
  }
  
  while (selection) {
  
    row = GPOINTER_TO_INT (selection->data);   
    
    if (row != -1) { /* Append when selection_mode == GTK_SELECTION_BROWSE */
  
      list = g_list_insert_sorted (list, gtk_clist_get_row_data (GTK_CLIST (color_list), row), insert_compare_func);
    
      if (length) {  
        *length = *length + 1;
      }
      
    }
  
    selection = selection->prev;
  }
  
  return list;
}

C_MAKE_TYPE (c_color_list, "CColorList", CColorList, class_init, init, PARENT_TYPE)
