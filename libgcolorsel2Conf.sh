prefix=/opt/gnome/
exec_prefix=${prefix}

LIBGCOLORSEL2_INCLUDEDIR="-I${prefix}/include/libgcolorsel2"
LIBGCOLORSEL2_LIBDIR="-L${exec_prefix}/lib"
LIBGCOLORSEL2_LIBS="-lgcolorsel2 -lgcolorsel2-gui"
MODULE_VERSION="libgcolorsel2-0.1"
