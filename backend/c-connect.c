/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-connect.h"

#include "c-palette.h"
#include "gcolorsel2.h"

#include "GColorsel2.h"

#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

/* check every 5 minutes that client is still alive */

#define TIMEOUT 60000 * 5

static GTree *connects = NULL;
int cnum_last = 0;

static CConnect *
c_connect_new (ColorClient client)
{
  CConnect *connect;
  CORBA_Environment ev;
  
  g_assert (client != CORBA_OBJECT_NIL);
  
  CORBA_exception_init (&ev);
  
  connect = g_new0 (CConnect, 1);
  
  connect->cnum    = ++cnum_last;
  connect->client  = CORBA_Object_duplicate (client, &ev);
  connect->timeout = 0;  
  
  return connect;
}

static void
c_connect_destroy (CConnect *connect)
{
  CORBA_Environment ev;
  
  g_assert (connect != NULL);

  CORBA_exception_init (&ev);
  CORBA_Object_release (connect->client, &ev);  
  
  if (connect->timeout)
  
    g_source_remove (connect->timeout);

  g_free (connect);  
}

static gint 
c_connect_tree_compare_func (gconstpointer a, gconstpointer b)
{
  guint c1 = GPOINTER_TO_INT (a), c2 = GPOINTER_TO_INT (b);
  
  if (c1 < c2) return -1;
  if (c1 > c2) return 1;
  return 0;
}

CPaletteListData *
c_connect_get_palette_data (CConnect *connect)
{
  g_assert (connect != NULL);
  
  return connect->palette;
}

CPalette *
c_connect_get_palette (CConnect *connect)
{
  g_assert (connect != NULL);
  
  return c_palette_data_get_palette (c_connect_get_palette_data (connect));
}

ColorClient 
c_connect_get_client (CConnect *connect)
{
  g_assert (connect != NULL);
  
  return connect->client;
}

static gint
c_connect_timeout (gpointer data)
{
  CConnect *connect = data;
  CORBA_Environment ev;
  
  /* ping client */
  
  CORBA_exception_init (&ev);
  
  ColorClient_ping (connect->client, &ev);
  
  if (ev._major != CORBA_NO_EXCEPTION) {
    printf ("le client %d est mort !\n", connect->cnum);
    CORBA_exception_free (&ev);
    c_connect_list_disconnect (connect);    
    
    return FALSE;
  }

  return TRUE;
}

static void
c_connect_install_timeout (CConnect *connect)
{
  g_assert (connect != NULL);
  
  if (connect->timeout)
  
    g_source_remove (connect->timeout);
  
  connect->timeout = g_timeout_add_full (0, TIMEOUT, c_connect_timeout,
                                         connect, NULL);                                                                                  

}

/* CConnect List */

CConnect *
c_connect_list_connect (ColorClient client, const char *path, 
                        gboolean try_create, gboolean truncate, CError **error)
{
  CConnect *connect;
  
  g_assert (path != NULL);
  g_assert (client != CORBA_OBJECT_NIL);
  
  /* Create CConnect */
  
  connect = c_connect_new (client);
  
  /* Get palette */
  
  c_palette_list_connect (path, connect, try_create, truncate, error);
  if (*error) {
    c_connect_destroy (connect);
    return NULL;
  }
  
  if (! connects) 
    connects = g_tree_new (c_connect_tree_compare_func);  

  g_tree_insert (connects, GINT_TO_POINTER (connect->cnum), connect);
  
  /* Test alive timer */

  c_connect_install_timeout (connect);  
  
  return connect;
}

void
c_connect_list_disconnect (CConnect *connect)
{
  g_assert (connect != NULL);
  g_assert (connects != NULL);
  
  c_palette_list_disconnect (connect);
  
  g_tree_remove (connects, GINT_TO_POINTER (connect->cnum));
  
  c_connect_destroy (connect);
}

void 
c_connect_list_alive (CConnect *connect)
{
  c_connect_install_timeout (connect);  
}

CConnect *
c_connect_list_get (guint cnum, CError **error)
{
  CConnect *connect;

  g_assert (connects != NULL);
  g_assert ((cnum > 0) && (cnum <= cnum_last));
  
  connect = g_tree_lookup (connects, GINT_TO_POINTER (cnum));

  if (! connect) 
    *error = c_error_new (C_ERROR_BAD_CONNECT_NUM, C_ERROR_BUG, _("Connection number '%d' doesn't exist"), cnum);  
  
  return connect;
}

CPaletteListData *
c_connect_list_get_palette_data (guint cnum, CError **error)
{
  CConnect *connect;
  
  g_assert ((cnum > 0) && (cnum <= cnum_last));
  
  connect = c_connect_list_get (cnum, error);
  
  if (*error) return NULL;
  
  return c_connect_get_palette_data (connect);
}

CPalette *
c_connect_list_get_palette (guint cnum, CError **error)
{
  CPaletteListData *palette_data;
  
  palette_data = c_connect_list_get_palette_data (cnum, error);
  
  if (*error) return NULL;
  
  return c_palette_data_get_palette (palette_data);
}

