/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_COLOR_LIST_H
#define C_COLOR_LIST_H

#include <gcolorsel2.h>

#include "gcolorsel2-gui.h"
#include "c-list.h"
#include "c-shell.h"

#include <gdk/gdk.h>
#include <gtk/gtkclist.h>

#define C_COLOR_LIST_TYPE  (c_color_list_get_type ())
#define C_COLOR_LIST(o)    (GTK_CHECK_CAST ((o), C_COLOR_LIST_TYPE, CColorList))
#define C_IS_COLOR_LIST(o) (GTK_CHECK_TYPE ((o), C_COLOR_LIST_TYPE))

typedef struct _CColorList       CColorList;
typedef struct _CColorListClass  CColorListClass;

struct _CColorList {
  CList clist;

  CColorBase *color_base;
  
  GdkFont *font;
  
  guint width;
  guint height;
  
  GList *colors_to_redraw;
  guint idle;
  
  guint idle_select;
  
  guint add_one_freeze_next;
  guint add_one_thaw_next;
  
  GtkAdjustment *save_vadj;
  
  int go_to;
};

struct _CColorListClass {
  CListClass parent_class;

  void (*do_error) (CColorList *color_list, CError *error);
  void (*do_select_change) (CColorList *color_list);
};

GtkType     c_color_list_get_type  (void);
CColorList *c_color_list_new       (CColorBase *color_base, CSelectType select_type);
void        c_color_list_construct (CColorList *color_list, CColorBase *color_base,
                                    CSelectType select_type);

gboolean    c_color_list_have_selected   (CColorList *color_list);
GList      *c_color_list_get_selected    (CColorList *color_list, int *length);
void        c_color_list_clear_selection (CColorList *color_list, gboolean block_signal);

#endif
