/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>

#include "c-list.h"

static CListColumn column_name[] = {
  { "Col1", NULL, 100, TRUE  },
  { "Col2", NULL, 100, TRUE  },
  { NULL,   NULL, 0,   FALSE }
};

static void
do_move (CList *clist, gpointer data, guint from, guint to)
{
  GList *list;
  
  printf ("Move : %d -> %d\n", from, to);
  
  list =  GTK_CLIST (clist)->selection;
  while (list) {
  
    printf ("%d\n", GPOINTER_TO_INT (list->data));

    list = list->next;  
  }
}

static void
do_insert (CList *clist, gpointer data, guint pos)
{
  printf ("Insert : %d\n", pos);
}

int main (int argc, char *argv[])
{
  GtkWidget *app;
  GtkWidget *clist;
  GtkWidget *scrolled;
  GtkTargetEntry targets_table[] = { { "test", 0, 0 } };
  
  gnome_init ("test", "0.1", argc, argv);
  
  app = gnome_app_new ("test", "test");
  gtk_widget_set_usize (app, 300, 300);
  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
                      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
                      
  scrolled = gtk_scrolled_window_new (NULL, NULL);                      
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                  GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  gnome_app_set_contents (GNOME_APP (app), scrolled);
   
  clist = GTK_WIDGET (c_list_new (column_name, 1, targets_table, 1));
  gtk_clist_set_selection_mode (clist, GTK_SELECTION_EXTENDED);
  gtk_container_add (GTK_CONTAINER (scrolled), clist);
  
  gtk_signal_connect (GTK_OBJECT (clist), "do_insert",
                      GTK_SIGNAL_FUNC (do_insert), NULL);
                      
  gtk_signal_connect (GTK_OBJECT (clist), "do_move",
                      GTK_SIGNAL_FUNC (do_move), NULL);                      
  
  {
    int i;
    
    gtk_clist_freeze (GTK_CLIST (clist));
    
    for (i=0; i<100; i++) {
  
      char *text[2];
      
      text[0] = g_strdup_printf ("%d", i * 4);  
      text[1] = g_strdup_printf ("%d", 100 - i);
      
      gtk_clist_append (GTK_CLIST (clist), text);
    
    }
    
    gtk_clist_thaw (GTK_CLIST (clist)); 
          
  }
  
  gtk_widget_show_all (app);
     
  gtk_main ();

  return 0;
}

