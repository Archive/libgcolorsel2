/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_LIST_H
#define C_LIST_H

#include <gtk/gtkdnd.h>
#include <gtk/gtkclist.h>
#include <gtk/gtktooltips.h>

#define C_LIST_TYPE  (c_list_get_type ())
#define C_LIST(o)    (GTK_CHECK_CAST ((o), C_LIST_TYPE, CList))

typedef struct _CList       CList;
typedef struct _CListClass  CListClass;
typedef struct _CListColumn CListColumn;

struct _CList {
  GtkCList clist;
  
  int column_selected;
  GtkSortType sort_type;
  
  GtkTooltips *tooltips;
  
  gboolean drag_pending;
  gboolean drag_started;
  int press_button;
  int press_x;
  int press_y;  
  int on_drag_pos;
  int dragging;
  GtkCListDragPos drag_pos;
  int scroll_timeout;
  
  gboolean got_drop_data_type;
  int data_type;
  gboolean drop_occured;
};

struct _CListClass {
  GtkCListClass parent_class;    
  
  void (*do_move) (CList *clist, GtkSelectionData *data, guint to);
  void (*do_insert) (CList *clist, GtkSelectionData *data, guint pos); 

  void (*data_get) (CList *clist, GtkSelectionData *data, int time);  
  gpointer (*get_data_type) (CList *clist);
  gboolean (*get_accept_type) (CList *clist, int type);
  void (*get_drag_widget) (CList *clist, GtkWidget **widget);
  
  void (*do_popup) (CList *clist);
};

struct _CListColumn {
  char *name;
  GtkCListCompareFunc compare;    
  
  int width;
  gboolean resizeable;
};

GtkType  c_list_get_type (void);
CList   *c_list_new      (CListColumn columns[], int sort_column,
                          GtkTargetEntry *target_table, int target_length);
void     c_list_construct (CList *list, CListColumn columns[], int sort_column,
                           GtkTargetEntry *target_table, int target_length);

void     c_list_set_sort_column (CList *list, gint column, GtkSortType sort_type);
float    c_list_height (CList *clist);

#endif
