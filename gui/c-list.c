/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-list.h"

#include <gcolorsel2.h>
#include "e-clipped-label.h"

#include <gnome.h>

#define KEY_ARROW_UP     "c_list_arrow_up"
#define KEY_ARROW_DOWN   "c_list_arrow_down"
#define KEY_COMPARE_FUNC "c_list_compare_func"

#define RADIUS 10
#define SCROLL_TIMEOUT 200
#define AUTO_SCROLL_MARGIN 5
#define MIN_AUTOSCROLL_DELTA 5
#define MAX_AUTOSCROLL_DELTA 50

/* this defigns the base grid spacing */
#define CELL_SPACING 1

/* gives the top pixel of the given row in context of
 * the clist's voffset */
#define ROW_TOP_YPIXEL(clist, row) (((clist)->row_height * (row)) + \
                                    (((row) + 1) * CELL_SPACING) + \
                                    (clist)->voffset)

/* returns the row index from a y pixel location in the
 * context of the clist's voffset */
#define ROW_FROM_YPIXEL(clist, y)  (((y) - (clist)->voffset) / \
                                    ((clist)->row_height + CELL_SPACING))
                                    
/* returns the total height of the list */
#define LIST_HEIGHT(clist)         (((clist)->row_height * ((clist)->rows)) + \
                                    (CELL_SPACING * ((clist)->rows + 1)))
                                     
/* From nautilus */
static char * down_xpm[] = {
"6 5 2 1",
"       c None",
".      c #000000",
"......",
"      ",
" .... ",
"      ",
"  ..  "};

/* From nautilus */
static char * up_xpm[] = {
"6 5 2 1",
"       c None",
".      c #000000",
"  ..  ",
"      ",
" .... ",
"      ",
"......"};


static void     c_list_destroy              (GtkObject *object);

static void     c_list_click_column         (GtkCList *clist, gint column);

static gint     c_list_button_press_event   (GtkWidget *widget, GdkEventButton *event);
static gint     c_list_button_release_event (GtkWidget *widget, GdkEventButton *event);
static gint     c_list_motion_notify_event  (GtkWidget *widget, GdkEventMotion *event);

static void     c_list_drag_begin           (GtkWidget *widget, GdkDragContext *context);
static void     c_list_drag_end             (GtkWidget *widget, GdkDragContext *context);
static void     c_list_drag_data_received   (GtkWidget *widget, GdkDragContext *context,
                                             gint x, gint y, GtkSelectionData *data,
                                             guint info, guint time);
static void     c_list_drag_data_get        (GtkWidget *widget, GdkDragContext *context,
                                             GtkSelectionData *data, guint info, guint time);
static gboolean c_list_drag_drop            (GtkWidget *widget, GdkDragContext *context,
                                             gint x, gint y, guint time);
static gboolean c_list_drag_motion          (GtkWidget *widget, GdkDragContext *context, 
                                             int x, int y, guint time);
static void     c_list_drag_leave           (GtkWidget *widget, GdkDragContext *context,
                                             guint time);                                             
                                             
static void     c_list_dest_cell            (CList *list, gint x, gint y, 
                                             gint *row, GtkCListDragPos *insert_pos);                                             

static void     c_list_set_light            (CList *clist, int pos, GtkCListDragPos drag_pos);
static void     c_list_unset_light          (CList *clist);

enum {
  DO_MOVE,
  DO_INSERT,
  DO_POPUP,
  DATA_GET,
  GET_DRAG_WIDGET,
  GET_DATA_TYPE,
  GET_ACCEPT_TYPE,
  LAST_SIGNAL
};

#define PARENT_TYPE GTK_TYPE_CLIST
static GtkCListClass *parent_class = NULL;
static guint c_list_signals[LAST_SIGNAL] = { 0 };

typedef gpointer (*CListSignal_POINTER__NONE) (GtkObject *object, gpointer user_data);

static void
clist_marshal_POINTER__NONE (GtkObject *object, 
                             GtkSignalFunc func,
                             gpointer func_data, GtkArg *args)
{
  CListSignal_POINTER__NONE rfunc;
  gpointer *return_val;
  return_val = GTK_RETLOC_POINTER (args[0]);
  
  rfunc = (CListSignal_POINTER__NONE) func;
  *return_val = (*rfunc) (object, func_data);
}

typedef gboolean (*CListSignal_BOOL__INT) (GtkObject *object, int type, gpointer user_data);

static void
clist_marshal_BOOL__INT (GtkObject *object, 
                         GtkSignalFunc func,                        
                         gpointer func_data, GtkArg *args)
{
  CListSignal_BOOL__INT rfunc;
  gboolean *return_val;
  return_val = GTK_RETLOC_BOOL (args[1]);
  
  rfunc = (CListSignal_BOOL__INT) func;
  *return_val = (*rfunc) (object, GTK_VALUE_INT (args[0]), func_data);
}

/* GtkObject methods */

static void
class_init (CListClass *class)
{
  GtkObjectClass *object_class;
  GtkCListClass  *clist_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass *) class;
  widget_class = (GtkWidgetClass *) class;
  clist_class  = (GtkCListClass *) class;
  
  parent_class = gtk_type_class (GTK_TYPE_CLIST);
  
  c_list_signals[DO_MOVE] = 
    gtk_signal_new ("do_move",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, do_move),
                    gtk_marshal_NONE__POINTER_UINT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_POINTER, GTK_TYPE_UINT);
                    
  c_list_signals[DO_INSERT] = 
    gtk_signal_new ("do_insert",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, do_insert),
                    gtk_marshal_NONE__POINTER_UINT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_POINTER, GTK_TYPE_UINT);
                    
  c_list_signals[DO_POPUP] =
    gtk_signal_new ("do_popup",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, do_popup),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);                    
                    
  c_list_signals[DATA_GET] = 
    gtk_signal_new ("data_get",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, data_get),
                    gtk_marshal_NONE__POINTER_INT,
                    GTK_TYPE_NONE, 2, GTK_TYPE_POINTER, GTK_TYPE_INT);    
                    
  c_list_signals[GET_DATA_TYPE] = 
    gtk_signal_new ("get_data_type",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, get_data_type),
                    clist_marshal_POINTER__NONE,
                    GTK_TYPE_POINTER, 0);                                        
 
  c_list_signals[GET_ACCEPT_TYPE] =
    gtk_signal_new ("get_accept_type",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, get_accept_type),
                    clist_marshal_BOOL__INT,
                    GTK_TYPE_BOOL, 1, GTK_TYPE_INT);

  c_list_signals[GET_DRAG_WIDGET] =
    gtk_signal_new ("get_drag_widget",
                    GTK_RUN_FIRST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CListClass, get_drag_widget),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
                                        
                    
  gtk_object_class_add_signals (object_class, c_list_signals, LAST_SIGNAL);                                        
  
  object_class->destroy              = c_list_destroy;
  
  widget_class->button_press_event   = c_list_button_press_event;
  widget_class->motion_notify_event  = c_list_motion_notify_event;
  widget_class->button_release_event = c_list_button_release_event;

  widget_class->drag_begin           = c_list_drag_begin;
  widget_class->drag_motion          = c_list_drag_motion;
  widget_class->drag_drop            = c_list_drag_drop;
  widget_class->drag_end             = c_list_drag_end;
  widget_class->drag_leave           = c_list_drag_leave;
  widget_class->drag_data_get        = c_list_drag_data_get;
  widget_class->drag_data_received   = c_list_drag_data_received;
  
  clist_class->click_column          = c_list_click_column;
}

static void
init (CList *list)
{
  list->column_selected = -1;
  
  list->drag_pending = FALSE;
  list->drag_started = FALSE;

  list->got_drop_data_type = FALSE;

  list->scroll_timeout = 0;
  
  gtk_widget_add_events (GTK_WIDGET (list), GDK_POINTER_MOTION_MASK);
}

CList *
c_list_new (CListColumn columns[], int sort_column, 
            GtkTargetEntry *target_table, int target_length)
{
  CList *list;
  
  g_assert (columns != NULL);
  g_assert (columns[0].name != NULL);
    
  list = C_LIST (gtk_type_new (c_list_get_type ()));
  
  c_list_construct (C_LIST (list), columns, sort_column, target_table, target_length);
  
  return list;
}

static void
c_list_destroy (GtkObject *object)
{
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

float
c_list_height (CList *list)
{
  return LIST_HEIGHT (GTK_CLIST (list));
}

/* From Nautilus */
static void
c_list_drag_autoscroll_calculate_delta (GtkWidget *widget, float *x_scroll_delta, float *y_scroll_delta)
{
  int x, y;

  g_assert (GTK_IS_WIDGET (widget));

  gdk_window_get_pointer (widget->window, &x, &y, NULL);    

  /* Find out if we are anywhere close to the tree view edges
   * to see if we need to autoscroll.
   */
  *x_scroll_delta = 0;
  *y_scroll_delta = 0;   
	
  if (x < AUTO_SCROLL_MARGIN) {
	 	*x_scroll_delta = (float)(x - AUTO_SCROLL_MARGIN);
	}

 	if (x > widget->allocation.width - AUTO_SCROLL_MARGIN) {
	        if (*x_scroll_delta != 0) {
	                /* Already trying to scroll because of being too close to
	                 * the top edge -- must be the window is really short,   
	                 * don't autoscroll.
	                 */
	                return;
	        }
	        *x_scroll_delta = (float)(x - (widget->allocation.width - AUTO_SCROLL_MARGIN));
	}

	if (y < AUTO_SCROLL_MARGIN) {
	        *y_scroll_delta = (float)(y - AUTO_SCROLL_MARGIN);
	}

	if (y > widget->allocation.height - AUTO_SCROLL_MARGIN) {
	        if (*y_scroll_delta != 0) {
	                /* Already trying to scroll because of being too close to
	                 * the top edge -- must be the window is really narrow,  
	                 * don't autoscroll.
	                 */
	                return;
                }
	        *y_scroll_delta = (float)(y - (widget->allocation.height - AUTO_SCROLL_MARGIN));
	}

	if (*x_scroll_delta == 0 && *y_scroll_delta == 0) {
	        /* no work */
	        return;
	}

	/* Adjust the scroll delta to the proper acceleration values depending on how far
	 * into the sroll margins we are.
	 * FIXME bugzilla.eazel.com 2486:
	 * we could use an exponential acceleration factor here for better feel
	 */
	if (*x_scroll_delta != 0) {
	        *x_scroll_delta /= AUTO_SCROLL_MARGIN;
	        *x_scroll_delta *= (MAX_AUTOSCROLL_DELTA - MIN_AUTOSCROLL_DELTA);
	        *x_scroll_delta += MIN_AUTOSCROLL_DELTA;
	}
	 
	if (*y_scroll_delta != 0) {
	        *y_scroll_delta /= AUTO_SCROLL_MARGIN;
	        *y_scroll_delta *= (MAX_AUTOSCROLL_DELTA - MIN_AUTOSCROLL_DELTA);
         	*y_scroll_delta += MIN_AUTOSCROLL_DELTA;
	}
}

/* From Nautilus */
/* The standard gtk_adjustment_set_value ignores page size, which
 * disagrees with the logic used by scroll bars, for example.
 */ 
static void
c_list_adjustment_set_value (GtkAdjustment *adjustment,
                                   float value)
{
 	float upper_page_start, clamped_value;

	g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
	
 	upper_page_start = MAX (adjustment->upper - adjustment->page_size, adjustment->lower);
 	clamped_value = CLAMP (value, adjustment->lower, upper_page_start);
 	if (clamped_value != adjustment->value) { 
		adjustment->value = clamped_value;
         	gtk_adjustment_value_changed (adjustment);
        }
}

static int
c_list_auto_scroll_timeout (gpointer data)
{
  float x, y;
  
  c_list_drag_autoscroll_calculate_delta (GTK_WIDGET (data), &x, &y);
  
  if ((x) || (y)) {
    
    c_list_unset_light (C_LIST (data));
      
    c_list_adjustment_set_value (GTK_CLIST (data)->vadjustment, GTK_CLIST (data)->vadjustment->value + y);     
    c_list_adjustment_set_value (GTK_CLIST (data)->hadjustment, GTK_CLIST (data)->hadjustment->value + x);        
    
  }
 
  return TRUE;
}

static void
c_list_start_auto_scroll (CList *clist)
{
  if (! clist->scroll_timeout) {
  
    clist->scroll_timeout = gtk_timeout_add (SCROLL_TIMEOUT, c_list_auto_scroll_timeout, clist);
    
  }
}

static void
c_list_stop_auto_scroll (CList *clist)
{
  if (clist->scroll_timeout) {
  
    gtk_timeout_remove (clist->scroll_timeout);
    clist->scroll_timeout = 0;  
  
  }
}

static gboolean 
c_list_is_row_selected (CList *list, int row)
{
  GtkCListRow *elem;
  
  elem = g_list_nth (GTK_CLIST (list)->row_list, row)->data;
  
  return elem->state == GTK_STATE_SELECTED;
} 

static void
row_set_selected (CList *list, int row_index, gboolean select, gboolean d)
{
  GtkCList *l = GTK_CLIST (list);
  
  if (select) {
    gtk_clist_select_row (l, row_index, 0);
    if ((d) && (row_index != l->focus_row)) {
      if (l->focus_row >= 0) {
        gtk_widget_draw_focus (GTK_WIDGET (l));
      }
      l->focus_row = row_index;
      gtk_widget_draw_focus (GTK_WIDGET (l));
    }
  } else {
    gtk_clist_unselect_row (l, row_index, 0);
  }
}

static void
select_range (CList *list, int row)
{
  int min, max;
  int i;
  GtkCList *l = GTK_CLIST (list);
  
  if (l->anchor == -1) {
    l->anchor = row;
  }
  
  if (row < l->anchor) {
    min = row;
    max = l->anchor;
  } else {
    min = l->anchor;
    max = row;
  }
  
  for (i = min; i <= max; i++) {
    row_set_selected (list, i, TRUE, FALSE);
  }
}

static void
select_row_from_mouse (CList *list, int row, guint state)
{
  int range, additive;
  
  range = (state & GDK_SHIFT_MASK) != 0;
  additive = (state & GDK_CONTROL_MASK) != 0;
  
  if (! additive) {
    int anchor = GTK_CLIST (list)->anchor;
    gtk_clist_unselect_all (GTK_CLIST (list));
    GTK_CLIST (list)->anchor = anchor;
  } 
  
  if (range) {
    select_range (list, row);
  } else {
    row_set_selected (list, row, ! additive || ! c_list_is_row_selected (list, row), TRUE);  
    GTK_CLIST (list)->anchor = row;
  }  
}

static gint
c_list_button_press_event (GtkWidget *widget, GdkEventButton *event)
{
  CList *clist;
  int row, col;
  gint on_row;
  
  clist = C_LIST (widget);

  if (! GTK_WIDGET_HAS_FOCUS (widget)) {
    gtk_widget_grab_focus (widget);
  }
  
  if ((event->window != GTK_CLIST (widget)->clist_window)) {
    return GTK_WIDGET_CLASS (parent_class)->button_press_event (widget, event);
  }
  
  on_row = gtk_clist_get_selection_info (GTK_CLIST (widget), event->x, event->y, &row, &col);
  
  if (event->button == 3) {
  
    if (on_row) {
    
      if (! c_list_is_row_selected (clist, row)) {
    
        GTK_CLIST (clist)->focus_row = row,
        gtk_clist_unselect_all (GTK_CLIST (clist));
        gtk_clist_select_row (GTK_CLIST (clist), row, col);
        
      }
      
    }  

    gtk_signal_emit (GTK_OBJECT (clist), c_list_signals[DO_POPUP], event);

    return FALSE;    
  }
  
  if ((event->type != GDK_BUTTON_PRESS) || (event->button != 1) || (! on_row)) {
    return FALSE;
  }

  clist->drag_pending = TRUE;
  clist->dragging = row;
  clist->on_drag_pos = -1;
  clist->drag_pos = GTK_CLIST_DRAG_NONE;
  clist->press_button = event->button;
  clist->press_x = event->x;
  clist->press_y = event->y;
  
  if (on_row) {
    if ((c_list_is_row_selected (clist, row)) && (! (event->state & GDK_CONTROL_MASK))) {
    } else {
      select_row_from_mouse (clist, row, event->state);
    }        
    
  } else {
    gtk_clist_unselect_all (GTK_CLIST (clist));
  }

  return FALSE;
}

static gint
c_list_button_release_event (GtkWidget *widget, GdkEventButton *event)
{
  CList *clist;
  
  clist = C_LIST (widget);
  
  clist->drag_pending = FALSE;
  clist->drag_started = FALSE;
  
  if (event->window != GTK_CLIST (widget)->clist_window) {
    return GTK_WIDGET_CLASS (parent_class)->button_release_event (widget, event);
  }
  
  return TRUE;
}

static gboolean
get_distance (CList *clist, GdkEventMotion *event)
{
  return (MAX (abs (event->x - clist->press_x), 
               abs (event->y - clist->press_y)) > 3);
}

static gint 
c_list_motion_notify_event (GtkWidget *widget, GdkEventMotion *event)
{
  CList *clist;
  
  clist = C_LIST (widget);
  
  if (event->window != GTK_CLIST (widget)->clist_window) {
    return GTK_WIDGET_CLASS (parent_class)->motion_notify_event (widget, event);;
  }  
  
  if (clist->drag_pending) {
  
    GtkTargetList *target_list;
    
    if (get_distance (clist, event)) {
  
      gtk_signal_emit (GTK_OBJECT (clist), c_list_signals[GET_DATA_TYPE], &target_list);

      if (target_list) {
        gtk_drag_begin (widget,
                        target_list, GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_ASK,
                        clist->press_button,
                        (GdkEvent *) event);
                      
        return TRUE;                      
               
        gtk_target_list_unref (target_list);
      }
    }
  }  
  
  return FALSE;  
}

static void
c_list_unset_light (CList *clist)
{
  if (clist->on_drag_pos == -1) {
    return;
  }

  parent_class->draw_drag_highlight (GTK_CLIST (clist),
                                     g_list_nth (GTK_CLIST (clist)->row_list, clist->on_drag_pos)->data,
                                     clist->on_drag_pos, clist->drag_pos);
  
  clist->on_drag_pos = -1;                            
  clist->drag_pos = GTK_CLIST_DRAG_NONE;
}

static void
c_list_set_light (CList *clist, int pos, GtkCListDragPos drag_pos)
{
  if ((pos == clist->on_drag_pos) && (drag_pos == clist->drag_pos)) {
    return;
  }
  
  c_list_unset_light (clist);
  
  if (pos != -1) {
 
    parent_class->draw_drag_highlight (GTK_CLIST (clist), 
                                       g_list_nth (GTK_CLIST (clist)->row_list, pos)->data,
                                       pos, drag_pos);
                                       
  }                                       
                            
  clist->on_drag_pos = pos;                            
  clist->drag_pos = drag_pos;
}

static void
c_list_dest_cell (CList *list, gint x, gint y, gint *row, GtkCListDragPos *insert_pos)
{
  GtkWidget *widget;
  GtkCList *clist;

  widget = GTK_WIDGET (list);
  clist = GTK_CLIST (list);

  *insert_pos = GTK_CLIST_DRAG_NONE;

  y -= (GTK_CONTAINER (clist)->border_width +
        widget->style->klass->ythickness + /* Remove klass for Gtk 2.0 */
        clist->column_title_area.height);

  *row = ROW_FROM_YPIXEL (clist, y);
  
  if (*row >= clist->rows) {
    *row = clist->rows - 1;
    y = ROW_TOP_YPIXEL (clist, *row) + clist->row_height;
  }
  
  if (*row < -1)
    *row = -1;  
  
  x -= GTK_CONTAINER (widget)->border_width + widget->style->klass->xthickness;

  if (*row >= 0) {
    gint y_delta;
    gint h = 0;

    y_delta = y - ROW_TOP_YPIXEL (clist, *row);
	
    *insert_pos = GTK_CLIST_DRAG_BEFORE;
    h = clist->row_height / 2;
	
    if (y_delta < h)
      *insert_pos = GTK_CLIST_DRAG_BEFORE;
    else 
      if (clist->row_height - y_delta < h)
        *insert_pos = GTK_CLIST_DRAG_AFTER;	   	      	    	      
  }        
}

static void
c_list_drag_begin (GtkWidget *widget, GdkDragContext *context)
{
  GtkWidget *drag_widget = NULL;
  
  C_LIST (widget)->drag_pending = FALSE;
  C_LIST (widget)->drag_started = TRUE;
  C_LIST (widget)->got_drop_data_type = FALSE;
  
  gtk_signal_emit (GTK_OBJECT (widget), c_list_signals[GET_DRAG_WIDGET], &drag_widget);
  
  if (drag_widget) {
  
    gtk_drag_set_icon_widget (context, drag_widget, -2, -2);
  
  } else {
  
    gtk_drag_set_icon_default (context);
  
  }
}

static void     
c_list_drag_end (GtkWidget *widget, GdkDragContext *context)
{
}

static void     
c_list_drag_leave (GtkWidget *widget, GdkDragContext *context, guint time)
{
  c_list_unset_light (C_LIST (widget));
  
  C_LIST (widget)->got_drop_data_type = FALSE;
  
  c_list_stop_auto_scroll (C_LIST (widget));
}

static gboolean
c_list_drag_motion (GtkWidget *widget, GdkDragContext *context, 
                    int x, int y, guint time)
{
  int row;
  GtkCListDragPos pos;
  GdkModifierType modifiers;
  
  if (! C_LIST (widget)->got_drop_data_type) {

    C_LIST (widget)->drop_occured = FALSE;  
    gtk_drag_get_data (widget, context, 
                       GPOINTER_TO_INT (context->targets->data), time);
   
  }
  
  if (C_LIST (widget)->got_drop_data_type) {
  
    gboolean accept;
  
    gtk_signal_emit (GTK_OBJECT (widget), c_list_signals[GET_ACCEPT_TYPE], 
                     C_LIST (widget)->data_type, &accept);

    if (accept) {  
    
      c_list_start_auto_scroll (C_LIST (widget));  

      c_list_dest_cell (C_LIST (widget), x, y, &row, &pos);
    
      c_list_set_light (C_LIST (widget), row, pos);
     
      gdk_window_get_pointer (NULL, NULL, NULL, &modifiers);  

      if ((modifiers & GDK_CONTROL_MASK) || (gtk_drag_get_source_widget (context) != widget)) {
  
        gdk_drag_status (context, GDK_ACTION_COPY, time);

      } else {

        gdk_drag_status (context, GDK_ACTION_MOVE, time);
    
      }
    
      return TRUE;
    }
  }
  
  c_list_unset_light (C_LIST (widget));
  
//  gdk_drag_status (context, 0, time);

  return FALSE;
}                    

static gboolean
c_list_drag_drop (GtkWidget *widget, GdkDragContext *context,
                  gint x, gint y, guint time)
{
  C_LIST (widget)->drop_occured = TRUE;
  gtk_drag_get_data (widget, context, GPOINTER_TO_INT (context->targets->data), time);

  return FALSE;
}                  

static void     
c_list_drag_data_received (GtkWidget *widget, GdkDragContext *context,
                           gint x, gint y, GtkSelectionData *data,
                           guint info, guint time)
{
  CList *clist;
  GtkCListDragPos pos;
  int row;
  
  clist = C_LIST (widget);

  clist->data_type = info;
  clist->got_drop_data_type = TRUE;
  
  if (clist->drop_occured) {
  
    c_list_dest_cell (C_LIST (widget), x, y, &row, &pos);
  
//    if (info != DRAG_TYPE) {
//      gtk_drag_finish (context, FALSE, FALSE, time);
//      return;
//    }
    
    if (row == -1) row = 0;    
    row++;
    
    if (pos == GTK_CLIST_DRAG_AFTER) {
      row++;
    }
    
    if ((gtk_drag_get_source_widget (context) == widget) && (context->action != GDK_ACTION_COPY)) {
      
      gtk_signal_emit (GTK_OBJECT (widget), c_list_signals[DO_MOVE], data, row);
         
    } else {
  
      gtk_signal_emit (GTK_OBJECT (widget), c_list_signals[DO_INSERT], data, row);
  
    }
  
    gtk_drag_finish (context, TRUE, FALSE, time);
    
    clist->drop_occured = FALSE;
    clist->got_drop_data_type = FALSE;    
  }
}
                            
static void     
c_list_drag_data_get (GtkWidget *widget, GdkDragContext *context,
                      GtkSelectionData *data, guint info, guint time)
{
  gtk_signal_emit (GTK_OBJECT (widget), c_list_signals[DATA_GET], data, info);
}                      

static void
c_list_get_pixmap_widget (CList *list, gint column, GtkWidget **up, GtkWidget **down)
{
  GtkWidget *column_widget;

  g_assert (list != NULL);
  g_assert (column >= 0);
  
  column_widget = gtk_clist_get_column_widget (GTK_CLIST (list), column);
  g_assert (column_widget != NULL);
  
  *up = gtk_object_get_data (GTK_OBJECT (column_widget), KEY_ARROW_UP);
  *down = gtk_object_get_data (GTK_OBJECT (column_widget), KEY_ARROW_DOWN);  
}

static GtkCListCompareFunc
c_list_get_compare_func (CList *list, gint column)
{
  GtkCListCompareFunc compare_func;
  GtkWidget *column_widget;
  
  g_assert (list != NULL);
  g_assert (column >= 0);
  
  column_widget = gtk_clist_get_column_widget (GTK_CLIST (list), column);
  g_assert (column_widget != NULL);
  
  compare_func = gtk_object_get_data (GTK_OBJECT (column_widget), KEY_COMPARE_FUNC);

  return compare_func;  
}

void
c_list_set_sort_column (CList *list, gint column, GtkSortType sort_type)
{
  GtkWidget *up;
  GtkWidget *down;
  GtkCListCompareFunc compare_func;
  
  g_assert (list != NULL);
  g_assert (column >= -1);
  
  if (list->column_selected != -1) {
  
    c_list_get_pixmap_widget (list, list->column_selected, &up, &down);
    g_assert (up != NULL);
    g_assert (down != NULL);
    
    gtk_widget_hide (up);
    gtk_widget_hide (down);
  
  }

  if (column != -1) {

    gtk_clist_set_sort_column (GTK_CLIST (list), column);
    gtk_clist_set_sort_type (GTK_CLIST (list), sort_type);
             
    list->column_selected = column;
    list->sort_type = sort_type;

    c_list_get_pixmap_widget (list, column, &up, &down);

    g_assert (up != NULL);
    g_assert (down != NULL);
    
    if (sort_type == GTK_SORT_ASCENDING)
      gtk_widget_show (down);
    else
      gtk_widget_show (up);
      
    compare_func = c_list_get_compare_func (list, column);
    
    gtk_clist_set_compare_func (GTK_CLIST (list), compare_func);
    
    gtk_clist_sort (GTK_CLIST (list));

  } else 
    list->column_selected = -1;

}

static void
c_list_click_column (GtkCList *clist, gint column)
{
  CList *list;
  
  list = C_LIST (clist);
  
  if (list->column_selected == column) {
  
    if (list->sort_type == GTK_SORT_ASCENDING)
      c_list_set_sort_column (list, column, GTK_SORT_DESCENDING);
    else
      c_list_set_sort_column (list, column, GTK_SORT_ASCENDING);
  
  } else 
    c_list_set_sort_column (list, column, GTK_SORT_ASCENDING);      

  if (GTK_CLIST_CLASS (parent_class)->click_column)
    return GTK_CLIST_CLASS (parent_class)->click_column (clist, column);  
}

static GtkWidget *
c_list_construct_column_widget (CList *clist, CListColumn *column)
{
  GtkWidget *hbox;
  GtkWidget *label;
  GtkWidget *up;
  GtkWidget *down;

  g_assert (clist != NULL);  
  g_assert (column != NULL);
  g_assert (column->name != NULL);
  
  hbox = gtk_hbox_new (FALSE, 0);
  
  label = e_clipped_label_new (column->name);
  gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 2);
  
  up   = gnome_pixmap_new_from_xpm_d (up_xpm);
  down = gnome_pixmap_new_from_xpm_d (down_xpm);  
  
  gtk_box_pack_end (GTK_BOX (hbox), up, FALSE, FALSE, 2);
  gtk_box_pack_end (GTK_BOX (hbox), down, FALSE, FALSE, 2);
  
  gtk_object_set_data (GTK_OBJECT (hbox), KEY_ARROW_UP, up);
  gtk_object_set_data (GTK_OBJECT (hbox), KEY_ARROW_DOWN, down);
  
  gtk_object_set_data (GTK_OBJECT (hbox), KEY_COMPARE_FUNC, column->compare);
  
  gtk_widget_show (label);
  gtk_widget_show (hbox);
    
  return hbox;
}

void
c_list_construct (CList *list, CListColumn columns[], int sort_column,
                  GtkTargetEntry *targets_table, int targets_table_length)
{
  GtkWidget *column_widget;
  int i = 0;
  
  g_assert (list != NULL);
  g_assert (columns != NULL);
  g_assert (sort_column >= -1); 
  
  while (columns[i].name) i++;
  
  g_assert (sort_column < i);
  g_assert (i >= 1);
  
  gtk_clist_construct (GTK_CLIST (list), i, NULL);
  
  if (targets_table) {
  
    gtk_drag_dest_set (GTK_WIDGET (list), 0,
                       targets_table, targets_table_length, GDK_ACTION_MOVE | GDK_ACTION_COPY | GDK_ACTION_ASK);
                       
  }                       
  
  gtk_clist_column_titles_show (GTK_CLIST (list));
  
  list->tooltips = gtk_tooltips_new ();
  
  i = 0;
  while (columns[i].name) {

    column_widget = c_list_construct_column_widget (list, &columns[i]);
      
    gtk_clist_set_column_widget (GTK_CLIST (list), i, column_widget);
    gtk_clist_set_column_min_width (GTK_CLIST (list), i, 25);
    gtk_clist_set_column_width (GTK_CLIST (list), i, columns[i].width);
    gtk_clist_set_column_resizeable (GTK_CLIST (list), i, columns[i].resizeable);
  
    gtk_tooltips_set_tip (list->tooltips, GTK_CLIST (list)->column[i].button, columns[i].name, "");
   
    i++;
  }
  
  if (sort_column != -1) 
    c_list_set_sort_column (list, sort_column, GTK_SORT_ASCENDING);    
}

C_MAKE_TYPE (c_list, "CList", CList, class_init, init, PARENT_TYPE)
