/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_PALETTE_H
#define C_PALETTE_H

#include "gcolorsel2.h"

#include "GColorsel2.h"

#include <stdio.h>
#include <glib.h>

typedef struct _CPalette CPalette;

struct _CPalette {  
  guint nb_elem;   // Le nombre d'element occupe
  guint size;      // La taille du tableau (nombre d'elements)  
  
  gpointer *data;
  
  char *header;

  gpointer user_data;      
};

CPalette *c_palette_new     (void);
void      c_palette_free    (CPalette *palette);
CPalette *c_palette_load    (FILE *fp, const char *path, CError **error);
void      c_palette_save    (FILE *fp, CPalette *palette, const char *path, CError **error);

void      c_palette_insert  (CPalette *palette, guint pos,
                             const ColorList *color_list, CColorBase *base, CError **error);
void      c_palette_remove  (CPalette *palette, const ColorPosList *pos_list,
		             CColorBase *color_base, CError **error);
void      c_palette_move    (CPalette *palette, guint pos, 
                             const ColorPosList *pos_list, CColorBase *base, CError **error);
void      c_palette_change  (CPalette *palette, guint pos, 
                             const Color *corba_color, CColorBase *base, CError **error);

gpointer  c_palette_get     (CPalette *palette, guint pos, CError **error);

#endif /* PALETTE_H */
