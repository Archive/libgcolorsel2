#include <stdio.h>
#include <stdlib.h>

#include "GColorsel2.h"
#include "c-color.h"
#include "c-palette.h"
#include "gcolorsel2.h"

static void
fill_color (ColorList *insert_list, int pos, int red, int green, int blue)
{
  CColor *color;
  char buf[20];
  
  sprintf (buf, "%d", pos);
  
  color = &(insert_list->_buffer[pos - 1]);
  color->red   = red;
  color->blue  = green;
  color->green = blue;
  color->name  = CORBA_string_dup (buf);
  color->comment = CORBA_string_dup (buf);
}

static CPalette *
test (int from, int to, int dest, int size, CError **error)
{
  ColorList *insert_list;
  ColorPosList *pos_list;
  CPalette *palette;
  guint i;
  
  printf ("MOVING  ( %d ... %d )  ->  %d\n", from, to, dest);
  
  palette = c_palette_new ();
  
  /****** Insert ******/
  
  insert_list = ColorList__alloc ();
  insert_list->_buffer = CORBA_sequence_Color_allocbuf (size);
  insert_list->_length = size;
  insert_list->_maximum = size;

  for (i = 1; i <= size; i++) {
    fill_color (insert_list, i, 100, 100, 100);
  }

  c_palette_insert (palette, 1, insert_list, NULL, error);
  if (*error) {
    return NULL;
  }
  
  /****** Move ******/
  
  pos_list = ColorPosList__alloc ();
  pos_list->_buffer = CORBA_sequence_CORBA_unsigned_long_allocbuf (to - from + 1);
  pos_list->_length = to - from + 1;
  pos_list->_maximum = to - from + 1;

  for (i = 0; i < to - from + 1; i++) {  
    pos_list->_buffer[i] = from + i;    
  }

  c_palette_move (palette, dest, pos_list, NULL, error);
  if (*error) {
    return NULL;
  }
  
  CORBA_sequence_set_release (pos_list, TRUE);
  CORBA_free (pos_list);
  CORBA_sequence_set_release (insert_list, TRUE);
  CORBA_free (insert_list);
  
  return palette;
}
int main (int argc, char *argv[])
{
  CPalette *palette;
  int i, j, k;
  CColor *color;
  CError *error = NULL;
  int size = 7;
  
  for (i = 1; i <= size; i++) {
    
    for (j = 1; j <= size; j++) {
    
      for (k = j; k <= size; k++) {

        if ((k - j + 1) + i > size + 1) continue;
    
        palette = test (j, k, i, size, &error);
        if (error) {
          printf ("Error (1) : '%s'\n", error->message);
          c_palette_dump (palette);        
          exit (-1);
        }
      
        color = c_palette_get (palette, i, &error); 
        if (error) {
          printf ("Error (2) : '%s'\n", error->message);
          c_palette_dump (palette);
          exit (-1);
        }
      
        if (j != atoi (color->name)) {
          printf ("Error (3)\n");
          c_palette_dump (palette);        
          exit (-1);
        }
      
        c_palette_free (palette);
      }
    }
    
  }
  
  return 0;
}
