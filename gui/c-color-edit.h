/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_COLOR_EDIT_H
#define C_COLOR_EDIT_H

#include "c-preview.h"
#include <gcolorsel2.h>

#include <gnome.h>

#define C_COLOR_EDIT_TYPE  (c_color_edit_get_type ())
#define C_COLOR_EDIT(o)    (GTK_CHECK_CAST ((o), C_COLOR_EDIT_TYPE, CColorEdit))
#define C_IS_COLOR_EDIT(o) (GTK_CHECK_TYPE ((o), C_COLOR_EDIT_TYPE))

typedef struct _CColorEdit       CColorEdit;
typedef struct _CColorEditClass  CColorEditClass;

struct _CColorEdit {
  GnomeApp app;

  CColorBase *color_base;
  
  guint pos;
  
  GtkCombo *combo;
  GtkSpinButton *red;
  GtkSpinButton *green;
  GtkSpinButton *blue;
  GtkEntry *name;
  GtkEntry *comment;
  CPreview *preview;
  GtkWidget *prev;
  GtkWidget *next;
  GtkWidget *close;
  GtkWidget *grab;
  GtkTable *table;
  
  GList *to_edit;
  int pass_thaw;
  
  gushort save_red;
  gushort save_green;
  gushort save_blue;
  
  gboolean graping;
};

struct _CColorEditClass {
  GnomeAppClass parent_class;
  
  void (*do_error) (CColorEdit *color_edit, CError *error);
};

GtkType     c_color_edit_get_type  (void);
CColorEdit *c_color_edit_new       (CColorBase *color_base, 
                                    int pass_thaw, GList *to_edit);
void        c_color_edit_construct (CColorEdit *color_edit, 
				    CColorBase *color_base, 
				    int pass_thaw, GList *to_edit);
					 
#endif
