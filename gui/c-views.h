/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_VIEWS_H
#define C_VIEWS_H

#include "gcolorsel2-gui.h"
#include "c-shell.h"
#include <gcolorsel2.h>
#include "c-color-edit.h"

#include <gtk/gtknotebook.h>

#define C_VIEWS_TYPE   (c_views_get_type ())
#define C_VIEWS(o)     (GTK_CHECK_CAST ((o), C_VIEWS_TYPE, CViews))

typedef struct _CViews       CViews;
typedef struct _CViewsClass  CViewsClass;

struct _CViews {
  GtkNotebook notebook;

  CShell *shell;

  GtkWidget *empty;
  
  GtkWidget *selection_view;

  guint clear_idle;
  GList *clear_list;
  
  CSelectType select_type;
  CViewType view_type;
};

struct _CViewsClass {
  GtkNotebookClass parent_class;    

  void (*do_popup) (CViews *views, GdkEventButton *event);  
  void (*do_select_change) (CViews *views);
};

GtkType       c_views_get_type            (void);
CViews       *c_views_new                 (CShell *shell, CSelectType select_type, CViewType view_type);
void          c_views_construct           (CViews *views, CShell *shell, CSelectType select_type, CViewType view_type);

gint          c_views_get_selection_view  (CViews *views);
gboolean      c_views_have_selected_by_id (CViews *views, int id);
GList        *c_views_get_selected_by_id  (CViews *views, int id);
guint         c_views_get_insert_pos      (CViews *views, int id);
GtkWidget    *c_views_get_widget          (CViews *views, int id);

CColorEdit   *c_views_cmd_insert          (CViews *views, gboolean show_edit, GtkWindow *parent);
void          c_views_cmd_remove          (CViews *views);
CColorEdit   *c_views_cmd_edit            (CViews *views, GtkWindow *parent);
void          c_views_cmd_paste           (CViews *views);

#endif
