/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "gcolorsel2.h"

#include <stdarg.h>
#include <glib.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

static const char *error_messages[] = {
  N_("Unknown error"),
  N_("Color server couldn't be contacted"), 
  N_("Bad connection number"),
  N_("Bad color position"),
  N_("Bad color RGB"),
  N_("Cannot open file"),
  N_("Cannot save file"),
  N_("Bad file"),
  N_("Read only")
};

static const int n_error_messages = sizeof (error_messages) / sizeof (error_messages[0]);

const char *
c_error_strtype (CErrorType type)
{
  g_assert (type < n_error_messages);
  
  return _(error_messages [type]);
}

static CError *
c_error_new_valist (CErrorType type, CErrorSeverity severity, const char *format, va_list args)
{
  CError *error;
  
  error = g_new0 (CError, 1);
  
  error->message = g_strdup_vprintf (format, args);
  error->type = type;
  error->severity = severity;
    
  return error;
}

CError *
c_error_new (CErrorType type, CErrorSeverity severity, const char *format, ...)
{
  CError *error;
  va_list args;
  
  va_start (args, format);
  error = c_error_new_valist (type, severity, format, args);
  va_end (args);
  
  return error;
}

CError *
c_error_new_simple (CErrorType type, CErrorSeverity severity, const char *message)
{
  CError *error;
  
  g_assert (message != NULL);
  
  error = g_new0 (CError, 1);
  
  error->type     = type;
  error->message  = g_strdup (message);
  error->severity = severity;
  
  return error;
}

void
c_error_free (CError *error)
{
  g_assert (error != NULL);
  
  g_free ((char *)error->message);
   
  g_free (error);
}
