/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-views.h"

#include <gcolorsel2.h>
#include "gcolorsel2-gui.h"
#include "c-color-list.h"
#include "c-color-edit.h"
#include "c-shell.h"

#include "e-clipped-label.h"

#include <glib.h>
#include <gtk/gtksignal.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkscrolledwindow.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

#define PARENT_TYPE GTK_TYPE_NOTEBOOK
static GtkNotebookClass *parent_class = NULL;

#define KEY_ID "id"
#define KEY_VIEW "view"

static GtkTargetEntry targets_table[] = {
  { "text/uri-list", 0, 0 }
};

static void shell_append    (CViews *views, const char *name, int id,
			     CColorBase *base, CShell *shell);
static void shell_switch_to (CViews *views, int id, CShell *shell);
static void shell_remove    (CViews *views, int id);

static GList   *c_views_get_selected  (CViews *views, GtkWidget *view);
static gboolean c_views_have_selected (CViews *views, GtkWidget *view);

static gint selection_clear_event (GtkWidget *widget, GdkEventSelection *event);
static void selection_get         (GtkWidget *widget, GtkSelectionData *selection_data, 
                                   guint info, guint time);
static void selection_received    (GtkWidget *widget, GtkSelectionData *selection_data,
                                   guint time);
static void drag_data_received    (GtkWidget *widget, GdkDragContext *context,
                                   gint x, gint y, GtkSelectionData *data,
                                   guint info, guint time);                               

static void c_views_clear_selection (CViews *views, GtkWidget *view);                                   
static GtkWidget *c_views_get_view (CViews *views, int id);

enum {
  DO_POPUP, 
  DO_SELECT_CHANGE,
  LAST_SIGNAL
};

static guint c_views_signals[LAST_SIGNAL] = { 0 };

static void
class_init (CViewsClass *class)
{
  GtkObjectClass   *object_class;
  GtkWidgetClass   *widget_class;
  GtkNotebookClass *notebook_class;
  
  object_class = (GtkObjectClass *) class;
  notebook_class = (GtkNotebookClass *) class;
  widget_class = (GtkWidgetClass *) class;
  
  c_views_signals[DO_POPUP] =
    gtk_signal_new ("do_popup",   
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CViewsClass, do_popup),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);
                    
  c_views_signals[DO_SELECT_CHANGE] =
    gtk_signal_new ("do_select_change",   
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CViewsClass, do_select_change),
                    gtk_marshal_NONE__NONE,
                    GTK_TYPE_NONE, 0);                    

  gtk_object_class_add_signals (object_class, c_views_signals, LAST_SIGNAL);
  
  widget_class->selection_clear_event = selection_clear_event;
  widget_class->selection_get = selection_get;
  widget_class->selection_received = selection_received;
  
  parent_class = gtk_type_class (GTK_TYPE_NOTEBOOK);
}

static void
init (CViews *views)
{
  views->selection_view = NULL;
  
  gtk_selection_add_target (GTK_WIDGET (views),
                            GDK_SELECTION_PRIMARY,
                            GDK_SELECTION_TYPE_STRING,
                            0);
}

void
c_views_construct (CViews *views, CShell *shell, CSelectType select_type, CViewType view_type)
{
  GtkWidget *label;

  g_assert (views != NULL);
  g_assert (shell != NULL);
  
  gtk_notebook_set_show_tabs (GTK_NOTEBOOK (views), FALSE);
  gtk_notebook_set_show_border (GTK_NOTEBOOK (views), FALSE);
  
  views->shell = shell;
  views->select_type = select_type;
  views->view_type = view_type;
  
  gtk_drag_dest_set (GTK_WIDGET (views), GTK_DEST_DEFAULT_MOTION |
                                         GTK_DEST_DEFAULT_HIGHLIGHT |
                                         GTK_DEST_DEFAULT_DROP,
                     targets_table, 1, GDK_ACTION_COPY);
                     
  gtk_signal_connect (GTK_OBJECT (views), "drag_data_received",
                      GTK_SIGNAL_FUNC (drag_data_received), NULL);                     

  gtk_signal_connect_object_while_alive (GTK_OBJECT (shell), "append",
					 shell_append, GTK_OBJECT (views));
  gtk_signal_connect_object_while_alive (GTK_OBJECT (shell), "switch_to",
					 shell_switch_to, GTK_OBJECT (views));
  gtk_signal_connect_object_while_alive (GTK_OBJECT (shell), "remove",
					 shell_remove, GTK_OBJECT (views));

  views->empty = gtk_vbox_new (FALSE, 0);
  gtk_container_set_border_width (GTK_CONTAINER (views->empty), 10);

  label = e_clipped_label_new (_("There are no palette to show here"));
  gtk_box_pack_start (GTK_BOX (views->empty), label, FALSE, FALSE, 5);
  gtk_widget_show (label);

  label = e_clipped_label_new (_("Go in the file menu to create or open one !"));
  gtk_box_pack_start (GTK_BOX (views->empty), label, FALSE, FALSE, 5);
  gtk_widget_show (label);

  gtk_notebook_append_page (GTK_NOTEBOOK (views), views->empty, NULL);

  gtk_widget_show (views->empty);
}

CViews *
c_views_new (CShell *shell, CSelectType select_type, CViewType view_type)
{
  CViews *views;
  
  views = C_VIEWS (gtk_type_new (c_views_get_type ()));
  
  c_views_construct (views, shell, select_type, view_type);
  
  return views;
}

static void 
drag_data_received (GtkWidget *widget, GdkDragContext *context,
                    gint x, gint y, GtkSelectionData *data,
                    guint info, guint time)
{
  GList *names, *list;
  CViews *views;
  
  views = C_VIEWS (widget);
  
  names = list = gnome_uri_list_extract_filenames (data->data);
  
  while (names) {
    CError *error = NULL;
  
    c_shell_open (views->shell, names->data, g_basename (names->data), FALSE, FALSE, &error);
    if (error) {
      c_shell_log_error (views->shell, error);
    }
  
    names = names->next;
  }
  
  gnome_uri_list_free_strings (list);
}                    

static gint
selection_clear_event (GtkWidget *widget, GdkEventSelection *event)
{
  CViews *views;
  
  if (! GTK_WIDGET_CLASS (parent_class)->selection_clear_event (widget, event))
    return FALSE;
  
  views = C_VIEWS (widget);
  
  if (views->selection_view) {

    c_views_clear_selection (views, views->selection_view);
  
    views->selection_view = NULL;
    
    gtk_signal_emit (GTK_OBJECT (views), c_views_signals[DO_SELECT_CHANGE]);
    
  }

  return TRUE;    
}

int
c_views_get_selection_view (CViews *views)
{
  if (! views->selection_view)
    return -1;
  
  return GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (views->selection_view), KEY_ID));
}

static void 
selection_get (GtkWidget *widget, GtkSelectionData *selection_data, 
               guint info, guint time)
{
  GList *list, *tmp;
  GString *string;
  guint pos;
  CColorBase *color_base;
  gushort red, green, blue;
  char *name;
  CError *error = NULL;
  int view_id;
  gboolean first = TRUE;
  
  tmp = list = c_views_get_selected (C_VIEWS (widget), C_VIEWS (widget)->selection_view);
  g_assert (list != NULL);
  string = g_string_new (NULL);

  view_id = c_views_get_selection_view (C_VIEWS (widget));
  color_base = c_shell_get_color_base (C_VIEWS (widget)->shell, view_id);
  g_assert (color_base != NULL);

  while (list) {
  
    pos = GPOINTER_TO_INT (list->data);
    
    c_color_base_get (color_base, pos, &red, &green, &blue, &name, NULL, &error);
    if (error) {
      c_shell_log_error (C_VIEWS (widget)->shell, error);
      break;
    }    

    if (first) {
      g_string_sprintfa (string, "%d %d %d\t\t%s", red, green, blue, name);  
      first = FALSE;
    } else {
      g_string_sprintfa (string, "\n%d %d %d\t\t%s", red, green, blue, name);    
    }
  
    list = list->next;
  }

  gtk_selection_data_set (selection_data, GDK_SELECTION_TYPE_STRING,
                          8, string->str, string->len);
                          
  g_list_free (tmp);
  g_string_free (string, TRUE);                          
}
                                   
static void 
selection_received (GtkWidget *widget, GtkSelectionData *selection_data,
                    guint time)
{
  CViews *views;
  CColorBase *color_base;
  CError *error = NULL;
  char *string;
  char name[1000];
  int red, green, blue;  
  guint pos;
  int id;
  int nb = 0;
  
  if (selection_data->length < 0) return;
  if (selection_data->type != GDK_SELECTION_TYPE_STRING) return;  
  
  views = C_VIEWS (widget);
  
  id = c_shell_get_current (views->shell);
  
  if (c_shell_read_only_dialog (views->shell, id))
    return;
    
  color_base = c_shell_get_color_base (views->shell, id);
  g_return_if_fail (color_base != NULL);
  
  pos = c_views_get_insert_pos (C_VIEWS (views), id);
  if (! pos)
    pos = c_color_base_get_length (color_base) + 1;    
  
  string = (char *)selection_data->data;
  
  while (string[0]) {
  
    name[0] = 0;
  
    if (sscanf (string, "%d %d %d\t\t%999[^\n]\n", &red, &green, &blue, name) < 3) {
      /* fail */  
      return;
    }
    
    c_color_base_insert (color_base, red, green, blue, name, ""); 
    nb++;
    
    while ((string[0]) && (string[0] != '\n'))
      string++;
      
    if (string[0] == '\n')
      string++;          
  }
  
  if (nb) {
    c_color_base_commit_insert (color_base, pos, &error);
    if (error) {
      c_shell_log_error (views->shell, error);
    }
  }
  
}                                   

static int
c_views_find (CViews *views, guint id)
{
  GList *list;
  GtkNotebookPage *page;
  guint tmp;
  int pos = 0;
  
  g_assert (views != NULL);
  
  list = GTK_NOTEBOOK (views)->children;
  
  while (list) {
  
    page = list->data;
    g_assert (page != NULL);

    tmp = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (page->child), 
						KEY_ID));

    if (tmp == id) return pos;

    pos++;
    
    list = list->next;
  }
  
  return -1;
}

static void
c_views_do_popup_list (CColorList *color_list, GdkEventButton *event, CViews *views)
{
  gtk_signal_emit (GTK_OBJECT (views), c_views_signals[DO_POPUP], event);
}

static void
c_views_do_error_list (CColorList *color_list, CError *error, CViews *views)
{
  c_shell_log_error (views->shell, error);
}

static void
c_views_do_select_change (GtkWidget *view, CViews *views)
{
  GtkWidget *tmp;
  gint have_selection = TRUE;
  
  tmp = views->selection_view;
  
  if (! c_views_have_selected (views, view)) {
  
    if (view == tmp) {
      gtk_selection_owner_set (NULL, GDK_SELECTION_PRIMARY, GDK_CURRENT_TIME);
      views->selection_view = NULL;
    }
  
  } else {
  
    if (tmp) {
      if (tmp != view) {
        c_views_clear_selection (views, tmp);
        views->selection_view = view;                                          
      } 
    } else {
    
      have_selection = gtk_selection_owner_set (GTK_WIDGET (views),
                                                GDK_SELECTION_PRIMARY,
                                                GDK_CURRENT_TIME);
                                              
      if (! have_selection) {
        g_warning ("can't get selection !\n");    
      } else {                                              
        views->selection_view = view;                                              
      }      
    }
  }
  
  gtk_signal_emit (GTK_OBJECT (views), c_views_signals[DO_SELECT_CHANGE]);
}

static GtkWidget *
c_views_create_view (CViews *views, CColorBase *color_base, 
                     const char *name, int id)
{
  GtkWidget *scrolled;
  GtkWidget *view = NULL;
  GtkPolicyType pol1 = GTK_POLICY_AUTOMATIC, pol2 = GTK_POLICY_AUTOMATIC;
  
  g_assert (views != NULL);
  g_assert (color_base != NULL);
  g_assert (name != NULL);

  scrolled = gtk_scrolled_window_new (NULL, NULL);  
  
  switch (views->view_type) {
  
    /* FIXME : considerer que toutes les vues ont la m�me interface, et
               connecter les signaux en dehors du switch */
    
    case C_VIEW_LIST:
    
      pol2 = GTK_POLICY_ALWAYS;
      
      view = GTK_WIDGET (c_color_list_new (color_base, views->select_type));
      
      gtk_signal_connect (GTK_OBJECT (view), "do_error",
                          GTK_SIGNAL_FUNC (c_views_do_error_list), views);

      gtk_signal_connect (GTK_OBJECT (view), "do_popup", 
                          GTK_SIGNAL_FUNC (c_views_do_popup_list), views);      
                          
      gtk_signal_connect (GTK_OBJECT (view), "do_select_change",
                          GTK_SIGNAL_FUNC (c_views_do_select_change), views);                          

      break;
      
    default:
      g_assert_not_reached ();
  }

  gtk_object_set_data (GTK_OBJECT (scrolled), KEY_ID, GINT_TO_POINTER (id));
  gtk_object_set_data (GTK_OBJECT (scrolled), KEY_VIEW, view);
  
  gtk_object_set_data (GTK_OBJECT (view), KEY_ID, GINT_TO_POINTER (id));
  
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled), pol1, pol2);
  
  gtk_container_add (GTK_CONTAINER (scrolled), view);

  gtk_notebook_append_page (GTK_NOTEBOOK (views), scrolled, NULL);

  gtk_widget_show (view);
  gtk_widget_show (scrolled);

  return scrolled;
}

static void 
shell_append (CViews *views, const char *name, int id, 
	      CColorBase *base, CShell *shell)
{
  GtkWidget *view;

  view = c_views_create_view (views, base, name, id);
}

static void 
shell_switch_to (CViews *views, int id, CShell *shell)
{
  int page;

  if (id != -1) {

    page = c_views_find (views, id);
    g_return_if_fail (page != -1);
    
  } else 

    page = gtk_notebook_page_num (GTK_NOTEBOOK (views), views->empty);

  gtk_notebook_set_page (GTK_NOTEBOOK (views), page);
  
  gtk_signal_emit (GTK_OBJECT (views), c_views_signals[DO_SELECT_CHANGE]);
}

static void
shell_remove (CViews *views, int id)
{
  int page;
  
  page = c_views_find (views, id);
  g_return_if_fail (page != -1);
  
  if (views->selection_view) {
    if (GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (views->selection_view), KEY_ID)) == id);
      gtk_selection_owner_set (NULL,
                               GDK_SELECTION_PRIMARY,
                               GDK_CURRENT_TIME);
      views->selection_view = NULL;
  }
  
  if (views->clear_list) {
    GtkWidget *view;
    
    view = c_views_get_view (views, id);
    views->clear_list = g_list_remove (views->clear_list, view);
  }

  gtk_notebook_remove_page (GTK_NOTEBOOK (views), page);
}

static GtkWidget *
c_views_get_view (CViews *views, int id)
{
  int pos;
  GtkWidget *child;
  
  pos = c_views_find (views, id);  
  
  if (pos == -1)
    return NULL;
    
  child = gtk_notebook_get_nth_page (GTK_NOTEBOOK (views), pos);  
  g_assert (child != NULL);
  
  return gtk_object_get_data (GTK_OBJECT (child), KEY_VIEW);
}

static void
c_views_clear_selection (CViews *views, GtkWidget *view)
{
  g_return_if_fail (views != NULL);
  g_return_if_fail (view != NULL);
  
  if (C_IS_COLOR_LIST (view)) {     
    c_color_list_clear_selection (C_COLOR_LIST (view), TRUE);
  } else {
    g_assert_not_reached ();
  }
}

static GList *
c_views_get_selected (CViews *views, GtkWidget *view)
{
  int length;
  
  g_return_val_if_fail (views != NULL, NULL);  
  g_return_val_if_fail (view != NULL, NULL);
  
  if (C_IS_COLOR_LIST (view)) {
  
    return c_color_list_get_selected (C_COLOR_LIST (view), &length);
  
  } 
  
  g_assert_not_reached ();
    
  return NULL;
}

GList *
c_views_get_selected_by_id (CViews *views, int id)
{
  GtkWidget *view;
  
  g_return_val_if_fail (views != NULL, NULL);
  
  view = c_views_get_view (views, id);
  if (! view) return NULL;
  
  return c_views_get_selected (views, view);
}

static gboolean
c_views_have_selected (CViews *views, GtkWidget *view)
{
  g_return_val_if_fail (views != NULL, FALSE);  
  g_return_val_if_fail (view != NULL, FALSE);
    
  if (C_IS_COLOR_LIST (view)) 
    return c_color_list_have_selected (C_COLOR_LIST (view));
  
  g_assert_not_reached ();
  return FALSE;  
}

gboolean 
c_views_have_selected_by_id (CViews *views, int id)
{
  GtkWidget *view;
  
  g_return_val_if_fail (views != NULL, FALSE);
  
  view = c_views_get_view (views, id);
  if (! view) return FALSE;
  
  return c_views_have_selected (views, view);
}

guint
c_views_get_insert_pos (CViews *views, int id)
{
  GtkWidget *view;

  g_return_val_if_fail (views != NULL, 0);  
  
  view = c_views_get_view (views, id);
  g_return_val_if_fail (view != NULL, 0);
  
  if (C_IS_COLOR_LIST (view)) {
  
    if (GTK_CLIST (view)->focus_row == -1)
      return 0;
  
    return GTK_CLIST (view)->focus_row + 1;
  
  } 
  
  g_assert_not_reached ();
    
  return 0;
}

GtkWidget *
c_views_get_widget (CViews *views, int id)
{
  g_return_val_if_fail (views != NULL, 0);  

  return c_views_get_view (views, id);
}

static void
edit_do_error (CColorEdit *color_edit, CError **error, CShell *shell)
{
  c_shell_log_error (shell, *error);
}

CColorEdit *
c_views_cmd_insert (CViews *views, gboolean show_edit, GtkWindow *parent)
{
  CColorBase *color_base;
  CColorEdit *edit;  
  CError *error = NULL;
  guint pos;
  int id;
  
  id = c_shell_get_current (views->shell);
  
  if (c_shell_read_only_dialog (views->shell, id)) 
    return NULL;
  
  color_base = c_shell_get_color_base (views->shell, id);
  g_return_val_if_fail (color_base != NULL, NULL);
  
  pos = c_views_get_insert_pos (C_VIEWS (views), id);
  if (! pos) 
    pos = c_color_base_get_length (color_base) + 1;
    
  c_color_base_insert (color_base, 255, 255, 255, _("New color"), "");
  c_color_base_commit_insert (color_base, pos, &error);
  
  if (error) {
    c_shell_log_error (C_VIEWS (views)->shell, error);
    return NULL;
  }
  
  if (! show_edit)
    return NULL;
    
  edit = c_color_edit_new (color_base, 1, g_list_append (NULL, GINT_TO_POINTER (pos)));

  if (parent) {
    gtk_window_set_transient_for (GTK_WINDOW (edit), parent);
  }
  
  gtk_widget_show (GTK_WIDGET (edit));
  
  gtk_signal_connect (GTK_OBJECT (edit), "do_error", edit_do_error, views->shell);

  gtk_signal_connect_object_while_alive (GTK_OBJECT (views->shell), "destroy",
                                         gtk_widget_destroy, GTK_OBJECT (edit));        
                                         
  return edit;
}

void
c_views_cmd_remove (CViews *views)
{
  CError *error = NULL;
  int id;
  CColorBase *color_base;
  GList *list;
  
  id = c_shell_get_current (views->shell);
  
  if (c_shell_read_only_dialog (views->shell, id)) return;
  
  color_base = c_shell_get_color_base (views->shell, id);
  g_return_if_fail (color_base != NULL);
  
  list = c_views_get_selected_by_id (views, id);
  
  if (list) {
  
    c_color_base_remove (color_base, list, &error);
    
    if (error)
      c_shell_log_error (views->shell, error);

    g_list_free (list);
    
  }
}

CColorEdit *
c_views_cmd_edit (CViews *views, GtkWindow *parent)
{
  CColorBase *color_base;
  GList *list;
  int id;
  CColorEdit *edit = NULL;
  
  id = c_shell_get_current (views->shell);
  
/*  if (c_shell_read_only_dialog (views->shell, id)) return NULL; */
  
  color_base = c_shell_get_color_base (views->shell, id);
  g_return_val_if_fail (color_base != NULL, NULL);
  
  list = c_views_get_selected_by_id (views, id);
  
  if (list) {
  
    edit = c_color_edit_new (color_base, 0, list);

    if (parent) {
      gtk_window_set_transient_for (GTK_WINDOW (edit), parent);    
    }
    
    gtk_widget_show (GTK_WIDGET (edit));

    gtk_signal_connect (GTK_OBJECT (edit), "do_error", edit_do_error, views->shell);

    gtk_signal_connect_object_while_alive (GTK_OBJECT (views->shell), "destroy",
                                           gtk_widget_destroy, GTK_OBJECT (edit));      
  }
  
  return edit;
}

void
c_views_cmd_paste (CViews *views)
{
  gtk_selection_convert (GTK_WIDGET (views), GDK_SELECTION_PRIMARY,
                         GDK_SELECTION_TYPE_STRING, GDK_CURRENT_TIME);
}

C_MAKE_TYPE (c_views, "CViews", CViews, class_init, init, PARENT_TYPE)
