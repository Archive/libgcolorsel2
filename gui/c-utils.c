/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "c-utils.h"
#include "e-clipped-label.h"

#include <gnome.h>

GtkWidget *
c_utils_widget_rgb (int r, int g, int b, const char *str)
{
  GtkWidget *window;
  GdkColor bg;
 
  window = gtk_window_new (GTK_WINDOW_POPUP);
  gtk_widget_set_app_paintable (GTK_WIDGET (window), TRUE);
  gtk_widget_set_usize (window, 48, 32);
  gtk_widget_realize (window);
 
  bg.red   = (r / 255.0) * 0xffff;
  bg.green = (g / 255.0) * 0xffff;
  bg.blue  = (b / 255.0) * 0xffff;  

  gdk_color_alloc (gtk_widget_get_colormap (window), &bg);
  gdk_window_set_background (window->window, &bg);
  
  if (str) {
    GtkWidget *label;
    
    label = e_clipped_label_new (str);
    
    gtk_container_add (GTK_CONTAINER (window), label);  
    
    gtk_widget_show (label);
  }

  return window;
}