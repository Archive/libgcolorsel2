/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GCOLORSEL2_H
#define GCOLORSEL2_H

#include <glib.h>
#include <gtk/gtkobject.h>

/***************** Make type macros */

#define C_MAKE_TYPE(l,str,t,ci,i,parent) \
GtkType l##_get_type(void)\
{\
  	static GtkType type = 0;\
  	if (!type){\
  	        GtkTypeInfo info = {\
  	                str,\
		        sizeof (t),\
	                sizeof (t##Class),\
		        (GtkClassInitFunc) ci,\
		        (GtkObjectInitFunc) i,\
	  		NULL,\
	           	NULL,\
 		   	(GtkClassInitFunc) NULL\
	  	};\
          	type = gtk_type_unique (parent, &info);\
 	}\
 	return type;\
}

/***************** CError */

typedef enum {
  C_ERROR_UNKNOWN = 0,
  C_ERROR_NO_SERVER,  
  C_ERROR_BAD_CONNECT_NUM,
  C_ERROR_BAD_POSITION,   
  C_ERROR_BAD_RGB,
  C_ERROR_CANNOT_OPEN_FILE,
  C_ERROR_CANNOT_SAVE_FILE,
  C_ERROR_BAD_FILE,
  C_ERROR_READ_ONLY
} CErrorType;

typedef enum {
  C_ERROR_BUG,	  /* Bad position, ... (should never append) */
  C_ERROR_SERVER, /* No server, ... */
  C_ERROR_USER    /* File not found, ... */
} CErrorSeverity;

typedef struct _CError CError;

struct _CError {
  const char *message;

  CErrorType type;
  CErrorSeverity severity;
};
  
CError      *c_error_new (CErrorType type, CErrorSeverity severity, const char *format, ...);
CError      *c_error_new_simple (CErrorType type, CErrorSeverity severity, const char *message);

void        c_error_free (CError *error);

const char *c_error_strtype (CErrorType type);

/***************** CColorBase */

#define C_COLOR_BASE_TYPE   (c_color_base_get_type ())
#define C_COLOR_BASE(o)     (GTK_CHECK_CAST ((o), C_COLOR_BASE_TYPE, CColorBase))
#define C_COLOR_BASE_GET_CLASS(o) ((CColorBaseClass *) (GTK_OBJECT (o)->klass))

typedef struct {
  GtkObject object;

  int freeze_count;
  
  gboolean data_received;
  
  gboolean read_only;
} CColorBase;

typedef struct {
  GtkObjectClass parent_class;

  void (*do_remove) (CColorBase *color_base, GList *pos, CError **error);
  void (*do_insert) (CColorBase *color_base, 
		     gushort red, gushort green, gushort blue,
		     const char *name, const char *comment);

  void (*do_commit_insert) (CColorBase *color_base, guint pos, CError **error);

  void (*do_change) (CColorBase *color_base, guint pos,
                     gushort red, gushort green, gushort blue,
                     const char *name, const char *comment, CError **error);

  void (*do_get) (CColorBase *color_base, guint pos,
		  gushort *red, gushort *green, gushort *blue,
		  char **name, char **comment, CError **error);
		  
  void (*do_move) (CColorBase *color_base, guint to,
                   GList *pos, CError **error);		  

  guint (*do_get_length) (CColorBase *color_base);
	
  void (*freeze)       (CColorBase *color_base);
  void (*thaw)         (CColorBase *color_base);
	
  void (*remove_color) (CColorBase *color_base, guint pos);
  void (*insert_color) (CColorBase *color_base, guint pos);
  void (*move_color)   (CColorBase *color_base, guint pos, int offset);  
  void (*change_color) (CColorBase *color_base, guint pos);
} CColorBaseClass;

GtkType     c_color_base_get_type (void);
CColorBase *c_color_base_new      (void);

void c_color_base_remove        (CColorBase *color_base, GList *pos, CError **error);

void c_color_base_insert        (CColorBase *color_base, 
			         gushort red, gushort green, gushort blue, 
			         const char *name, const char *comment);

void c_color_base_move          (CColorBase *color_base, guint to,
                                 GList *list, CError **error);			    

void c_color_base_commit_insert (CColorBase *color_base, guint pos,
				 CError **error);
				 
void c_color_base_change        (CColorBase *color_base, guint pos,
                                 gushort red, gushort green, gushort blue,
                                 const char *name, const char *comment, CError **error);

void c_color_base_get           (CColorBase *color_base, guint pos,
				 gushort *red, gushort *green, gushort *blue,
				 char **name, char **comment, CError **error);

guint c_color_base_get_length   (CColorBase *color_base);

void c_color_base_connect       (GtkObject *view, CColorBase *base,
                                 GtkSignalFunc freeze, GtkSignalFunc thaw,
                                 GtkSignalFunc insert, GtkSignalFunc remove,
                                 GtkSignalFunc move, GtkSignalFunc change);
  
/***************** CColorClient */

#define C_COLOR_CLIENT_TYPE  (c_color_client_get_type ())
#define C_COLOR_CLIENT(o)    (GTK_CHECK_CAST ((o), C_COLOR_CLIENT_TYPE, CColorClient))
#define C_IS_COLOR_CLIENT(o) (GTK_CHECK_TYPE ((o), C_COLOR_CLIENT_TYPE))

typedef struct {
  CColorBase color_base;
      
  char *path;
  
  gpointer private;    
} CColorClient;
  
typedef struct {
  CColorBaseClass parent_class;
  
} CColorClientClass;

GtkType       c_color_client_get_type  (void);
CColorClient *c_color_client_new       (const char *path, gboolean try_create,
                                        gboolean truncate, CError **error);
void          c_color_client_construct (CColorClient *color_client, const char *path,
                                        gboolean try_create, gboolean truncate,
                                        CError **error);                                        

/***************** CColorInfo */

typedef struct {
  char *name;
  guint nb_colors;
  guint nb_connects;
  gboolean read_only;  
} CColorInfo;

void c_color_client_get_server_info (char **version, GList **info, CError **error);                                        
void c_color_client_info_free       (GList *info);
                                        
/***************** CColorFilter */

#define C_COLOR_FILTER_TYPE (c_color_filter_get_type ())
#define C_COLOR_FILTER(o)   (GTK_CHECK_CAST ((o), C_COLOR_FILTER_TYPE, CColorFilter))

typedef struct {
  CColorBase color_base;
  
} CColorFilter;

typedef struct {
  CColorBaseClass parent_class;
  
} CColorFilterClass;

GtkType c_color_filter_get_type (void);
CColorFilter *c_color_filter_new  (void);

/***************** Library */

extern const char *gcolorsel_system_paths[];

char       *gcolorsel_get_user_file_path (void);
                            
gboolean gcolorsel_init           (int argc, char **argv);

gboolean gcolorsel_activate       (void);

void gcolorsel_preinit            (gpointer app, gpointer mod_info);
void gcolorsel_postinit           (gpointer app, gpointer mod_info);

gboolean gcolorsel_is_initialized (void);

extern const char gcolorsel_version[];

/* FIXME : remove below functions */

void c_color_base_emit_freeze  (CColorBase *color_base);
void c_color_base_emit_thaw    (CColorBase *color_base);
void c_color_base_emit_insert  (CColorBase *color_base, guint to);
void c_color_base_emit_change  (CColorBase *color_base, guint pos);
void c_color_base_emit_move    (CColorBase *color_base, 
				guint from, guint to, int offset);
void c_color_base_emit_remove  (CColorBase *color_base, guint pos);
                                
#endif
