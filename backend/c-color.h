/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_COLOR_H
#define C_COLOR_H

#include <glib.h>

typedef struct _CColor CColor;

struct _CColor {
  gushort red;
  gushort green;
  gushort blue;
  
  char *name;
  char *comment;
};

CColor *c_color_new  (gushort red, gushort green, gushort blue, const char *name, const char *comment);
void    c_color_free (CColor *color);

#endif
