/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_CONNECT_H
#define C_CONNECT_H

#include "gcolorsel2.h"
typedef struct _CConnect CConnect;
#include "c-palette.h"
#include "c-palette-list.h"

#include <glib.h>

struct _CConnect {
  guint cnum;
  
  ColorClient client;
  
  CPaletteListData *palette;
  
  guint timeout;
};

CPaletteListData *c_connect_get_palette_data      (CConnect *connect);
CPalette         *c_connect_get_palette           (CConnect *connect);

ColorClient       c_connect_get_client            (CConnect *connect);

CConnect         *c_connect_list_connect          (ColorClient client, 
                                                   const char *path,
                                                   gboolean try_create,
                                                   gboolean truncate,                                                  
                                                   CError **error);
void              c_connect_list_disconnect       (CConnect *connect);

void              c_connect_list_alive            (CConnect *connect);

CPaletteListData *c_connect_get_palette_data      (CConnect *connect);
CPalette         *c_connect_get_palette           (CConnect *connect);

CConnect         *c_connect_list_get              (guint cnum, CError **error);

CPaletteListData *c_connect_list_get_palette_data (guint cnum, CError **error);
CPalette         *c_connect_list_get_palette      (guint cnum, CError **error);

#endif
