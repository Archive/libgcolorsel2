/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "gcolorsel2-gui.h"
#include "c-shell.h"
#include <gcolorsel2.h>

#include <gnome.h>

#define PARENT_TYPE GTK_TYPE_OBJECT
static GtkObjectClass *parent_class = NULL;
 
enum {
  APPEND, 
  REMOVE,
  SWITCH_TO,
  RENAME,
  LAST_SIGNAL
};

static guint c_shell_signals[LAST_SIGNAL] = { 0 };

typedef struct view_t {
  CColorBase *color_base;

  int id;

  char *name;
} view_t;

static void
destroy (GtkObject *object)
{
  CShell *shell;
  GList  *list;
  view_t *view;
  
  shell = C_SHELL (object);
  
  list = shell->views;
  
  while (list) {
  
    view = list->data;
    
    list = list->next;
    
    c_shell_close (shell, view->id);
  }
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
class_init (CShellClass *class)
{
  GtkObjectClass   *object_class;
  
  object_class = (GtkObjectClass *) class;
  
  parent_class = gtk_type_class (GTK_TYPE_OBJECT);
  
  c_shell_signals[APPEND] = 
    gtk_signal_new ("append",
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CShellClass, append),
                    gtk_marshal_NONE__STRING_INT_POINTER,
                    GTK_TYPE_NONE, 3, GTK_TYPE_STRING,
		    GTK_TYPE_INT, GTK_TYPE_POINTER);

  c_shell_signals[REMOVE] = 
    gtk_signal_new ("remove",
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CShellClass, remove),
                    gtk_marshal_NONE__INT,
                    GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  c_shell_signals[SWITCH_TO] = 
    gtk_signal_new ("switch_to",
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CShellClass, switch_to),
                    gtk_marshal_NONE__INT,
                    GTK_TYPE_NONE, 1, GTK_TYPE_INT);

  c_shell_signals[RENAME] = 
    gtk_signal_new ("rename",
                    GTK_RUN_LAST, 
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CShellClass, rename),
                    gtk_marshal_NONE__INT_POINTER,
                    GTK_TYPE_NONE, 2, GTK_TYPE_INT, GTK_TYPE_POINTER);
                    
  gtk_object_class_add_signals (object_class, c_shell_signals, LAST_SIGNAL);
  
  object_class->destroy = destroy;
}

static void
init (CShell *shell)
{
  shell->id = 0;

  shell->clients = NULL;
  shell->views = NULL;

  shell->current = -1;  
}

void
c_shell_construct (CShell *shell)
{
  g_return_if_fail (shell != NULL);
}

CShell *
c_shell_new (void)
{
  CShell *shell;
  
  shell = C_SHELL (gtk_type_new (c_shell_get_type ()));
  
  return shell;
}

static view_t *
c_shell_find_view (CShell *shell, int id)
{
  GList *list;
  view_t *view;

  g_return_val_if_fail (shell != NULL, NULL);

  list = shell->views;

  while (list) {

    view = list->data;
    if (view->id == id)
      return view;

    list = list->next;
  }

  return NULL;
}

static CColorClient *
c_shell_find_client (CShell *shell, const char *path)
{
  GList *list;
  CColorClient *client;
  
  g_return_val_if_fail (shell != NULL, NULL);
  g_return_val_if_fail (path != NULL, NULL);
  
  list = shell->clients;

  /* FIXME */
  
  while (list) {
  
    client = list->data;
    g_assert (client != NULL);
    
    if (! strcmp (client->path, path)) 
      return client;
    
    list = list->next;
  }
  
  return NULL;
}

static void
color_client_destroy (CColorClient *color_client, CShell *shell)
{
  shell->clients = g_list_remove (shell->clients, color_client);
}

int
c_shell_open (CShell *shell, const char *path, 
	      const char *name, gboolean try_create, gboolean truncate, 
	      CError **error)
{
  CColorClient *color_client;
  view_t *view;

  g_return_val_if_fail (shell != NULL, 0);
  g_return_val_if_fail (path != NULL, 0);
  g_return_val_if_fail (name != NULL, 0);
  
  while ((path[0]) && (path[0] == '/') && (path[1]) && (path[1] == '/')) path++;
  
  color_client = c_shell_find_client (shell, path);

  if (! color_client) {
  
    color_client = c_color_client_new (path, try_create, truncate, error);

    if (*error) { 
      gtk_object_destroy (GTK_OBJECT (color_client));
      return 0;
    }

    shell->clients = g_list_prepend (shell->clients, color_client);

    gtk_signal_connect (GTK_OBJECT (color_client), "destroy", 
			color_client_destroy, shell);
			
    gtk_object_ref (GTK_OBJECT (color_client));
    gtk_object_sink (GTK_OBJECT (color_client));			
			
  } else  
   
    gtk_object_ref (GTK_OBJECT (color_client));

  view = g_new0 (view_t, 1);
  view->id = ++shell->id;
  view->color_base = C_COLOR_BASE (color_client);
  view->name = g_strdup (name);

  shell->views = g_list_prepend (shell->views, view);

  gtk_signal_emit (GTK_OBJECT (shell), c_shell_signals[APPEND], 
		   view->name, view->id, view->color_base);

  if (shell->current == -1)
    c_shell_switch_to (shell, view->id);
    
  return view->id;    
}

int 
c_shell_open_user (CShell *shell, const char *name,
                   CError **error)
{
  char *path;
  int id;

  path = gcolorsel_get_user_file_path ();

  id = c_shell_open (shell, path, name, TRUE, FALSE, error);

  g_free (path);
  
  return id;
}

int   
c_shell_open_system (CShell *shell, const char *name,  
                     CError **error)
{
  int i=0;
  int id;

  while (gcolorsel_system_paths[i]) {

    id = c_shell_open (shell, gcolorsel_system_paths[i], name, FALSE, FALSE, error);

    if (! *error) return id;

    i++;
  }

  if (*error) {
   
    c_error_free (*error);

    *error = c_error_new (C_ERROR_CANNOT_OPEN_FILE, C_ERROR_USER,
                          _("Cannot open colors system file for reading !"));

  }                          
  
  return 0;
}

void          
c_shell_switch_to (CShell *shell, int id)
{
  g_return_if_fail (shell != NULL);
  
  /* FIXME : check id ? */
  
  shell->current = id;

  gtk_signal_emit (GTK_OBJECT (shell), c_shell_signals[SWITCH_TO], id);
}

int          
c_shell_get_current (CShell *shell)
{
  g_return_val_if_fail (shell != NULL, -1);
  
  return shell->current;
}

CColorBase *
c_shell_get_color_base (CShell *shell, int id)
{
  view_t *view;
  
  g_return_val_if_fail (shell != NULL, NULL);
  
  view = c_shell_find_view (shell, id);
  if (!view) return NULL;
  
  return view->color_base;
}

static int
c_shell_get_nearest (CShell *shell, int id)
{
  GList *list;
  int nearest = 0;
  view_t *view;

  g_return_val_if_fail (shell != NULL, -1);

  list = shell->views;

  if (!list) return -1;

  view = list->data;
  nearest = view->id;

  list = list->next;

  while (list) {

    view = list->data;

    if (abs (view->id - id) < abs (nearest - id))
      
      nearest = view->id;

    list = list->next;
  }
  
  return nearest;
}

void
c_shell_close (CShell *shell, int id)
{
  view_t *view;

  g_return_if_fail (shell != NULL);

  view = c_shell_find_view (shell, id);
  g_return_if_fail (view != NULL);

  gtk_object_unref (GTK_OBJECT (view->color_base));

  g_free (view->name);
  g_free (view);

  shell->views = g_list_remove (shell->views, view);

  gtk_signal_emit (GTK_OBJECT (shell), c_shell_signals[REMOVE], id);

  if (shell->current == id) 
    
    c_shell_switch_to (shell, c_shell_get_nearest (shell, id));
    
}

void         
c_shell_rename (CShell *shell, int id, const char *name)
{
  view_t *view;
  
  g_return_if_fail (shell != NULL);
  g_return_if_fail (name != NULL);

  view = c_shell_find_view (shell, id);
  g_return_if_fail (view != NULL);

  g_free (view->name);
  view->name = g_strdup (name);

  gtk_signal_emit (GTK_OBJECT (shell), c_shell_signals[RENAME], 
		   id, name);
}

const char *
c_shell_get_name (CShell *shell, int id)
{
  view_t *view;
  
  g_return_val_if_fail (shell != NULL, NULL);
  
  if (id == -1) return NULL;
  
  view = c_shell_find_view (shell, id);
  g_return_val_if_fail (view != NULL, NULL);
  
  return view->name;
}

gboolean
c_shell_get_read_only (CShell *shell, int id)
{
  view_t *view;
  
  g_return_val_if_fail (shell != NULL, FALSE);
  
  if (id == -1) return FALSE;
  
  view = c_shell_find_view (shell, id);
  g_return_val_if_fail (view != NULL, FALSE);
  
  return view->color_base->read_only;
}

void
c_shell_log_error (CShell *shell, CError *error)
{
  static GtkWidget *dialog = NULL;
  char *buf = NULL;
  
  g_return_if_fail (shell != NULL);
  g_return_if_fail (error != NULL);
  
  if (dialog) {
    /* FIXME : add next button, ... */  
    g_warning ("Error received, but I can't show it ! : %s\n", error->message);
    return;
  }
  
  switch (error->severity) {
  
  case C_ERROR_USER:
   
    buf = g_strdup_printf (_("Error : %s\n\n%s"), 
                           c_error_strtype (error->type), error->message);
                           
    break;
    
  case C_ERROR_BUG:                           
                           
    buf = g_strdup_printf (_("Oups ! I have got an error message :\n\n%s\n\nType : %s\n\nThis genereally means that the client or the server is buggy !"), 
                           error->message, c_error_strtype (error->type));
                           
    break;
    
  case C_ERROR_SERVER:
  
    buf = g_strdup_printf (_("Error : %s\n\n%s\n\nPlease, check that the server is correctly installed ..."), 
                           c_error_strtype (error->type), error->message);                           
    
    break;
    
  default:
  
    g_assert_not_reached ();    
    
  }

  dialog = gnome_message_box_new (buf, GNOME_MESSAGE_BOX_ERROR, GNOME_STOCK_BUTTON_OK, NULL);
  
  g_free (buf);
  
  gtk_widget_show (GTK_WIDGET (dialog));
  
  gtk_signal_connect (GTK_OBJECT (dialog), "destroy", 
                      GTK_SIGNAL_FUNC (gtk_widget_destroyed), &dialog);
  
  c_error_free (error);
}

gboolean
c_shell_read_only_dialog (CShell *shell, int id)
{
  view_t *view;
  GtkWidget *dialog;
  char *buf;
  
  g_return_val_if_fail (shell != NULL, FALSE);
  
  if (id == -1) return FALSE;
  
  view = c_shell_find_view (shell, id);
  g_return_val_if_fail (view != NULL, FALSE);
  
  if (view->color_base->read_only) {
  
    buf = g_strdup_printf (_("Cannot change '%s' : read only !"), view->name);

    dialog = gnome_message_box_new (buf, GNOME_MESSAGE_BOX_ERROR, GNOME_STOCK_BUTTON_OK, NULL);  

    g_free (buf);
    
    gtk_widget_show (dialog);  
    
    return TRUE;
  }
  
  return FALSE;
}

int
c_shell_for_each_view (CShell *shell, CShellForeachFunc func, gpointer data)
{
  GList *list;
  view_t *view;
  int nb = 0;
  
  g_return_val_if_fail (shell != NULL, 0);
  g_return_val_if_fail (func != NULL, 0);
  
  /* Ok, we traverse the list in reverse mode because when a view is created,
     c-shell prepend it to the list, ... */
  list = g_list_last (shell->views);
  while (list) {
  
    view = list->data;
  
    func (shell, view->id, view->name, view->color_base, data);
  
    list = list->prev; nb++;
  }  
  
  return nb;
}

C_MAKE_TYPE (c_shell, "CShell", CShell, class_init, init, PARENT_TYPE)
