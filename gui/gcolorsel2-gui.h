/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef GCOLORSEL2_GUI_H
#define GCOLORSEL2_GUI_H

#include <glib.h>

typedef enum {
  C_SELECT_SINGLE,
  C_SELECT_MULTIPLE
} CSelectType;

typedef enum {
  C_VIEW_LIST
} CViewType;

extern const char gcolorsel_gui_version[];

void     gcolorsel_gui_preinit         (gpointer app, gpointer mod_info);
void     gcolorsel_gui_postinit        (gpointer app, gpointer mod_info);

gboolean gcolorsel_gui_init            (int argc, char **argv);
gboolean gcolorsel_gui_is_initialized  (void);

#endif
