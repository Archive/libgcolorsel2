/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-palette.h"

#include "c-color.h"
#include "gcolorsel2.h"

#include "GColorsel2.h"

#include <glib.h>
#include <liboaf/liboaf.h>
#include <string.h>
#include <stdio.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

/*static*/ void
c_palette_dump (CPalette *palette)
{
  guint i;
  
  g_return_if_fail (palette != NULL);
  
  printf ("************* dump : %d colors *************\n", palette->nb_elem);
    
  for (i=0; i<palette->nb_elem; i++) {
    CColor *color = palette->data[i];
    
    if (color) {
      printf ("%4d : %3d   %3d   %3d    |   '%s'  '%s'\n", i + 1, color->red, color->green, color->blue, color->name, color->comment);
    } else {
      printf ("%4d : ARRRGRRG ... ERROR, COLOR NOT FOUND !!!\n", i + 1);
    }
  }
}

CPalette *
c_palette_new ()
{
  CPalette *palette;

  palette = g_new0 (CPalette, 1);  
  
  palette->nb_elem   = 0;
  palette->size      = 0;
  palette->data      = NULL;
  palette->header    = NULL;
    
  return palette;  
}

void 
c_palette_free (CPalette *palette)
{
  guint i;
  
  g_assert (palette != NULL);
  
  if (palette->data) {
  
    for (i=0; i<palette->nb_elem; i++) 
        c_color_free (palette->data[i]);
      
    g_free (palette->data);      
  
  }
  
  if (palette->header)
    g_free (palette->header);
    
  g_free (palette);
}

static void 
c_palette_alloc (CPalette *palette, guint new_nb_elem)
{
  g_assert (palette != NULL);
  
  palette->nb_elem = new_nb_elem;
     
  if (palette->size < palette->nb_elem) {
  
    while (palette->size < palette->nb_elem) {
  
      if (palette->size)
        palette->size *= 2;
      else
        palette->size = 100;
    
    }
      
    if (palette->data) 
      palette->data = g_realloc (palette->data, palette->size * sizeof (gpointer));
    else 
      palette->data = g_malloc (palette->size * sizeof (gpointer));

  }
  
  /* FIXME : si trop de place libre, reduire la taille ... */
}

gpointer 
c_palette_get (CPalette *palette, guint pos, CError **error)
{
  g_assert (palette != NULL);
  
  if ((pos < 1) || (pos > palette->nb_elem)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                           _("Bad position in GET : %d"), pos);
    return NULL;
  }
  
  return palette->data[pos - 1];
}   

static void
c_palette_private_move (CPalette *palette, guint from, guint to, guint offset, 
                        CColorBase *base, CError **error)
{
  g_assert (palette != NULL);
  
  if (from > to) {
    int temps = from;
    from = to;
    to = temps;
  }
  
  if ((from < 1) || (to > palette->nb_elem)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                          _("Bad positions in MOVE (PRIVATE) : from = %d to = %d"), from, to);
    return;
  }                          
  
  if ((from + offset < 1) || (to + offset > palette->nb_elem)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                          _("Bad offset in MOVE (PRIVATE) : from = %d to = %d offset = %d"), from, to, offset);
    return;
  }
  
  if (base) {
    c_color_base_emit_move (base, from, to, offset);
  }
  
  g_memmove (palette->data + from + offset - 1,
             palette->data + from - 1,
             (to - from + 1) * sizeof (gpointer));
}

static void
c_palette_append (CPalette *palette, gushort red, gushort green,  
                  gushort blue, const char *name, const char *comment)
{
  CColor *color;
  
  g_assert (palette != NULL);
  g_assert (name    != NULL);
  g_assert (comment != NULL);
  
  color = c_color_new (red, green, blue, name, comment);
  
  c_palette_alloc (palette, palette->nb_elem + 1);
  
  palette->data[palette->nb_elem - 1] = color;  
}                                    

void
c_palette_insert (CPalette *palette, guint pos,
                  const ColorList *color_list, CColorBase *base, CError **error)
{
  CColor *color;
  Color  *corba_color;
  int i;
  
  g_assert (palette    != NULL);
  g_assert (color_list != NULL); 
  
  if ((pos < 1) || (pos > palette->nb_elem + 1)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                          _("Bad position in INSERT : pos = %d"), pos);
    return;
  }
  
  c_palette_alloc (palette, palette->nb_elem + color_list->_length);
  
  if ((pos < palette->nb_elem) && (pos <= palette->nb_elem - color_list->_length)) {
    c_palette_private_move (palette, pos, palette->nb_elem - color_list->_length, + color_list->_length, base, error);
    g_assert (*error == NULL);
  }    

  for (i=0; i<color_list->_length; i++) { 
  
    corba_color = &(color_list->_buffer[i]);
  
    color = c_color_new (corba_color->red, 
                         corba_color->green,
                         corba_color->blue,
                         corba_color->name,
                         corba_color->comment);
    
    palette->data[(pos + i) - 1] = color;
    
    if (base) {
      c_color_base_emit_insert (base, pos + i);
    }
    
  }    	     

}

static void
c_palette_check_list_pos (CPalette *palette, const ColorPosList *list_pos, 
                          const char *msg, CError **error)
{
  guint i;
  
  g_assert (palette != NULL);
  g_assert (list_pos != NULL);
  
  /* Check that position are sorted ... */
  
  for (i = 0; i < list_pos->_length - 1; i++) {
  
    if (list_pos->_buffer[i] >= list_pos->_buffer[i + 1]) {
      *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                            _("Bad positions(s) in %s : must be sorted and not equal"), msg);  
      return;                           
    }
  }
  
  /* Check that first and last position exit ... */
  
  if ((list_pos->_buffer[0] < 1) || (list_pos->_buffer[list_pos->_length - 1] > palette->nb_elem)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                          _("Bad position(s) in %s"), msg);
  }
}

void 
c_palette_remove (CPalette *palette, const ColorPosList *list_pos, 
                  CColorBase *base, CError **error)
{
  guint pos, pos_next = 0, not_last = 1;
  guint i;
  gint offset = 0;
  
  g_assert (palette != NULL);
  g_assert (list_pos != NULL);
  
  if (list_pos->_length == 0) {
    return;
  }

  c_palette_check_list_pos (palette, list_pos, "REMOVE", error);
  if (*error) {
    return;
  }

  pos = list_pos->_buffer[0]; 

  for (i = 0; i < list_pos->_length; i++) { 
    
    if (i < list_pos->_length - 1) 
      pos_next = list_pos->_buffer[i+1];      
    else {
      pos_next = palette->nb_elem;      
      not_last = 0;
    }

    offset--;
    
    /* Remove color */
    
    if (base) {
      c_color_base_emit_remove (base, pos);
    }
    c_color_free (palette->data[pos - 1]);

    /* Reorganize array */
    
    if ((pos + 1 < pos_next) || ((! not_last) && (pos < pos_next))) {
    
      /* Shift to the left colors from 'pos' to 'pos_next' for 'offset' position */
      
      c_palette_private_move (palette, pos + 1, pos_next - not_last, offset, base, error);
      g_assert (*error == NULL);
    }

    pos = pos_next;
  }
    
  c_palette_alloc (palette, palette->nb_elem + offset);
  
  g_assert (palette->nb_elem >= 0);
}

void
c_palette_move (CPalette *palette, guint to, const ColorPosList *list_pos, 
                CColorBase *base, CError **error)
{
  guint i;  
  guint real_length, save_to = to;
  
  g_assert (palette != NULL);
  g_assert (list_pos != NULL);
  
  if (list_pos->_length == 0) {
    return;
  }

  if ((to < 1) || (to > palette->nb_elem + 1)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                          _("Bad position in MOVE : pos = %d"), to);
    return;  
  }
  
  c_palette_check_list_pos (palette, list_pos, "MOVE", error);
  if (*error) {
    return;
  }

  real_length = palette->nb_elem;
  c_palette_alloc (palette, palette->nb_elem + list_pos->_length);  

  for (i = 0; i < list_pos->_length; i++) {

    int pos = list_pos->_buffer[i];
    
    c_palette_private_move (palette, pos, pos, real_length - pos + i + 1, base, error);

    if (i + 1 < list_pos->_length) {

      if (pos + 1 < list_pos->_buffer[i + 1]) {      
        c_palette_private_move (palette, pos + 1, list_pos->_buffer[i + 1] - 1, - (i + 1), base, error);
        g_assert (error != NULL);
      }
      
      if (pos < save_to) to = to - 1;
      
    } else {
    
      if (pos < real_length) {
        c_palette_private_move (palette, pos + 1, real_length, - (i + 1), base, error);
        g_assert (error != NULL);
      }
      
      if (pos < save_to) to = to - 1;       
      
    }
           
  }  
  
  if (to + list_pos->_length < real_length + 1) {
    c_palette_private_move (palette, to, real_length - list_pos->_length, list_pos->_length, base, error);  
    g_assert (error != NULL);
  }
  
  c_palette_private_move (palette, real_length + 1, palette->nb_elem, to - real_length - 1, base, error);
  g_assert (error != NULL);
  
  c_palette_alloc (palette, real_length);
}

void 
c_palette_change (CPalette *palette, guint pos, const Color *corba_color, 
                  CColorBase *base, CError **error)
{
  g_assert (palette != NULL);
  g_assert (corba_color != NULL);
  
  if ((pos < 1) || (pos > palette->nb_elem)) {
    *error = c_error_new (C_ERROR_BAD_POSITION, C_ERROR_BUG,
                           _("Bad position in CHANGE : %d"), pos);
    return;
  }
  
  c_color_free (palette->data[pos - 1]);
  
  palette->data[pos - 1] = c_color_new (corba_color->red, 
                                        corba_color->green,
                                        corba_color->blue,
                                        corba_color->name,
                                        corba_color->comment);
                                  
  if (base) {                                        
    c_color_base_emit_change (base, pos);
  }
}                  

CPalette *
c_palette_load (FILE *fp, const char *path, CError **error)
{
  char buf[1000], name[1000], *comment;
  int red, green, blue;
  CPalette *palette;
  int result;
  gboolean have_header = FALSE;
  
  g_return_val_if_fail (fp != NULL, NULL);
  g_return_val_if_fail (path != NULL, NULL);
  
  palette = c_palette_new ();
  
  while (! feof (fp)) {
    fgets (buf, 999, fp);
    
    if (feof (fp)) break;
    
    g_strchug (buf);
    
    if ((buf[0] == '!') || (buf[0] == '#')) {
      
      if (! have_header) {
        have_header = TRUE;
        palette->header = g_strdup (buf);
      }
      
      continue;
    
    }
      
    name[0] = 0;      
      
    result = sscanf (buf, "%d %d %d\t\t%999[^\n]\n", &red, &green, &blue, name);
    if (result < 3) {
    
      if (have_header) {
        c_palette_free (palette);

        *error = c_error_new (C_ERROR_BAD_FILE, C_ERROR_USER,
                              _("File '%s' is not a valid palette file or is corrupted"), path);
        return NULL;

      }
      
      if (result <= 0) {
        palette->header = g_strdup (buf);
        have_header = TRUE;                  
      }

    } else {

      have_header = TRUE;
      
      /* search for comment */
      
      if ((comment = strchr (name, '#'))) {
        comment[0] = 0;
        comment++;
      } 
      
      c_palette_append (palette, red, green, blue, g_strstrip (name), comment ? g_strstrip (comment) : "");
      
    }
  }
  
  return palette;
}

void
c_palette_save (FILE *fp, CPalette *palette, const char *path, CError **error)
{
  CColor *color;
  char *buf;
  int i;
  
  g_assert (fp != NULL);
  g_assert (palette != NULL);
  g_assert (path != NULL);
      
  if (palette->header) {
  
    if (fputs (palette->header, fp) == EOF) {
      *error = c_error_new (C_ERROR_CANNOT_SAVE_FILE, C_ERROR_USER,
                            _("Error when writting '%s'"), path);
                            
      return;
    }                            
    
  }
  
  for (i=0; i < palette->nb_elem; i++) {
  
    color = palette->data[i];
  
    if (! strcmp (color->comment, "")) {
      buf = g_strdup_printf ("%d %d %d\t\t%s\n", color->red, color->green, 
                             color->blue, color->name);
    } else {
      buf = g_strdup_printf ("%d %d %d\t\t%s # %s\n", color->red, color->green,
                             color->blue, color->name, color->comment);
    }                             
                           
    if (fputs (buf, fp) == EOF) {
      *error = c_error_new (C_ERROR_CANNOT_SAVE_FILE, C_ERROR_USER,
                            _("Error when writting '%s'"), path);
                            
      g_free (buf);
      
      return;                                
    }
    
    g_free (buf);                           
  
  }
}

