/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_PREVIEW_H
#define C_PREVIEW_H

#include <gtk/gtkframe.h>
#include <gtk/gtkwidget.h>

#define C_PREVIEW_TYPE (c_preview_get_type ())
#define C_PREVIEW(o)  (GTK_CHECK_CAST ((o), C_PREVIEW_TYPE, CPreview))

typedef struct _CPreview       CPreview;
typedef struct _CPreviewClass  CPreviewClass;

struct _CPreview {
  GtkFrame frame;
  
  GtkWidget *preview;
  
  gushort red;
  gushort green;
  gushort blue;
};

struct _CPreviewClass {
  GtkFrameClass frame;
};

GtkType     c_preview_get_type         (void);
CPreview   *c_preview_new              (void);
void        c_preview_construct        (CPreview *preview);

void        c_preview_set_rgb          (CPreview *preview, gushort red,
                                        gushort green, gushort blue);

#endif
