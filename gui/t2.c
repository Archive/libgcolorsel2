/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gnome.h>

#include "c-shell.h"
#include "c-views.h"

static void
remove_cb (GtkButton *button, CViews *views)
{
  c_views_cmd_remove (views);
}

static void
insert_cb (GtkButton *button, CViews *views)
{
  c_views_cmd_insert (views, TRUE, NULL);
}

int main (int argc, char *argv[])
{
  GtkWidget *app;
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;

  CShell *shell;
  CViews *views;  
  
  CError *error = NULL;
  
  gcolorsel_gui_init (argc, argv);
  gnome_init ("test", "0.1", argc, argv);
  
  app = gnome_app_new ("test", "test");
  gtk_widget_set_usize (app, 300, 300);
  gtk_signal_connect (GTK_OBJECT (app), "delete_event",
                      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
                      
  shell = c_shell_new ();
  
  views = c_views_new (shell, C_SELECT_MULTIPLE, C_VIEW_LIST);                       
                      
  vbox = gtk_vbox_new (FALSE, 0);
  gnome_app_set_contents (GNOME_APP (app), vbox);
  
  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
  
  button = gtk_button_new_with_label ("REMOVE");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked", 
                      GTK_SIGNAL_FUNC (remove_cb), views);
  
  button = gtk_button_new_with_label ("INSERT");
  gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
  gtk_signal_connect (GTK_OBJECT (button), "clicked",
                      GTK_SIGNAL_FUNC (insert_cb), views);
                                                 
  gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (views), TRUE, TRUE, 0);

  c_shell_open_user (shell, "toto", &error);
  if (error) {
    c_shell_log_error (shell, error);
  }
  
  gtk_widget_show_all (app);

  gcolorsel_activate ();     
  gtk_main ();

  gtk_object_destroy (GTK_OBJECT (app));
  gtk_object_destroy (GTK_OBJECT (shell));

  return 0;
}

