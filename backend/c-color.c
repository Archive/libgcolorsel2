/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-color.h"

#include <glib.h>

CColor *
c_color_new (gushort red, gushort green, gushort blue,
             const char *name, const char *comment)
{
  CColor *color;
  
  g_assert (red   <= 255);
  g_assert (green <= 255);  
  g_assert (blue  <= 255);
  
  color = g_new0 (CColor, 1);
  
  color->red   = red;
  color->green = green;
  color->blue  = blue;
  
  if (name)
    color->name = g_strdup (name);
    
  if (comment)
    color->comment = g_strdup (comment);
    
  return color;    
}

void	
c_color_free (CColor *color)
{
  g_assert (color != NULL);
  
  if (color->name)
    g_free (color->name);
    
  if (color->comment)
    g_free (color->comment);

  g_free (color);
}

