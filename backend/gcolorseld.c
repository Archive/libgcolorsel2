/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include "c-color.h"
#include "c-palette.h"
#include "c-palette-list.h"
#include "c-connect.h"

#include "GColorsel2.h"

#include <glib.h>
#include <liboaf/liboaf.h>
#include <libgnome/gnome-defs.h>
#include <libgnome/gnome-i18n.h>

static ColorServer server = CORBA_OBJECT_NIL;

static gboolean
gcolorsel_set_exception (CError *error, CORBA_Environment *ev);

static CORBA_unsigned_long
gcolorsel_connect (PortableServer_Servant servant,
                   ColorClient client, const char *path, CORBA_boolean try_create,
                   CORBA_boolean truncate, CORBA_boolean *read_only,
                   CORBA_Environment *ev);

static void
gcolorsel_disconnect (PortableServer_Servant servant,
                      CORBA_unsigned_long cnum,
                      CORBA_Environment *ev);
                      
static void
gcolorsel_remove (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum, const ColorPosList *pos_list,
                  CORBA_Environment *ev);                      
                  
static void
gcolorsel_move   (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum, CORBA_unsigned_long to,
                  const ColorPosList *pos_list,
                  CORBA_Environment *ev);      
                  
static void
gcolorsel_insert (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum,
                  CORBA_unsigned_long pos, const ColorList *color_list,
                  CORBA_Environment *ev);
                  
static void
gcolorsel_change (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum,
                  CORBA_unsigned_long pos, const Color *color,
                  CORBA_Environment *ev);
                  
static void
gcolorsel_info (PortableServer_Servant servant,
                CORBA_char **version,
                ColorInfo **colorinfo,
                CORBA_Environment *ev); 
                                 
                                                     
static PortableServer_ServantBase__epv base_epv = {
  NULL,
  NULL,
  NULL
};

static POA_ColorServer__epv server_epv = {
  NULL,
  gcolorsel_connect,
  gcolorsel_disconnect,
  gcolorsel_remove,
  gcolorsel_move,
  gcolorsel_insert,
  gcolorsel_change,
  gcolorsel_info
};

static POA_ColorServer__vepv poa_server_vepv = { &base_epv, &server_epv };
static POA_ColorServer poa_server_servant = { NULL, &poa_server_vepv };

static CORBA_unsigned_long
gcolorsel_connect (PortableServer_Servant servant,
                   ColorClient client, const char *path, CORBA_boolean try_create,
                   CORBA_boolean truncate, CORBA_boolean *read_only,
                   CORBA_Environment *ev)
{
  CConnect  *connect;
  CPalette  *palette;
  CPaletteListData *palette_data;
  ColorList *color_list;
  CColor    *color;
  Color     *corba_color;
  CError    *error = NULL;
  int i;
  
  connect = c_connect_list_connect (client, path, try_create, truncate, &error);
  if (gcolorsel_set_exception (error, ev))
    return 0;
    
  palette_data = c_connect_get_palette_data (connect);
  *read_only = palette_data->read_only;        

  palette = c_connect_get_palette (connect);
  
  color_list           = ColorList__alloc ();
  color_list->_buffer  = CORBA_sequence_Color_allocbuf (palette->nb_elem);
  color_list->_length  = palette->nb_elem;
  color_list->_maximum = palette->nb_elem;
  
  CORBA_sequence_set_release (color_list, TRUE);
  
  for (i = 0; i < palette->nb_elem; i++) {
  
    color = palette->data[i]; 

    corba_color = &(color_list->_buffer[i]);
  
    corba_color->red     = color->red;
    corba_color->green   = color->green;
    corba_color->blue    = color->blue;
    corba_color->name    = CORBA_string_dup (color->name);
    corba_color->comment = CORBA_string_dup (color->comment);
    
  }  

  ColorClient_insert (c_connect_get_client (connect), 1, color_list, ev);
  
  /* FIXME : error ? */
  
  CORBA_free (color_list);  
  
  return connect->cnum;
}                   

static void
gcolorsel_disconnect (PortableServer_Servant servant,
                      CORBA_unsigned_long cnum,
                      CORBA_Environment *ev)
{
  CConnect *connect;
  CError   *error = NULL;
  
  connect = c_connect_list_get (cnum, &error);
  if (gcolorsel_set_exception (error, ev)) 
    return;
  
  c_connect_list_disconnect (connect);
}                      

#define FOREACH_CONNECT(_list_, _func_, _ev_)              \
{                                                          \
  CConnect *_connect_;                                     \
  ColorClient client;                                      \
  GList *_list2_;					   \
  							   \
  _list2_ = _list_;					   \
                                                           \
  while (_list2_) {                                        \
           						   \
           _connect_ = _list2_->data;                      \
           g_assert (_connect_ != NULL);                   \
           client = c_connect_get_client (_connect_);      \
           _list2_ = _list2_->next;		           \
                      					   \
           _func_;                                         \
                                                           \
           if (_ev_->_major != CORBA_NO_EXCEPTION) {       \
             printf ("Le client %d est mort !\n", _connect_->cnum); \
             CORBA_exception_free (ev);                    \
             c_connect_list_disconnect (_connect_);        \
           }                                               \
} }

const char *read_only_msg = N_("You cannot change '%s' : read only !");

#define READ_ONLY_CHECK(_palette_data_, _ev_);             \
{							   \
	CError *_error_;				   \
						           \
	if (_palette_data_->read_only) {                   \
							   \
  	  _error_ = c_error_new (C_ERROR_READ_ONLY, C_ERROR_USER,   \
  	                         dgettext (PACKAGE, read_only_msg), \
                                 palette_data->path);      \
							   \
          gcolorsel_set_exception (_error_, _ev_);         \
        						   \
          return;  					   \
        }                                                  \
}                                                     
                                                                    
static void
gcolorsel_remove (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum, const ColorPosList *pos_list,                                    
                  CORBA_Environment *ev)
{
  CPaletteListData *palette_data;
  CConnect *connect;
  CError *error = NULL;
  
  connect = c_connect_list_get (cnum, &error);
  if (gcolorsel_set_exception (error, ev))
    return;  
  
  palette_data = c_connect_get_palette_data (connect);
    
  c_connect_list_alive (connect);

  READ_ONLY_CHECK (palette_data, ev);

  c_palette_remove ((CPalette *)palette_data, pos_list, NULL, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
    
  c_palette_data_set_modified (palette_data);    
    
  FOREACH_CONNECT (palette_data->connect, 
                   ColorClient_remove (client, pos_list, ev), ev);
}                     

static void
gcolorsel_move (PortableServer_Servant servant,
                CORBA_unsigned_long cnum, CORBA_unsigned_long to,
                const ColorPosList *pos_list,
                CORBA_Environment *ev)
{
  CPaletteListData *palette_data;
  CConnect *connect;
  CError *error = NULL;
  
  connect = c_connect_list_get (cnum, &error);
  if (gcolorsel_set_exception (error, ev))
    return;  
  
  palette_data = c_connect_get_palette_data (connect);
    
  c_connect_list_alive (connect);

  READ_ONLY_CHECK (palette_data, ev);

  c_palette_move ((CPalette *)palette_data, to, pos_list, NULL, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
    
  c_palette_data_set_modified (palette_data);    
    
  FOREACH_CONNECT (palette_data->connect, 
                   ColorClient_move (client, to, pos_list, ev), ev);
}                  

static void
gcolorsel_insert (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum,
                  CORBA_unsigned_long pos, const ColorList *color_list,
                  CORBA_Environment *ev)
{
  CPaletteListData *palette_data;
  CConnect *connect;
  CError *error = NULL;
  
  connect = c_connect_list_get (cnum, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
  
  palette_data = c_connect_get_palette_data (connect);
    
  c_connect_list_alive (connect);
    
  READ_ONLY_CHECK (palette_data, ev);    
    
  c_palette_insert ((CPalette *)palette_data, pos, color_list, NULL, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
    
  c_palette_data_set_modified (palette_data);        
  
  FOREACH_CONNECT (palette_data->connect, 
                   ColorClient_insert (client, pos, color_list, ev), ev);
}                 

static void
gcolorsel_change (PortableServer_Servant servant,
                  CORBA_unsigned_long cnum,
                  CORBA_unsigned_long pos, const Color *color,
                  CORBA_Environment *ev)
{
  CPaletteListData *palette_data;
  CConnect *connect;
  CError *error = NULL;
  
  connect = c_connect_list_get (cnum, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
  
  palette_data = c_connect_get_palette_data (connect);
    
  c_connect_list_alive (connect);    
    
  READ_ONLY_CHECK (palette_data, ev);    
    
  c_palette_change ((CPalette *)palette_data, pos, color, NULL, &error);
  if (gcolorsel_set_exception (error, ev))
    return;
    
  c_palette_data_set_modified (palette_data);        
  
  FOREACH_CONNECT (palette_data->connect, 
                   ColorClient_change (client, pos, color, ev), ev);
}                                    

static void
gcolorsel_info (PortableServer_Servant servant,
                CORBA_char **version,
                ColorInfo **info,
                CORBA_Environment *ev)
{
  CPaletteListData *palette_data;
  CPalette *palette;
  GList *list;
  int n;
  
  *version = CORBA_string_dup (VERSION);
  
  n = g_list_length (list = palette_list);
    
  *info = ColorInfo__alloc ();
  (*info)->_buffer = CORBA_sequence_Info_allocbuf (n);
  (*info)->_length = n;
  (*info)->_maximum = n;
  
  n = 0;
  
  while (list) {
  
    palette_data = list->data;
    palette = list->data;
  
    (*info)->_buffer[n].name = CORBA_string_dup (palette_data->path);
    (*info)->_buffer[n].nb_connects = g_list_length (palette_data->connect);
    (*info)->_buffer[n].nb_colors = palette->nb_elem;
    (*info)->_buffer[n].read_only = palette_data->read_only;
  
    list = list->next; n++;
  }

}                

static gboolean
gcolorsel_set_exception (CError *error, CORBA_Environment *ev)
{
  ColorException *except;

  if (! error) return FALSE;
    
  g_assert (error->message != NULL);
  
  except = ColorException__alloc ();
  except->message = CORBA_string_dup (error->message);
  
  switch (error->type) {
    case C_ERROR_BAD_CONNECT_NUM:
      except->type = ErrorBadConnectNum; break;
    case C_ERROR_BAD_POSITION:
      except->type = ErrorBadPosition; break;
    case C_ERROR_BAD_RGB:
      except->type = ErrorBadRGB; break;
    case C_ERROR_CANNOT_OPEN_FILE:
      except->type = ErrorCannotOpenFile; break;
    case C_ERROR_CANNOT_SAVE_FILE:
      except->type = ErrorCannotSaveFile; break;
    case C_ERROR_BAD_FILE:
      except->type = ErrorBadFile; break;
    case C_ERROR_READ_ONLY:
      except->type = ErrorReadOnly; break;

    default:
      except->type = ErrorUnknown;
  }  
  
  switch (error->severity) {
    case C_ERROR_USER:
      except->severity = ErrorUser; break;
    default:
      except->severity = ErrorBug; break;
  }
  
  CORBA_exception_set (ev, CORBA_USER_EXCEPTION, 
                       ex_ColorException, except);
                       
  c_error_free (error);

  return TRUE;
}

int main (int argc, char *argv[])
{
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  CORBA_Environment ev;
  CORBA_ORB orb;
  
  OAF_RegistrationResult result;
  
  GMainLoop *loop;
  
  bindtextdomain (PACKAGE, GNOMELOCALEDIR);
  textdomain (PACKAGE);
  
  CORBA_exception_init (&ev);
  
  if (!oaf_init (argc, argv)) 
    g_error (_("Failed to init Object Activation Framework"));
    
  orb = oaf_orb_get ();
  
  POA_ColorServer__init (&poa_server_servant, &ev);
  
  poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references (orb, "RootPOA", &ev);    
  PortableServer_POAManager_activate (PortableServer_POA__get_the_POAManager (poa, &ev), &ev);
  
  objid = PortableServer_POA_activate_object (poa, &poa_server_servant, &ev);
  CORBA_free (objid);
  
  server = PortableServer_POA_servant_to_reference (poa, &poa_server_servant, &ev);
  
  if (CORBA_Object_is_nil (server, &ev)) 
    g_error (_("Failed to get object reference for ColorServer"));
    
  result = oaf_active_server_register (IID, server);
  
  if (result != OAF_REG_SUCCESS) 
    switch (result) {
      case OAF_REG_NOT_LISTED:
        g_error (_("OAF doesn't know about our IID; indicates broken installation; can't register; exiting\n"));
          
      case OAF_REG_ALREADY_ACTIVE:
        g_error (_("Another server (gcolorsel2d) already registered with OAF; exiting\n"));

      default:
	g_error (_("Unknown error registering server (gcolorsel2d) with OAF; exiting\n"));
     }
     
  loop = g_main_new (TRUE);
  g_main_run (loop);     
  
  return 0;
}

