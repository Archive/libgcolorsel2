/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-color-edit.h"

#include "c-preview.h"
#include <gcolorsel2.h>

#include <gnome.h>
#include <gdk/gdkx.h>

#define PARENT_TYPE GNOME_TYPE_APP
static GnomeAppClass *parent_class = NULL;

#define KEY_POS "pos"

static void base_thaw         (CColorEdit *color_edit);
static void base_remove_color (CColorEdit *color_edit, guint pos, 
			       CColorBase *color_base);
static void base_move_color   (CColorEdit *color_edit, guint pos, 
			       int offset, CColorBase *color_base);
static void base_change_color (CColorEdit *color_edit, guint pos,
                               CColorBase *color_base);			       
                               
enum {
  DO_ERROR,
  LAST_SIGNAL
};

static guint c_color_edit_signals[LAST_SIGNAL] = { 0 };                               

/* from gtkcolorsel.c (gtk 2.0) */

/* The cursor for the dropper */
#define DROPPER_WIDTH 17
#define DROPPER_HEIGHT 17
#define DROPPER_X_HOT 2  
#define DROPPER_Y_HOT 16 

static char dropper_bits[] = {
  0xff, 0x8f, 0x01, 0xff, 0x77, 0x01, 0xff, 0xfb, 0x00, 0xff, 0xf8, 0x00,
  0x7f, 0xff, 0x00, 0xff, 0x7e, 0x01, 0xff, 0x9d, 0x01, 0xff, 0xd8, 0x01,
  0x7f, 0xd4, 0x01, 0x3f, 0xee, 0x01, 0x1f, 0xff, 0x01, 0x8f, 0xff, 0x01,
  0xc7, 0xff, 0x01, 0xe3, 0xff, 0x01, 0xf3, 0xff, 0x01, 0xfd, 0xff, 0x01,
  0xff, 0xff, 0x01, };

static char dropper_mask[] = {
  0x00, 0x70, 0x00, 0x00, 0xf8, 0x00, 0x00, 0xfc, 0x01, 0x00, 0xff, 0x01,
  0x80, 0xff, 0x01, 0x00, 0xff, 0x00, 0x00, 0x7f, 0x00, 0x80, 0x3f, 0x00,
  0xc0, 0x3f, 0x00, 0xe0, 0x13, 0x00, 0xf0, 0x01, 0x00, 0xf8, 0x00, 0x00,
  0x7c, 0x00, 0x00, 0x3e, 0x00, 0x00, 0x1e, 0x00, 0x00, 0x0d, 0x00, 0x00,
  0x02, 0x00, 0x00, };

static void
c_color_edit_log_error (CColorEdit *color_edit, CError *error)
{
  gtk_signal_emit (GTK_OBJECT (color_edit), c_color_edit_signals[DO_ERROR], error);
}
                               
static void
entry_set_text (GtkEntry *entry, const char *text, gpointer data)
{
  g_assert (entry != NULL);
  
  gtk_signal_handler_block_by_data (GTK_OBJECT (entry), data);

  if (text)   
    gtk_entry_set_text (entry, text);
  else  
    gtk_entry_set_text (entry, "");
    
  gtk_signal_handler_unblock_by_data (GTK_OBJECT (entry), data);    
}

static void
spin_button_set_value (GtkSpinButton *spin, int value, gpointer data)
{
  GtkAdjustment *adj;
  
  g_assert (spin != NULL);
    
  adj = gtk_spin_button_get_adjustment (spin);

  gtk_signal_handler_block_by_data (GTK_OBJECT (adj), data);  
  gtk_spin_button_set_value (spin, value);
  gtk_signal_handler_unblock_by_data (GTK_OBJECT (adj), data);
}                               

static GtkWidget *
combo_search (CColorEdit *color_edit, guint pos)
{
  GList *list = GTK_LIST (color_edit->combo->list)->children;
  
  while (list) {
  
    if (GPOINTER_TO_INT (gtk_object_get_data (list->data, KEY_POS)) == pos)
      return list->data;
      
    list = list->next;
  }
  
  return NULL;
}

static void
combo_append (CColorEdit *color_edit, guint pos, const char *name)
{
  GtkWidget *item;
  GtkList *list;
  char *buf;
  
  list = GTK_LIST (GTK_COMBO (color_edit->combo)->list);
  
  buf = g_strdup_printf ("%s (%d)", name, pos);  
  
  item = gtk_list_item_new_with_label (buf);  
  gtk_object_set_data (GTK_OBJECT (item), KEY_POS, GINT_TO_POINTER (pos));
  
  gtk_container_add (GTK_CONTAINER (list), item);
  gtk_widget_show (item);        
  
  g_free (buf);
}

static void
combo_change_one (CColorEdit *color_edit, GtkWidget *item, const char *name, guint pos)
{
  char *buf;
  
  buf = g_strdup_printf ("%s (%d)", name, pos);

  gtk_label_set_text (GTK_LABEL (GTK_BIN (item)->child), buf);
  
  if (color_edit->pos == pos) {
  
    entry_set_text (GTK_ENTRY (color_edit->combo->entry), buf, color_edit->combo);
  
  }
  
  g_free (buf);
}

static void
c_color_edit_set (CColorEdit *color_edit, guint pos, gushort red, gushort green, gushort blue,
                  const char *name, const char *comment)
{
  g_assert (color_edit != NULL);

  if (name)  
    entry_set_text (color_edit->name, name, color_edit);

  if (comment)    
    entry_set_text (color_edit->comment, comment, color_edit);
  
  spin_button_set_value (color_edit->red, red, color_edit);
  spin_button_set_value (color_edit->green, green, color_edit);
  spin_button_set_value (color_edit->blue, blue, color_edit);
  
  c_preview_set_rgb (color_edit->preview, red, green, blue);
}

static void
c_color_edit_change (CColorEdit *color_edit, gushort red, gushort green, gushort blue,
                     const char *name, const char *comment)
{
  CError *error = NULL;
  
  g_assert (color_edit != NULL);
  
  if (! name)
    name = gtk_entry_get_text (color_edit->name);
    
  if (! comment)
    comment = gtk_entry_get_text (color_edit->comment);
    
  c_color_base_change (color_edit->color_base, color_edit->pos,
  		       red, green, blue,
  		       name, comment,
  		       &error);

  if (error) 
    c_color_edit_log_error (color_edit, error);
}  
                        
static void
set_sensitive (CColorEdit *color_edit)
{
  gboolean sensi;
  
  sensi = GTK_LIST (color_edit->combo->list)->selection != NULL;
  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->table), sensi);
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->combo), sensi);    
}

static void
button_set_sensitive (CColorEdit *color_edit)
{
  GList *list;
  GtkList *gtk_list;
  gboolean prev = FALSE, next = FALSE;
  
  gtk_list = GTK_LIST (color_edit->combo->list);
  
  if (gtk_list->selection) {
  
    list = g_list_find (gtk_list->children, gtk_list->selection->data);
  
    if (list) {
      next = list->next != NULL;
      prev = list->prev != NULL;
    } 
  }
      
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->prev), prev);
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->next), next);  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->grab), 
                            (gtk_list->children != NULL) && (! color_edit->color_base->read_only));
}

static void
entry_set_sensitive (CColorEdit *color_edit)
{
  gboolean sensi;
  
  sensi = ! color_edit->color_base->read_only;
  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->red), sensi);
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->green), sensi);  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->blue), sensi);  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->name), sensi);  
  gtk_widget_set_sensitive (GTK_WIDGET (color_edit->comment), sensi);  
}
			       
static void
combo_changed (GtkList *list, CColorEdit *color_edit)
{
  guint pos;
  gushort red, green, blue;
  char *name, *comment;
  CError *error = NULL;
  GtkWidget *widget;
  
  if (list->selection) {
  
    widget = list->selection->data;
   
    pos = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (widget), KEY_POS)); 
    
    c_color_base_get (color_edit->color_base, pos, &red, &green, &blue, 
                      &name, &comment, &error);  
                      
    if (error) {
      c_color_edit_log_error (color_edit, error);
      set_sensitive (color_edit);
      button_set_sensitive (color_edit);
      color_edit->pos = 0;
      return;
    }           
    
    set_sensitive (color_edit);
    button_set_sensitive (color_edit);
        
    c_color_edit_set (color_edit, pos, red, green, blue, name, comment);
    
    color_edit->pos = pos;
    
  } else {
    color_edit->pos = 0;
    set_sensitive (color_edit);
    button_set_sensitive (color_edit);
  }
}			       

static void
next_clicked (GtkWidget *widget, CColorEdit *color_edit)
{
  GtkWidget *child;
  int pos;
  
  if (color_edit->pos) {

    child = combo_search (color_edit, color_edit->pos);
    g_assert (child != NULL);
    
    pos = gtk_list_child_position (GTK_LIST (color_edit->combo->list), child);             
    gtk_list_select_item (GTK_LIST (color_edit->combo->list), pos + 1);
  
  }
  
}

static void
prev_clicked (GtkWidget *widget, CColorEdit *color_edit)
{
  GtkWidget *child;
  int pos;
  
  if (color_edit->pos) {

    child = combo_search (color_edit, color_edit->pos);
    g_assert (child != NULL);
    
    pos = gtk_list_child_position (GTK_LIST (color_edit->combo->list), child);             
    gtk_list_select_item (GTK_LIST (color_edit->combo->list), pos - 1);
  
  }

}

static void
grab_color (gint x_root, gint y_root, double *r, double *g, double *b)
{
  GdkImage *image;
  guint32 pixel;
  GdkVisual *visual;
  GdkColormap *colormap = gdk_colormap_get_system ();
  XColor xcolor;
  
  image = gdk_image_get (GDK_ROOT_PARENT (), x_root, y_root, 1, 1);
  pixel = gdk_image_get_pixel (image, 0, 0);
  visual = gdk_colormap_get_visual (colormap);
  
  switch (visual->type) {
    case GDK_VISUAL_DIRECT_COLOR:
    case GDK_VISUAL_TRUE_COLOR:
      *r = (double)((pixel & visual->red_mask) >> visual->red_shift) /
                   ((1 << visual->red_prec) - 1);
      *g = (double)((pixel & visual->green_mask) >> visual->green_shift) /
                   ((1 << visual->green_prec) - 1);
      *b = (double)((pixel & visual->blue_mask) >> visual->blue_shift) /
                   ((1 << visual->blue_prec) - 1);
      break;
    case GDK_VISUAL_STATIC_GRAY:
    case GDK_VISUAL_GRAYSCALE:
      *r = (double)pixel / ((1 << visual->depth) - 1);
      *g = (double)pixel / ((1 << visual->depth) - 1);
      *b = (double)pixel / ((1 << visual->depth) - 1);
      break;
    case GDK_VISUAL_STATIC_COLOR:
      xcolor.pixel = pixel;
      XQueryColor (GDK_DISPLAY (), GDK_COLORMAP_XCOLORMAP (colormap), &xcolor);
      *r = xcolor.red / 65535.0;
      *g = xcolor.green / 65535.0;
      *b = xcolor.blue / 65535.0;
      break;
    case GDK_VISUAL_PSEUDO_COLOR:
      *r = colormap->colors[pixel].red / (double) 0xffffff;
      *g = colormap->colors[pixel].green / (double) 0xffffff;
      *b = colormap->colors[pixel].blue / (double) 0xffffff;
      break;
    default:
      g_assert_not_reached ();
      break;
    }                       
}

static void
grab_motion (GtkWidget *widget, GdkEventMotion *event, CColorEdit *color_edit)
{
  double r, g, b;
  
  grab_color (event->x_root, event->y_root, &r, &g, &b);
  c_color_edit_set (color_edit, 0, r * 255, g * 255, b * 255, NULL, NULL);
}

static void
grab_release (GtkWidget *widget, GdkEventButton *event, CColorEdit *color_edit)
{
  double r, g, b;
  
  if (event->button == 1) {
  
    grab_color (event->x_root, event->y_root, &r, &g, &b);
    c_color_edit_change (color_edit, r * 255, g * 255, b * 255, NULL, NULL);

  } else {
  
    c_color_edit_set (color_edit, 0, color_edit->save_red, color_edit->save_green, color_edit->save_blue, NULL, NULL);
  
  }
  
  gtk_signal_disconnect_by_func (GTK_OBJECT (widget), grab_release, color_edit);
  gtk_signal_disconnect_by_func (GTK_OBJECT (widget), grab_motion, color_edit);
  gdk_pointer_ungrab (0);
}

static void
grab_clicked (GtkWidget *widget, CColorEdit *color_edit)
{
  GdkColor fg, bg;
  GdkPixmap *pixmap, *mask;
  GdkCursor *cursor = NULL;
  
  if (color_edit->graping) {
    color_edit->graping = FALSE;
    return;
  }
  
  color_edit->graping = TRUE;
  color_edit->save_red = gtk_spin_button_get_value_as_int (color_edit->red);
  color_edit->save_green = gtk_spin_button_get_value_as_int (color_edit->green);  
  color_edit->save_blue = gtk_spin_button_get_value_as_int (color_edit->blue);  
    
  pixmap = gdk_bitmap_create_from_data (NULL,
                                        dropper_bits,
                                        DROPPER_WIDTH, DROPPER_HEIGHT);
                                 
  mask = gdk_bitmap_create_from_data (NULL,
                                      dropper_mask,
                                      DROPPER_WIDTH, DROPPER_HEIGHT);
                                      
  gdk_color_white (gdk_colormap_get_system (), &bg);
  gdk_color_black (gdk_colormap_get_system (), &fg);
  
  cursor = gdk_cursor_new_from_pixmap (pixmap, mask, &fg, &bg, DROPPER_X_HOT, DROPPER_Y_HOT);                                                   

  gdk_pixmap_unref (pixmap);
  gdk_pixmap_unref (mask);
  
  gtk_signal_connect (GTK_OBJECT (widget), "button_release_event", 
                      GTK_SIGNAL_FUNC (grab_release), color_edit);
  gtk_signal_connect (GTK_OBJECT (widget), "motion_notify_event",
                      GTK_SIGNAL_FUNC (grab_motion), color_edit);                      
  
  gdk_pointer_grab (widget->window,
                    FALSE,
                    GDK_BUTTON_RELEASE_MASK | GDK_BUTTON_PRESS_MASK | GDK_POINTER_MOTION_MASK,
                    NULL,
                    cursor,
                    0);  
}

static void
changed (GtkWidget *widget, CColorEdit *color_edit)
{
  if (color_edit->pos) 

    c_color_edit_change (color_edit, 
                         gtk_spin_button_get_value_as_int (color_edit->red),
                         gtk_spin_button_get_value_as_int (color_edit->green),                         
                         gtk_spin_button_get_value_as_int (color_edit->blue),                         
                         NULL, NULL);
}

static void
destroy (GtkObject *object)
{
  CColorEdit *color_edit;
  
  color_edit = C_COLOR_EDIT (object);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}			       

static void
class_init (CColorEditClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass *) class;
  widget_class = (GtkWidgetClass *) class;
  
  parent_class = gtk_type_class (GNOME_TYPE_APP);

  c_color_edit_signals[DO_ERROR] =
    gtk_signal_new ("do_error",
                    GTK_RUN_LAST,
                    object_class->type,
                    GTK_SIGNAL_OFFSET (CColorEditClass, do_error),
                    gtk_marshal_NONE__POINTER,
                    GTK_TYPE_NONE, 1, GTK_TYPE_POINTER);

  gtk_object_class_add_signals (object_class, c_color_edit_signals, LAST_SIGNAL);
                                                                                                             
  object_class->destroy = destroy;
}

static void
init (CColorEdit *color_edit)
{
  color_edit->color_base = NULL;
  color_edit->to_edit = NULL;
  color_edit->pass_thaw = 0;
  color_edit->pos = 0;
}

static GtkLabel *
label_new (GtkTable *table, const char *name, int pos)
{
  GtkWidget *label;
  
  label = gtk_label_new (name);
  gtk_table_attach (GTK_TABLE (table), label, 0, 1, pos, pos + 1,
                    GTK_FILL, GTK_FILL, 0, 0);
  gtk_widget_show (label);
  
  gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
  
  return GTK_LABEL (label);
}

static GtkSpinButton *
spin_button_new (GtkTable *table, int pos, GtkSignalFunc func, gpointer data)
{
  GtkAdjustment *adj;
  GtkSpinButton *spin;
  
  adj = (GtkAdjustment *)gtk_adjustment_new (1.0, 0.0, 255.0, 1.0, 5.0, 0.0);
  
  spin = GTK_SPIN_BUTTON (gtk_spin_button_new (adj, 0, 0));
  
  gtk_table_attach (GTK_TABLE (table), GTK_WIDGET (spin), 1, 2, pos, pos + 1,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);

  gtk_widget_show (GTK_WIDGET (spin));
  
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed", func, data);
  
  return spin;
}

static GtkEntry *
entry_new (GtkSignalFunc func, gpointer data)
{
  GtkEntry *entry;
  
  entry = GTK_ENTRY (gtk_entry_new ());
  
  gtk_signal_connect (GTK_OBJECT (entry), "changed", func, data);
  
  return entry;
}

void
c_color_edit_construct (CColorEdit *color_edit, CColorBase *color_base, 
                        int pass_thaw, GList *to_edit)
{ 
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *vbox2;
  GtkWidget *pixmap;
  
  g_assert (color_edit != NULL);
  g_assert (color_base != NULL);
  g_assert (pass_thaw >= 0);
  g_assert (to_edit != NULL);

  gnome_app_construct (GNOME_APP (color_edit), "gcolorsel", _("Edit"));
  gtk_container_set_border_width (GTK_CONTAINER (color_edit), GNOME_PAD);
  
  gtk_window_set_default_size (GTK_WINDOW (color_edit), 270, 220);
  
  color_edit->color_base = color_base;
  color_edit->to_edit = to_edit;
  color_edit->pass_thaw = pass_thaw;
  
  /* VBox */
  
  vbox = gtk_vbox_new (FALSE, 0);
  gnome_app_set_contents (GNOME_APP (color_edit), vbox);
  gtk_widget_show (vbox);
  
  /* Combo */
  
  color_edit->combo = GTK_COMBO (gtk_combo_new ());
  gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (color_edit->combo), FALSE, FALSE, 0);
  gtk_widget_show (GTK_WIDGET (color_edit->combo));
  gtk_entry_set_editable (GTK_ENTRY (color_edit->combo->entry), FALSE);
  
  gtk_signal_connect (GTK_OBJECT (color_edit->combo->list), "selection_changed",
                      GTK_SIGNAL_FUNC (combo_changed), color_edit);
  
  /* Table */
  
  color_edit->table = GTK_TABLE (gtk_table_new (5, 2, FALSE));
  gtk_table_set_row_spacings (color_edit->table, 6);
  gtk_table_set_col_spacings (color_edit->table, 5);
  gtk_box_pack_start (GTK_BOX (vbox), GTK_WIDGET (color_edit->table), FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_widget_show (GTK_WIDGET (color_edit->table));
  
  /* Labels */
  
  label_new (color_edit->table, _("Red :"), 0);
  label_new (color_edit->table, _("Green :"), 1);  
  label_new (color_edit->table, _("Blue :"), 2);
  label_new (color_edit->table, _("Name :"), 3);
  label_new (color_edit->table, _("Comment :"), 4);  
        
  /* SpinButtons */
    
  color_edit->red = spin_button_new (color_edit->table, 0, changed, color_edit);
  color_edit->green = spin_button_new (color_edit->table, 1, changed, color_edit);
  color_edit->blue = spin_button_new (color_edit->table, 2, changed, color_edit);
  
  /* Entry Name */
  
  color_edit->name = entry_new (changed, color_edit);
  gtk_table_attach (color_edit->table, GTK_WIDGET (color_edit->name), 1, 2, 3, 4,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_show (GTK_WIDGET (color_edit->name));  
  
  /* Entry Comment */
  
  color_edit->comment = entry_new (changed, color_edit);
  gtk_table_attach (color_edit->table, GTK_WIDGET (color_edit->comment), 1, 2, 4, 5,
                    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 0, 0);
  gtk_widget_show (GTK_WIDGET (color_edit->comment));    
  
  /* HBox */
  
  hbox = gtk_hbox_new (FALSE, GNOME_PAD);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show (hbox);
  
  /* Preview */
  
  color_edit->preview = c_preview_new ();
  gtk_box_pack_start (GTK_BOX (hbox), GTK_WIDGET (color_edit->preview), TRUE, TRUE, 0);
  gtk_widget_show (GTK_WIDGET (color_edit->preview));
  
  /* VBox 2 */
  
  vbox2 = gtk_vbox_new (TRUE, GNOME_PAD_SMALL);
  gtk_box_pack_start (GTK_BOX (hbox), vbox2, FALSE, FALSE, 0);
  gtk_widget_show (vbox2);
  
  /* Previous */
  
  color_edit->prev = gnome_stock_button (GNOME_STOCK_BUTTON_PREV);
  gtk_box_pack_start (GTK_BOX (vbox2), color_edit->prev, FALSE, FALSE, 0);
  gtk_widget_show (color_edit->prev);
  gtk_signal_connect (GTK_OBJECT (color_edit->prev), "clicked",
                      GTK_SIGNAL_FUNC (prev_clicked), color_edit);
  
  /* Next */
  
  color_edit->next = gnome_stock_button (GNOME_STOCK_BUTTON_NEXT);
  gtk_box_pack_start (GTK_BOX (vbox2), color_edit->next, FALSE, FALSE, 0);
  gtk_widget_show (color_edit->next);
  gtk_signal_connect (GTK_OBJECT (color_edit->next), "clicked",
                      GTK_SIGNAL_FUNC (next_clicked), color_edit);
                      
  /* Grab */
  
  pixmap = gnome_stock_pixmap_widget (NULL, GNOME_STOCK_PIXMAP_JUMP_TO);
  color_edit->grab = gnome_pixmap_button (pixmap, _("Grab"));
  gtk_box_pack_start (GTK_BOX (vbox2), color_edit->grab, FALSE, FALSE, 0);
  gtk_widget_show (color_edit->grab);
  gtk_signal_connect (GTK_OBJECT (color_edit->grab), "clicked",
                      GTK_SIGNAL_FUNC (grab_clicked), color_edit);                      
  
  /* Close */
  
  color_edit->close = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
  gtk_box_pack_start (GTK_BOX (vbox2), color_edit->close, FALSE, FALSE, 0);
  gtk_widget_show (color_edit->close);
  gtk_signal_connect_object (GTK_OBJECT (color_edit->close), "clicked", 
                             gtk_widget_destroy, GTK_OBJECT (color_edit));  
      
  c_color_base_connect (GTK_OBJECT (color_edit), color_base,
                       NULL, base_thaw,
                       NULL, base_remove_color,
                       base_move_color, base_change_color);                                   
                  
  entry_set_sensitive (color_edit);                  
}

CColorEdit *
c_color_edit_new (CColorBase *color_base, int pass_thaw, GList *to_edit)
{
  CColorEdit *edit;

  g_assert (color_base != NULL);
  g_assert (to_edit != NULL);
  g_assert (pass_thaw >= 0);
    
  edit = C_COLOR_EDIT (gtk_type_new (c_color_edit_get_type ()));
  
  c_color_edit_construct (edit, color_base, pass_thaw, to_edit);
  
  return edit;
}

static void
base_remove_color (CColorEdit *color_edit, guint pos, CColorBase *color_base)
{
  GtkWidget *child;
  GList list;
  int next_select;
  
  if (color_edit->pass_thaw) return;  
  
  child = combo_search (color_edit, pos);
  
  /* FIXME : if color_edit->grabing ? */
    
  if (child) {
  
    list.data = child;
    list.next = list.prev = NULL;
    
    next_select = gtk_list_child_position (GTK_LIST (color_edit->combo->list), child);
    if (! next_select)
      gtk_list_select_item (GTK_LIST (color_edit->combo->list), 1);
     
    gtk_list_remove_items (GTK_LIST (color_edit->combo->list), &list);        
        
  }
}

static void
base_move_color (CColorEdit *color_edit, guint pos, 
		 int offset, CColorBase *color_base)
{
  GtkWidget *child;
  char *name;
  CError *error = NULL;
  
  if (color_edit->pass_thaw) return;

  child = combo_search (color_edit, pos);

  if (child) {
  
    if (color_edit->pos == pos)
      color_edit->pos += offset;
    
    gtk_object_set_data (GTK_OBJECT (child), KEY_POS, 
                         GINT_TO_POINTER (pos + offset));
  
    c_color_base_get (color_edit->color_base, pos, NULL, NULL, NULL,
                      &name, NULL, &error);  

    if (error) {
      c_color_edit_log_error (color_edit, error);
      return;
    }        

    combo_change_one (color_edit, child, name, pos + offset);
  }
}

static void
base_thaw (CColorEdit *color_edit)
{
  gushort red, green, blue;
  char *name;
  char *comment;
  CError *error = NULL;
  GList *list;
  guint pos;
  
  if (color_edit->pass_thaw) {
    color_edit->pass_thaw--;
    return;
  }
  
  list = color_edit->to_edit;
    
  while (list) {
    
    pos = GPOINTER_TO_INT (list->data);
    
    c_color_base_get (color_edit->color_base, pos, &red, &green, &blue, 
                      &name, &comment, &error);  
              		
    if (error) {

      c_color_edit_log_error (color_edit, error);
      error = NULL;
        
    } else {

      combo_append (color_edit, pos, name);    
      
    }    
    
    list = list->next;
  }    
  
  g_list_free (color_edit->to_edit);
  color_edit->to_edit = NULL;

  set_sensitive (color_edit);
  button_set_sensitive (color_edit);  
}

static void base_change_color (CColorEdit *color_edit, guint pos,
                               CColorBase *color_base)
{
  GtkWidget *item;
  gushort red, green, blue;
  char *name;
  char *comment;
  CError *error = NULL;  
  
  if (color_edit->pass_thaw) return;
  
  item = combo_search (color_edit, pos);
  if (! item) return;
  
  /* FIXME : if color_base->grabing ? */
  
  c_color_base_get (color_edit->color_base, pos, &red, &green, &blue, 
                    &name, &comment, &error);  
                    
  if (error) {    
    c_color_edit_log_error (color_edit, error);
    return;
  }  
  
  combo_change_one (color_edit, item, name, pos);                    

  if (color_edit->pos == pos) {
    
    c_color_edit_set (color_edit, pos, red, green, blue, name, comment);
    
  }    
 
}                               

C_MAKE_TYPE (c_color_edit, "CColorEdit", CColorEdit, class_init, init, PARENT_TYPE)
