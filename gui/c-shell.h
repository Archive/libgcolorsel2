/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef C_SHELL_H
#define C_SHELL_H

#include <gcolorsel2.h>

#include <gtk/gtkobject.h>

#define C_SHELL_TYPE   (c_shell_get_type ())
#define C_SHELL(o)     (GTK_CHECK_CAST ((o), C_SHELL_TYPE, CShell))
#define C_IS_SHELL(o)  (GTK_CHECK_TYPE ((o), C_SHELL_TYPE))

typedef struct _CShell       CShell;
typedef struct _CShellClass  CShellClass;

typedef void (CShellForeachFunc) (CShell *shell, int id, const char *name, CColorBase *color_base, gpointer data);

struct _CShell {
  GtkObject object;

  int id;
  
  GList *clients;
  GList *views;
  
  int current;
};

struct _CShellClass {
  GtkObjectClass parent_class;    
  
  void (*append)    (CShell *shell, const char *name, int id, gpointer client);
  void (*remove)    (CShell *shell, int id);
  void (*switch_to) (CShell *shell, int id);
  void (*rename)    (CShell *shell, int id, const char *name);
};

GtkType       c_shell_get_type         (void);
CShell       *c_shell_new              (void);
void          c_shell_construct        (CShell *shell);

int           c_shell_open             (CShell *shell, const char *path, 
					const char *name, gboolean try_create,
					gboolean truncate, 
					CError **error);

int           c_shell_open_user        (CShell *shell, const char *name,
                                        CError **error);					

int           c_shell_open_system      (CShell *shell, const char *name,
                                        CError **error);					

void          c_shell_switch_to        (CShell *shell, int id);

int           c_shell_get_current      (CShell *shell);
CColorBase   *c_shell_get_color_base   (CShell *shell, int id);

void          c_shell_close            (CShell *shell, int id);

void          c_shell_rename           (CShell *shell, int id,
					const char *name);
					
const char   *c_shell_get_name         (CShell *shell, int id);					
gboolean      c_shell_get_read_only    (CShell *shell, int id);

void          c_shell_log_error        (CShell *shell, CError *error);
gboolean      c_shell_read_only_dialog (CShell *shell, int id);

int           c_shell_for_each_view    (CShell *shell, CShellForeachFunc func, gpointer data);
					
#endif
