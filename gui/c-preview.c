/* LibGColorsel II
 * Copyright (C) 2000 Eric Brayeur
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "config.h"
#include "c-preview.h"

#include "c-utils.h"

#include <gcolorsel2.h>

#include <gnome.h>

#define PARENT_TYPE GTK_TYPE_FRAME
static GtkFrameClass *parent_class = NULL;

static void c_preview_draw (CPreview *preview);

GtkTargetEntry target_table[] = { { "application/x-color", 0, 69 } };

/* GtkObject methods */

static void
destroy (GtkObject *object)
{
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
class_init (CPreviewClass *class)
{
  GtkObjectClass *object_class;
  GtkFrameClass *frame_class;
  
  object_class = (GtkObjectClass *) class;
  frame_class = (GtkFrameClass *) class;
  
  parent_class = gtk_type_class (GTK_TYPE_FRAME);
  
  object_class->destroy = destroy;
}

static void
init (CPreview *preview)
{  
  preview->red = 0;
  preview->green = 0;
  preview->blue = 0;    
}

static void
size_allocate (GtkWidget *widget, GtkAllocation *allocation, CPreview *preview)
{
  c_preview_draw (preview);
}

static void
c_preview_drag_begin (GtkWidget *widget, GdkDragContext *context, CPreview *preview)
{
  GtkWidget *drag_widget;
  
  drag_widget = c_utils_widget_rgb (preview->red, preview->green, preview->blue, NULL);
  
  gtk_drag_set_icon_widget (context, drag_widget, -2, -2);  
}

static void
c_preview_drag_data_get (GtkWidget *widget, GdkDragContext *context,
                         GtkSelectionData *selection_data, guint info,
                         guint time, CPreview *preview)
{
  guint16 vals[4];
  
  vals[0] = (preview->red / 255.0) * 0xffff;
  vals[1] = (preview->green / 255.0) * 0xffff;  
  vals[2] = (preview->blue / 255.0) * 0xffff;  
  vals[3] = 0xffff;
  
  gtk_selection_data_set (selection_data, 
                          gdk_atom_intern ("application/x-color", FALSE),
                          16, (guchar *) vals, 8);
}

void
c_preview_construct (CPreview *preview)
{
  g_assert (preview != NULL);  
  
  gtk_frame_set_shadow_type (GTK_FRAME (preview), GTK_SHADOW_IN);
  
  preview->preview = gtk_preview_new (GTK_PREVIEW_COLOR);
  
  gtk_signal_connect (GTK_OBJECT (preview->preview), "size_allocate",
                      size_allocate, preview);
                      
  /* Dnd */
  
  gtk_drag_source_set (preview->preview, GDK_BUTTON1_MASK,
                       target_table, 1, GDK_ACTION_COPY);                      

  gtk_signal_connect (GTK_OBJECT (preview->preview), "drag_begin",
                      GTK_SIGNAL_FUNC (c_preview_drag_begin), preview);                                              
  gtk_signal_connect (GTK_OBJECT (preview->preview), "drag_data_get",
                      GTK_SIGNAL_FUNC (c_preview_drag_data_get), preview);                       
                      
  gtk_container_add (GTK_CONTAINER (preview), preview->preview);
  gtk_preview_set_expand (GTK_PREVIEW (preview->preview), TRUE);
  
  gtk_widget_show (preview->preview);  
  
  c_preview_draw (preview);
}

CPreview *
c_preview_new ()
{
  CPreview *preview;
  
  preview = C_PREVIEW (gtk_type_new (c_preview_get_type ()));
  
  c_preview_construct (preview);
  
  return preview;
}

static void
c_preview_draw (CPreview *preview)
{
  guchar *buf;
  int x, y;
  
  g_assert (preview != NULL);
  
  buf = g_new (guchar, 3 * preview->preview->allocation.width);
  
  for (x = 0; x < preview->preview->allocation.width; x++) {
    buf [x * 3]     = preview->red;
    buf [x * 3 + 1] = preview->green;
    buf [x * 3 + 2] = preview->blue;
  }
  
  for (y=0; y < preview->preview->allocation.height; y++)
    gtk_preview_draw_row (GTK_PREVIEW (preview->preview),
                          buf, 0, y, preview->preview->allocation.width);
                          
  gtk_widget_draw (preview->preview, NULL);                       
  
  g_free (buf);
}

void
c_preview_set_rgb (CPreview *preview, gushort red, gushort green, gushort blue)
{
  g_assert (preview != NULL);
  
  preview->red = red;
  preview->green = green;
  preview->blue = blue;

  c_preview_draw (preview);  
}

C_MAKE_TYPE (c_preview, "CPreview", CPreview, class_init, init, PARENT_TYPE)
